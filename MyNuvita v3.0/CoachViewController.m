//
//  CoachViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/21/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "CoachViewController.h"

@interface CoachViewController ()

@property (retain, nonatomic) NSMutableArray *lessons;
@property (retain, nonatomic) NSDictionary *coachInfo;
@property (retain, nonatomic) NSDate *today;

@end

@implementation CoachViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getCoachWeek];
}

#pragma mark - Public Methods

- (void)moveNextClicked:(id)sender {
    _today = [_today getNextWeek];
    [self getCoachWeek];
}

- (void)movePrevClicked:(id)sender {
    _today = [_today getLastWeek];
    [self getCoachWeek];
}

- (void)loadSettings {
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    buttonPrev.backgroundColor = [UIColor clearColor];
    buttonPrev.tintColor = kColorNuvitaBlue;
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                                [[UIBarButtonItem alloc] initWithCustomView:buttonPrev]];
    
    _today = [NSDate date];
    _lessons = [[NSMutableArray alloc] init];
}

- (void)loadCoachWeekData:(NSDictionary *)info {
    _coachInfo = info;
    _lblWeek.text = info[@"a:WeekLabel"];
    
    [_lessons removeAllObjects];
    id object = info[kInfoLessonKey][kLessonsKey];
    if ([object isKindOfClass:[NSArray class]])
        [_lessons addObjectsFromArray:object];
                 
    if ([object isKindOfClass:[NSDictionary class]])
        [_lessons addObject:object];
                 
    [_tableView reloadData];
}

- (void)getCoachWeek {
    if ([[_today getLastWeek] isEqualToDate:[[NSDate date] getLastWeek]]) {
        [[self.navigationItem.rightBarButtonItems firstObject] setEnabled:NO];
    } else {
        [[self.navigationItem.rightBarButtonItems firstObject] setEnabled:YES];
    }
    
    [JBLoadingView startAnimating];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetCoachWeek;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.date = [_today description];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetCoachWeekDidFinishWithResponse:response];
    });
}

- (void)didTapScheduleAppointment {
    [[NSUserDefaults standardUserDefaults] setObject:_coachInfo[@"a:ScheduleUrl"] forKey:kInfoURLKey];
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                  bundle:[NSBundle mainBundle]]
                                        instantiateViewControllerWithIdentifier:@"Webview"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)didTapAskQuestion {
    AddCoachPostViewController *viewController = (AddCoachPostViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                                          bundle:[NSBundle mainBundle]]
                                                  instantiateViewControllerWithIdentifier:@"addcoach"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)didTapAboutMe {
    [[NSUserDefaults standardUserDefaults] setObject:_coachInfo[@"a:AboutCoach"] forKey:kInfoURLKey];
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                  bundle:[NSBundle mainBundle]]
                                        instantiateViewControllerWithIdentifier:@"Webview"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

#pragma mark - WSDLRequestResponse Delegate

- (void)GetAppointmentSchedulerDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    if ([[response info] textValue]) {
        [[NSUserDefaults standardUserDefaults] setObject:[[response info] textValue] forKey:kInfoURLKey];
        UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                      bundle:[NSBundle mainBundle]]
                                            instantiateViewControllerWithIdentifier:@"Webview"];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    }
}

- (void)GetCoachWeekDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"GetCoachWeekDidFinishWithResponse: %@", [response info]);
//    if ([response error]) {
//        [UIAlertView displayAlert:[[response error] localizedDescription]];
//        return;
//    }
//    
//    if (![[response status] isSuccess]) {
//        [UIAlertView displayAlert:[[response status] errorMessage]];
//        return;
//    }
//    
    [self loadCoachWeekData:[response info]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_lessons count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.numberOfLines = 0;
    }
    
    NSDictionary *data = _lessons[indexPath.row];
    cell.textLabel.text = data[kLessonNameKey];
    cell.imageView.image = [data[@"a:MemberScore"] integerValue] >= [data[@"a:PassingScore"] integerValue] ? [[UIImage imageNamed:@"clipboard-icon"] colorizedImage:kColorNuvitaBlue] : [[UIImage imageNamed:@"clipboard-icon"] colorizedImage:[UIColor colorWithWhite:0.8 alpha:0.6]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)minHeightForText:(NSString *)_text {
    return [_text sizeWithFont:[UIFont boldSystemFontOfSize:14]
             constrainedToSize:CGSizeMake(300, 999999)
                 lineBreakMode:NSLineBreakByWordWrapping].height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = _lessons[indexPath.row];
    NSString *text = data[kLessonNameKey];
    CGFloat height = [self minHeightForText:text];
    return height + 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = [self minHeightForText: _coachInfo[@"a:WeeklyTopic"]];
    return 200 + height + 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    
    CGRect frame = tableView.frame;
    UIImageView *imgAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - 130 - 10, 10, 130, 160)];
    [imgAvatar setImageWithURL:[NSURL URLWithString:_coachInfo[@"a:CoachPhotoUrl"]]];
    imgAvatar.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *lblCoachName = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 160, 40)];
    lblCoachName.backgroundColor = [UIColor clearColor];
    lblCoachName.font = [UIFont fontWithName:@"Noteworthy-bold" size:20];
    lblCoachName.textColor = kColorNuvitaRed;
    lblCoachName.textAlignment = NSTextAlignmentRight;
    lblCoachName.text = _coachInfo[@"a:CoachName"];
    
    UIButton *btnScheduleAppointment = [[UIButton alloc] initWithFrame:CGRectMake(10, 50, 160, 40)];
    btnScheduleAppointment.backgroundColor = [UIColor clearColor];
    btnScheduleAppointment.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    btnScheduleAppointment.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btnScheduleAppointment setTitleColor:kColorNuvitaBlue forState:UIControlStateNormal];
    [btnScheduleAppointment setTitle:@"schedule appointment >" forState:UIControlStateNormal];
    [btnScheduleAppointment addTarget:self
                               action:@selector(didTapScheduleAppointment)
                     forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnAskQuestion = [[UIButton alloc] initWithFrame:CGRectMake(10, 90, 160, 40)];
    btnAskQuestion.backgroundColor = [UIColor clearColor];
    btnAskQuestion.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    btnAskQuestion.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btnAskQuestion setTitleColor:kColorNuvitaBlue forState:UIControlStateNormal];
    [btnAskQuestion setTitle:@"ask me a question >" forState:UIControlStateNormal];
    [btnAskQuestion addTarget:self
                               action:@selector(didTapAskQuestion)
                     forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnAbout = [[UIButton alloc] initWithFrame:CGRectMake(10, 130, 160, 40)];
    btnAbout.backgroundColor = [UIColor clearColor];
    btnAbout.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    btnAbout.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btnAbout setTitleColor:kColorNuvitaBlue forState:UIControlStateNormal];
    [btnAbout setTitle:@"about me >" forState:UIControlStateNormal];
    [btnAbout addTarget:self
                 action:@selector(didTapAboutMe)
       forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 180, 300, 40)];
    lblHeader.backgroundColor = [UIColor clearColor];
    lblHeader.textColor = kColorNuvitaRed;
    lblHeader.font = [UIFont boldSystemFontOfSize:20];
    lblHeader.text = @"Just for U";
    
    CGFloat height = [self minHeightForText: _coachInfo[@"a:WeeklyTopic"]];
    UILabel *lblWeeklyTopic = [[UILabel alloc] initWithFrame:CGRectMake(10, 220, 300, height + 18)];
    lblWeeklyTopic.backgroundColor = [UIColor whiteColor];
    lblWeeklyTopic.font = [UIFont boldSystemFontOfSize:18];
    lblWeeklyTopic.numberOfLines = 0;
    lblWeeklyTopic.textColor = [UIColor lightGrayColor];
    lblWeeklyTopic.text = _coachInfo[@"a:WeeklyTopic"];
    
    [view addSubview:imgAvatar];
    [view addSubview:lblCoachName];
    [view addSubview:btnScheduleAppointment];
    [view addSubview:btnAskQuestion];
    [view addSubview:btnAbout];
    [view addSubview:lblHeader];
    [view addSubview:lblWeeklyTopic];
    
    CALayer *layer = [CALayer drawLineWithSize:CGSizeMake(tableView.frame.size.width, (200 + height + 40 - 1))];
    [view.layer addSublayer:layer];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:_lessons[indexPath.row]
                                              forKey:kAPIGetCoachLesson];
    [[NSUserDefaults standardUserDefaults] setObject:_coachInfo
                                              forKey:@"CoachInfo"];
    [[NSUserDefaults standardUserDefaults] setObject:_today
                                              forKey:kInfoDateKey];
    
    [self performSelector:@selector(loadQuizDetails)
               withObject:nil
               afterDelay:0.5];
}

- (void)loadQuizDetails {
    JustForYouViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                          bundle:[NSBundle mainBundle]]
                                                instantiateViewControllerWithIdentifier:@"justforyou"];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
