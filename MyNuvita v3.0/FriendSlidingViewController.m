//
//  FriendSlidingViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/20/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "FriendSlidingViewController.h"

@interface FriendSlidingViewController ()
@end

@implementation FriendSlidingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     FriendViewController *friendViewController = (FriendViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                                     bundle:[NSBundle mainBundle]]
                                                                           instantiateViewControllerWithIdentifier:@"Friend"];
    self.topViewController = friendViewController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
