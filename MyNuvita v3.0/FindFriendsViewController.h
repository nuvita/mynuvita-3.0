//
//  FindFriendsViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/28/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface FindFriendsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *lblOption;

@end
