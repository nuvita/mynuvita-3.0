//
//  LoginViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/24/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgCheckmark;
@end
