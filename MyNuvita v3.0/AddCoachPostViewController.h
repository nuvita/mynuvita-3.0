//
//  AddCoachPostViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/8/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface AddCoachPostViewController : UIViewController

//@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end
