//
//  FriendTableViewCell.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/9/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "FriendTableViewCell.h"

@interface FriendTableViewCell ()

@property (retain, nonatomic) NSMutableArray *elements;

@end

@implementation FriendTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize size = self.frame.size;
    size.height = size.height - 1;
    CALayer *layer = [CALayer drawLineWithSize:size];
    [self.contentView.layer addSublayer:layer];
}

- (void)loadSettings {
    
    _imgAvatar.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _imgAvatar.layer.borderWidth = 1;
    _imgAvatar.layer.cornerRadius = _imgAvatar.frame.size.width / 2;
    _imgAvatar.layer.masksToBounds = YES;
    
    [_imgCheckbox setImage:[[UIImage imageNamed:@"uncheck-blue"] colorizedImage:kColorNuvitaBlue]];
    _imgCheckbox.layer.borderColor = [kColorNuvitaBlue CGColor];
    _imgCheckbox.layer.borderWidth = 1;
    _imgCheckbox.layer.cornerRadius = 2;
    _imgCheckbox.layer.masksToBounds = YES;
    
    [_btnCheckbox addTarget:self
                     action:@selector(didTapCheckbox:)
           forControlEvents:UIControlEventTouchUpInside];
    
//    CGRect frame = self.frame;
//    NSLog(@"width: %f", self.frame.size.width);
    JProgressView *graph = [[JProgressView alloc] initWithFrame:CGRectMake(112, 8, 200, 110)];
    graph.backgroundColor = [UIColor clearColor];
    graph.tag = 1111;
    graph.multiple = YES;
    graph.percent = 0;
    graph.progressTintColor = [UIColor grayColor];
    graph.progressName = @"";
    
    [self.contentView addSubview:graph];
}

- (void)delay {
    if ([_delegate respondsToSelector:@selector(removeCell:)]) {
        [_delegate removeCell:self];
    }
}

- (void)didTapCheckbox:(id)sender {
    if ([_delegate respondsToSelector:@selector(tapCheckmarkCell:)]) {
        [_delegate tapCheckmarkCell:self];
    }
}

- (void)loadUserWeeklyProgress:(NSDictionary *)info {
//    NSLog(@"width: %f", self.bounds.size.width);
    //...draw graph
    if ([info[kInfoElementKey][kInfoElementProgressKey] isKindOfClass:[NSArray class]]) {
        _elements = [NSMutableArray arrayWithArray:info[kInfoElementKey][kInfoElementProgressKey]];
        
        if ([_elements count] > 0) {
            JProgressView *graph = (JProgressView *)[self viewWithTag:1111];
            if (graph) {
                graph.percent = [info[kElementProgressKey] floatValue];
                graph.percents = _elements;
                [graph setNeedsDisplay];
            }
        }
    }
}

- (void)getUserCellData:(NSDictionary *)details atDate:(NSDate *)date {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetWeeklyProgress;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = details[@"a:Eid"];
        parameter.date = [date description];
        
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            _lblName.text = details[kFriendNameKey];
            _imgCheckbox.image = [details[@"selected"] boolValue] ? [UIImage imageNamed:@"check-blue"] : [UIImage imageNamed:@"uncheck-blue"];
            [_imgAvatar setImageWithURL:[NSURL URLWithString:details[kFriendAvatarKey]]
                       placeholderImage:kDummyAvatar];
            
            [self GetWeeklyProgressDidFinishWithResponse:response];
        });
    });
}


- (void)GetWeeklyProgressDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
//    [self performSelectorInBackground:@selector(loadUserWeeklyProgress:) withObject:response.info];
//    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadUserWeeklyProgress:response.info];
//    });
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//        //Background Thread
//        dispatch_async(dispatch_get_main_queue(), ^(void){
//            //Run UI Updates
//        });
//    });
}

@end
