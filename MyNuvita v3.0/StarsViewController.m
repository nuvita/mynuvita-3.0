//
//  StarsViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/14/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "StarsViewController.h"

@interface StarsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSMutableArray *stars;

@property (retain, nonatomic) NSDate *today;
@property (retain, nonatomic) NSString *starType;

@end

@implementation StarsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _today = [NSDate date];
    self.view.backgroundColor = [UIColor lightGrayColor];
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getCardioStars:nil];
}

#pragma mark - Public Methods

- (void)moveNextClicked:(id)sender {
    _today = [_today getNextWeek];
    [self getCardioStars:nil];
}

- (void)movePrevClicked:(id)sender {
    _today = [_today getLastWeek];
    [self getCardioStars:nil];
}

- (void)loadSettings {
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    buttonPrev.backgroundColor = [UIColor clearColor];
    buttonPrev.tintColor = kColorNuvitaBlue;
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                                [[UIBarButtonItem alloc] initWithCustomView:buttonPrev]];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Global"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(getCardioStars:)];
    
    _stars = [[NSMutableArray alloc] init];
}

- (void)loadCardioStarsInfo:(NSDictionary *)info {
    
    _lblWeek.text = info[kInfoWeekKey];

    id object = info[kInfoCardioStarKey][kCardioStarsKey];
    if ([object isKindOfClass:[NSArray class]])
        [_stars addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_stars addObject:object];
    
    NSLog(@"stars: %d", (int)[_stars count]);
    [_tableView reloadData];
}

- (void)getCardioStars:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [_stars removeAllObjects];
        
        if (sender) {
            UIBarButtonItem *item = (UIBarButtonItem *)sender;
            if ([item.title isEqualToString:@"Global"]) {
                item.title = @"My Org";
            } else {
                item.title = @"Global";
            }
        }
        
        if ([[_today getLastWeek] isEqualToDate:[[NSDate date] getLastWeek]]) {
            [[self.navigationItem.rightBarButtonItems firstObject] setEnabled:NO];
        } else {
            [[self.navigationItem.rightBarButtonItems firstObject] setEnabled:YES];
        }
        
        [JBLoadingView startAnimating];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetCardioStars;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [_today description];
        parameter.pageNumber = [NSString stringWithFormat:@"%d", [_stars count]];
        parameter.pageSize = @"10";
        parameter.type = [self.navigationItem.leftBarButtonItem.title isEqualToString:@"Global"] ? @"min" : @"gmin";
 
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self GetCardioStarsWithResponse:response];
        });
    });
}

- (void)getRemainingStars {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetCardioStars;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [_today description];
        parameter.pageNumber = [NSString stringWithFormat:@"%d", [_stars count]];
        parameter.pageSize = @"10";
        parameter.type = [self.navigationItem.leftBarButtonItem.title isEqualToString:@"Global"] ? @"min" : @"gmin";

        response = [binding parseBindingRequestUsingParameter:parameter];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetCardioStarsWithResponse:response];
        });
    });
}

#pragma mark - WSDLRequestResponse Delegate

- (void)GetCardioStarsWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"response: %@", [response info]);
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self loadCardioStarsInfo:response.info];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_stars count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
        CGSize size = CGSizeMake(_tableView.frame.size.width, cell.frame.size.height);
        CALayer *layer = [CALayer drawLineWithSize:size];
        [cell.contentView.layer addSublayer:layer];
    }
    
    NSDictionary *info = _stars[indexPath.row];
    CGRect frame = tableView.frame;
    UILabel *lblvalue = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - 38, 0, 30, 44)];
    lblvalue.backgroundColor = [UIColor clearColor];
    lblvalue.font = cell.textLabel.font;
    lblvalue.textAlignment = NSTextAlignmentCenter;
    lblvalue.text = info[kInfoPercentKey];
    
    UILabel *lblpercent = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - 68 - lblvalue.frame.size.width - 5, 0, 60, 44)];
    lblpercent.font = cell.textLabel.font;
    lblpercent.textAlignment = NSTextAlignmentCenter;
    lblpercent.text = info[kInfoValueKey];
    
    [cell.contentView addSubview:lblvalue];
    [cell.contentView addSubview:lblpercent];

    cell.textLabel.text = [NSString stringWithFormat:@"%d\t%@", (int)([indexPath row] + 1), info[kInfoNameKey]];
    if ([self.navigationItem.leftBarButtonItem.title isEqualToString:@"My Org"]) {
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"\t%@", info[@"a:Organization"]];
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    
    CGRect frame = tableView.frame;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - 38, 0, 30, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:14];
    label.text = @"%";
    [view addSubview:label];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - 68 - label.frame.size.width - 5, 0, 60, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:14];
    label.text = @"Minutes";
    [view addSubview:label];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    NSLog(@"height: %f - %f", _tableView.contentSize.height, scrollView.contentOffset.y + _tableView.bounds.size.height);
    if (_tableView.contentSize.height < scrollView.contentOffset.y + _tableView.bounds.size.height) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getRemainingStars];
        });
    }
}

@end
