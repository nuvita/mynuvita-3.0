//
//  JProgressView.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/14/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface JProgressView : UIView

//.. single color
@property (retain, nonatomic) NSString *progressName;
@property (retain, nonatomic) UIColor *progressTintColor;
@property (assign) CGFloat percent;

//.. multiple color
//@property (assign) NSMutableArray *percents;
@property (assign) NSArray *percents;
@property (assign) BOOL multiple;

@end
