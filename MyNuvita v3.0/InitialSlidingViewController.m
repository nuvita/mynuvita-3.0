//
//  InitialSlidingViewController.m
//  NuvitaCardio
//
//  Created by John on 3/28/14.
//
//

#import "InitialSlidingViewController.h"
#import "HomeViewController.h"

@interface InitialSlidingViewController ()

@property (nonatomic, retain) HomeViewController *homeViewController;

@end

@implementation InitialSlidingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)didTapMenu {
    [_homeViewController showLeftBarMenu];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //left button
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"menu-icon"] colorizedImage:kColorNuvitaBlue]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(didTapMenu)];
    
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    buttonPrev.backgroundColor = [UIColor clearColor];
    buttonPrev.tintColor = kColorNuvitaBlue;
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self action:@selector(movePrevClicked) forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self action:@selector(moveNextClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                                [[UIBarButtonItem alloc] initWithCustomView:buttonPrev]];

    
    _homeViewController = (HomeViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                           bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    self.topViewController = _homeViewController;
}

- (void)moveNextClicked {
    [_homeViewController moveNextClicked];
}

- (void)movePrevClicked {
    [_homeViewController movePrevClicked];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
