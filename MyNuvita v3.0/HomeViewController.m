//
//  HomeViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/13/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController () <ECSlidingViewControllerDelegate, MenuViewControllerDelegate>

@property (strong, nonatomic) MenuViewController *menuViewController;

@property (retain, nonatomic) NSDate *today;
//@property (retain, nonatomic) NSDictionary *details;
@property (retain, nonatomic) NSMutableArray *elements;

@end

@implementation HomeViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self loadSettings];
    [self initLeftMenuViewController];
    
//    UIView *view = (UIView *)[self.view viewWithTag:1111];
//    view.layer.masksToBounds = NO;
//    view.layer.shadowOffset = CGSizeMake(0, 1);
//    view.layer.shadowRadius = 2.0f;
//    view.layer.shadowOpacity = 0.2f;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self loadUserProgress];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (UIColor *)getObjectColor:(NSDictionary *)info {
    if ([info isKindOfClass:[NSDictionary class]]) {
        UIColor *color = nil;
        if ([[info objectForKey:@"a:Color"] isEqualToString:@"Red"]) {
            color = kColorNuvitaRed;
        } else if ([[info objectForKey:@"a:Color"] isEqualToString:@"Blue"]) {
            color = kColorNuvitaBlue;
        } else if ([[info objectForKey:@"a:Color"] isEqualToString:@"Green"]) {
            color = kColorNuvitaGreen;
        } else if ([[info objectForKey:@"a:Color"] isEqualToString:@"Orange"]) {
            color = kColorNuvitaOrange;
        } else {
            color = [UIColor grayColor];
        }
        return color;
    }
    
    return nil;
}

- (void)loadSettings {
    
    _elements = [[NSMutableArray alloc] init];
    _today = [NSDate date];
    
    _imgAvatar.layer.cornerRadius = _imgAvatar.frame.size.width / 2;
    _imgAvatar.clipsToBounds = YES;
}

- (void)loadProgressViewData:(NSDictionary *)info {
    
    _lblProgram.text = info[kInfoProgramKey] ?: [[NSUserDefaults standardUserDefaults] objectForKey:kInfoProgramKey];
    _lblWeek.text = info[kInfoWeekKey];
    _lblWelcome.text = [[NSString stringWithFormat:@"welcome %@", info[kInfoFirstNameKey]] lowercaseString];
    [[NSUserDefaults standardUserDefaults] setObject:info[kInfoFirstNameKey]
                                              forKey:kInfoFirstNameKey];
    [_imgAvatar setImageWithURL:info[kInfoAvatarKey]
               placeholderImage:kDummyAvatar];
    [[NSUserDefaults standardUserDefaults] setObject:info[kInfoAvatarKey]
                                              forKey:kInfoAvatarKey];
    
   
    id object = info[kInfoElementKey][kInfoElementProgressKey];
    [_elements removeAllObjects];
    if ([object isKindOfClass:[NSArray class]])
        [_elements addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_elements addObject:object];
        

    if ([_elements count] > 0) {
        
        //...draw total
        NSInteger totalWidth = _graphView.width;
        
        UIView *content = [[UIView alloc] initWithFrame:CGRectMake(5, 0, totalWidth - 10, totalWidth * .45)];
        content.backgroundColor = [UIColor whiteColor];
        content.tag = 6666;
//        content.layer.cornerRadius = 2.0f;
//        content.layer.masksToBounds = NO;
//        content.layer.borderColor = [[UIColor colorWithHexString:@"#e1e1e1"] CGColor];
//        content.layer.borderWidth = 0.5f;
//        content.layer.shadowOffset = CGSizeMake(-1, 1);
//        content.layer.shadowRadius = 1.0f;
//        content.layer.shadowOpacity = 0.1f;
        
        JProgressView *totalProgressView = [[JProgressView alloc] initWithFrame:CGRectMake(0, 0, totalWidth * .70, content.height)];
        totalProgressView.backgroundColor = [UIColor clearColor];
        totalProgressView.multiple = YES;
        totalProgressView.progressTintColor = [UIColor grayColor];
        totalProgressView.progressName = @"progress";
        totalProgressView.percent = [info[kElementProgressKey] floatValue];
        totalProgressView.percents = _elements;
        totalProgressView.left = (content.width - totalProgressView.width) / 2;
        
        [content addSubview:totalProgressView];
        [_graphView addSubview:content];
        
        //...draw graph
        __block CGFloat offset = 5;
        __block CGFloat width = (totalWidth - 15) / 2; //160
        __block CGFloat height = width * .67; //100
        __block CGFloat x = offset;
        __block CGFloat y = offset;
        
        __block NSInteger count = [_elements count];
        NSInteger row = roundf(count / 2.0);
        _scrollview.contentSize = CGSizeMake(_scrollview.contentSize.width, row * (height + offset));
        _scrollview.top = totalProgressView.bottom;
        
        [_elements enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (idx == count - 1 && count % 2 != 0) {
                x += (width / 2);
            }
            
            UIView *subcontent = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
            subcontent.backgroundColor = [UIColor whiteColor];
            subcontent.tag = 6666;
//            subcontent.layer.cornerRadius = 2.0f;
//            subcontent.layer.masksToBounds = NO;
//            subcontent.layer.borderColor = [[UIColor colorWithHexString:@"#e1e1e1"] CGColor];
//            subcontent.layer.borderWidth = 0.5f;
//            subcontent.layer.shadowOffset = CGSizeMake(-1, 1);
//            subcontent.layer.shadowRadius = 1.0f;
//            subcontent.layer.shadowOpacity = 0.1f;
            
            JProgressView *view = [[JProgressView alloc] initWithFrame:CGRectMake(0, 0, subcontent.width - 10, subcontent.height)];
            view.layer.cornerRadius = 2.0f;
            view.layer.masksToBounds = YES;
            view.backgroundColor = [UIColor clearColor];
            view.multiple = NO;
            view.percent = [obj[kElementProgressKey] floatValue];
            view.progressTintColor = [self getObjectColor:obj];
            view.progressName = [obj[kElementNameKey] lowercaseString];
            view.left = (subcontent.width - view.width) / 2;
            [subcontent addSubview:view];
            
            UIButton *button = [[UIButton alloc] initWithFrame:view.frame];
            button.backgroundColor = [UIColor clearColor];
            button.tag = idx;
            [button addTarget:self
                       action:@selector(didTapGraph:)
             forControlEvents:UIControlEventTouchUpInside];
            
            [subcontent addSubview:button];
            [_scrollview addSubview:subcontent];
            x += width + offset;
            if (idx % 2 != 0) {
                x = offset;
                y += height + offset;
            }
        }];
 
    }
}

- (void)didTapGraph:(UIButton *)sender {
    NSDictionary *info = _elements[[sender tag]];
    [self loadController:info[kElementNameKey]];
}

- (void)loadController:(NSString *)identifier {
    [[NSUserDefaults standardUserDefaults] setObject:_today forKey:kInfoDateKey];
    NSString *string = [identifier getFirstWord];
    if ([[string lowercaseString] isEqualToString:@"coaching"]) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate loadCoachTabarController];
        return;
    }
    
    if ([[string lowercaseString] isEqualToString:@"nutrition"]) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate loadNutritionTabarController];
        return;
    }
    
    WebViewController *viewController = (WebViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                        bundle:[NSBundle mainBundle]]
                                              instantiateViewControllerWithIdentifier:string];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
    
}

- (void)cleanSubviews {
    for (id subview in [_graphView subviews]) {
        if ([subview tag] == 6666 && [subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        } else {
            for (id innersubview in [subview subviews]) {
                if ([innersubview tag] == 6666 && [innersubview isKindOfClass:[UIView class]]) {
                    [innersubview removeFromSuperview];
                }
            }
        }
    }
}

- (void)loadUserProgress {
    [self cleanSubviews];
    
    if ([[_today getLastWeek] isEqualToDate:[[NSDate date] getLastWeek]]) {
        [[self.slidingViewController.navigationItem.rightBarButtonItems firstObject] setEnabled:NO];
    } else {
        [[self.slidingViewController.navigationItem.rightBarButtonItems firstObject] setEnabled:YES];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        if (![loginInfo objectForKey:kInfoIDKey]) {
            [UIAlertView displayAlert:@"Session is invalid!"];
            return;
        }
        
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetWeeklyProgress;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [_today description];
        response = [binding parseBindingRequestUsingParameter:parameter];
   
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetWeeklyProgressDidFinishWithResponse:response];
        });
    });
}

#pragma mark - Menu

- (void)didShowLeftPanel {
    
}

- (void)initLeftMenuViewController {
    
    self.view.layer.shadowOpacity = 1;
    self.view.layer.shadowRadius = 1;
    self.view.layer.shadowColor = [UIColor grayColor].CGColor;
    
    _menuViewController = [[MenuViewController alloc] init];
    _menuViewController.delegate = self;
    self.slidingViewController.mydelegate = self;
    self.slidingViewController.underLeftViewController = _menuViewController;
    [self.slidingViewController.topViewController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (void)showLeftBarMenu {
    
    if ([_menuViewController.slidingViewController underLeftShowing]) {
        [_menuViewController.slidingViewController resetTopView];
        
    }else {
        [_menuViewController.slidingViewController anchorTopViewTo:ECRight animations:^{
            
        } onComplete:^{
            
        }];
    }
}

#pragma mark - MenuViewControllerDelegate

- (void)didTapURL:(NSString *)url {
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    NSString *urlString = [[NSString stringWithFormat:@"%@?eid=%@", url, [loginInfo objectForKey:kInfoIDKey]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:kInfoURLKey];
    [self loadController:@"Webview"];
}

- (void)menuViewController:(MenuViewController *)viewController didSelectMenu:(NSInteger)index {
    [self.slidingViewController resetTopView];
    if (index == 0) {
        [self didTapURL:@"http://members.mynuvita.com/Nuvita_Elements/Member/MemberSettings"];
//        [self didTapURL:@"http://briviserrorapp.digitalmode.com.au"];
    } else if (index == 1) {
        [self didTapURL:@"http://members.mynuvita.com/utilities/AvatarSelector"];
    } else {
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [delegate loadLogin];
    }
}

- (void)moveNextClicked {
    _today = [_today getNextWeek];
    [self loadUserProgress];
}

- (void)movePrevClicked {
    _today = [_today getLastWeek];
    [self loadUserProgress];
}

#pragma mark - WSDLRequestResponseDelegate

- (void)GetWeeklyProgressDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    
    NSLog(@"response: %@", [response info]);
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:_today forKey:kInfoDateKey];
    [self loadProgressViewData:response.info];
}

@end
