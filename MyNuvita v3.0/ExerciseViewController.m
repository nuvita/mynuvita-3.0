//
//  ExerciseViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/21/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "ExerciseViewController.h"

@interface ExerciseViewController ()

@end

@implementation ExerciseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark - Public Methods

- (void)loadSettings {
    UIImage *image = [UIImage imageNamed:@"button-default"];
    [_btnStart setBackgroundImage:[image colorizedImage:kColorNuvitaGreen]
                         forState:UIControlStateNormal];
    _btnStart.layer.cornerRadius = 4;
    _btnStart.layer.masksToBounds = YES;
    
    [_btnStop setBackgroundImage:[image colorizedImage:kColorNuvitaRed]
                         forState:UIControlStateNormal];
    _btnStop.layer.cornerRadius = 4;
    _btnStop.layer.masksToBounds = YES;
    
    [_btnPause setBackgroundImage:[image colorizedImage:kColorNuvitaOrange]
                        forState:UIControlStateNormal];
    _btnPause.layer.cornerRadius = 4;
    _btnPause.layer.masksToBounds = YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    RTableCell *cell = (RTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.contentView.backgroundColor = [UIColor clearColor];
        if (indexPath.row == 0)
            cell.top = YES;
        if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1)
            cell.down = YES;
    }
    
    cell.textLabel.text = @"Exercise title here!";
    cell.imageView.image = [[UIImage imageNamed:@"profile-avatar"] colorizedImage:kColorNuvitaBlue];
    cell.imageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.imageView.layer.borderWidth = 1;
    cell.imageView.layer.cornerRadius = 2;
    cell.imageView.layer.masksToBounds = YES;
    
    CGRect frame = CGRectMake(0, 0, 36, 36);
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, UIScreen.mainScreen.scale);
    [cell.imageView.image drawInRect:frame];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
