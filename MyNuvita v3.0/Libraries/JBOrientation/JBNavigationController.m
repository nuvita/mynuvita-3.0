//
//  JBNavigationController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/3/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "JBNavigationController.h"

@interface JBNavigationController ()

@end

@implementation JBNavigationController

- (BOOL)shouldAutorotate {
    NSLog(@"nav: %d", self.topViewController.shouldAutorotate);
    return self.topViewController.shouldAutorotate;
}

- (NSUInteger)supportedInterfaceOrientations {
    NSLog(@"nav sup: %lu", (unsigned long)self.topViewController.supportedInterfaceOrientations);
    return self.topViewController.supportedInterfaceOrientations;
}

@end
