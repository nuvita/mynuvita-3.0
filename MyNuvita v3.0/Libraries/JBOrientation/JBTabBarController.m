//
//  JBTabBarController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/3/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "JBTabBarController.h"

@interface JBTabBarController ()

@end

@implementation JBTabBarController

- (BOOL)shouldAutorotate {
    NSInteger index = self.selectedIndex;
//    NSLog(@"Tab Index %d", index);
    
    UINavigationController *nc = (UINavigationController*)[self.viewControllers objectAtIndex:index];
    UIViewController *vc = [nc.viewControllers objectAtIndex:0];
    NSLog(@"Tabbar: %d", vc.shouldAutorotate);
    return vc.shouldAutorotate;
}
- (NSUInteger)supportedInterfaceOrientations {
    NSLog(@"orientaion tab");
    return _orientation;
}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//        _supportedInterfaceOrientatoin = UIInterfaceOrientationMaskPortrait;
//        _orientation = UIInterfaceOrientationPortrait;
//    }
//    
//    return self;
//}
//
//- (BOOL)shouldAutorotate {
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations {
//    return _supportedInterfaceOrientatoin;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return _orientation;
//}

@end
