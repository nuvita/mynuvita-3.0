//
//  JBTabBarController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/3/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JBTabBarController : UITabBarController

@property (assign, nonatomic) UIInterfaceOrientation orientation;
@property (assign, nonatomic) NSUInteger supportedInterfaceOrientatoin;

@end
