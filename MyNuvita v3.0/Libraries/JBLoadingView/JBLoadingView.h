//
//  JBLoadingView.h
//  mynuvita
//
//  Created by John on 9/4/14.
//
//

#import "Globals.h"
#import "FXBlurView.h"

@interface JBLoadingView : NSObject

+ (UIImageView *)createLoadingView;
//+ (UIImageView *)createLoadingView:(UIView *)view;
+ (void)startAnimating;
+ (void)startAnimatingWithText:(NSString *)text;
+ (void)stopAnimating;
//+ (void)loadingViewStartAnimating:(UIView *)parentView withLoadingView:(UIView *)loadingView text:(NSString *)loadingText;
//+ (void)loadingViewStopAnimating:(UIView *)parentView;
//+ (void)loadingViewStopAnimating:(UIView *)parentView withPrompt:(NSString *)prompt;

@end
