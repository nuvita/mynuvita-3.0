//
//  JBLoadingView.m
//  mynuvita
//
//  Created by John on 9/4/14.
//
//


#import "JBLoadingView.h"
#import "UIView+ColorOfPoint.h"

static JBLoadingView *sharedAppData = nil;

@implementation JBLoadingView

+ (JBLoadingView *)sharedAppData {
    @synchronized(self) {
        if (sharedAppData == nil)
            sharedAppData = [[self alloc] init];
    }
    return sharedAppData;
}

+ (UIImage *)captureView:(UIView *)view {

    CALayer *layer = view.layer;
    UIGraphicsBeginImageContext(view.bounds.size);
    CGContextClipToRect(UIGraphicsGetCurrentContext(), view.bounds);
    [layer renderInContext:UIGraphicsGetCurrentContext()];

    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}

+ (UIView *)createLoadingView {
    UIViewController *rootViewController = [[UIApplication sharedApplication].keyWindow rootViewController];
    FXBlurView *loadingView = [[FXBlurView alloc] initWithFrame:rootViewController.view.bounds];
    loadingView.tintColor = [UIColor blackColor];
    loadingView.blurRadius = 20;
    loadingView.dynamic = YES;
    loadingView.tag = 1111;
    
    UIActivityIndicatorView	*progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(loadingView.frame.size.width/2 - 20, loadingView.frame.size.height/2 - 20, 40, 40)];
    progressIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    progressIndicator.color = [UIColor whiteColor];
    progressIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    [progressIndicator sizeToFit];
    [loadingView addSubview:progressIndicator];
    [progressIndicator startAnimating];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, loadingView.frame.size.height/2 + 40, rootViewController.view.bounds.size.width, 20)];
    label.font = [UIFont systemFontOfSize:12.0];
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(1, 1);
    label.shadowColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.text = @"";
    
    [loadingView addSubview:label];
    return loadingView;

}

+ (void)startAnimating {
    UIViewController *rootViewController = [[UIApplication sharedApplication].keyWindow rootViewController];
    UIView *loadingView = [self createLoadingView];
    UIColor *color = [rootViewController.view colorOfPoint:CGPointMake(loadingView.center.x, 44)];

    [[[loadingView subviews] objectAtIndex:0] setColor:[color inverseColor]];
    [[[loadingView subviews] objectAtIndex:1] setTextColor:[color inverseColor]];
    [rootViewController.view addSubview:loadingView];
}

+ (void)startAnimatingWithText:(NSString *)text {
    UIViewController *rootViewController = [[UIApplication sharedApplication].keyWindow rootViewController];
    UIView *loadingView = [self createLoadingView];
    UIColor *color = [rootViewController.view colorOfPoint:CGPointMake(loadingView.center.x, 44)];

    [[[loadingView subviews] objectAtIndex:0] setColor:[color inverseColor]];
    [[[loadingView subviews] objectAtIndex:1] setTextColor:[color inverseColor]];
    [[[loadingView subviews] objectAtIndex:1] setText:text];
    [rootViewController.view addSubview:loadingView];
}

+ (void)stopAnimating {
    UIViewController *rootViewController = [[UIApplication sharedApplication].keyWindow rootViewController];
//    [[rootViewController.view subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        if ([obj tag] == 1111) {
//            [obj removeFromSuperview];
//        }
//    }];
    
    for (UIView *subview in [rootViewController.view subviews]) {
        if ([subview tag] == 1111) {
            [subview removeFromSuperview];
        }
    }
}

@end
