#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class tns4_ArrayOfKeyValuePairOfstringint;
@class tns4_KeyValuePairOfstringint;
#import "tns2.h"
@interface tns4_KeyValuePairOfstringint : NSObject {
	
/* elements */
	NSString * key;
	NSNumber * value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns4_KeyValuePairOfstringint *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * key;
@property (retain) NSNumber * value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns4_ArrayOfKeyValuePairOfstringint : NSObject {
	
/* elements */
	NSMutableArray *KeyValuePairOfstringint;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns4_ArrayOfKeyValuePairOfstringint *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addKeyValuePairOfstringint:(tns4_KeyValuePairOfstringint *)toAdd;
@property (readonly) NSMutableArray * KeyValuePairOfstringint;
/* attributes */
- (NSDictionary *)attributes;
@end
