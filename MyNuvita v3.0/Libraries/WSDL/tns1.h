#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
@class tns1_Stars;
@class tns1_ArrayOfStar;
@class tns1_NuvitaStatus;
@class tns1_Star;
@class tns1_CardioWeek;
@class tns1_ArrayOfHRSession;
@class tns1_HRSession;
@class tns1_Lesson;
@class tns1_ArrayOfQuestion;
@class tns1_Question;
@class tns1_ArrayOfOption;
@class tns1_Range;
@class tns1_Option;
@class tns1_CoachingResources;
@class tns1_ArrayOfCoachingResource;
@class tns1_CoachingResource;
@class tns1_CoachingWalls;
@class tns1_CoachWeek;
@class tns1_ArrayOfLessonTopic;
@class tns1_LessonTopic;
@class tns1_NameList;
@class tns1_ArrayOfNameItem;
@class tns1_NameItem;
@class tns1_Friends;
@class tns1_ArrayOfFriend;
@class tns1_Friend;
@class tns1_MobilityWeek;
@class tns1_MobilityWorkouts;
@class tns1_ArrayOfMobilityWorkoutTopic;
@class tns1_MobilityWorkoutTopic;
@class tns1_MobilityWorkout;
@class tns1_ArrayOfMobilityExercise;
@class tns1_MobilityExercise;
@class tns1_WeeklyProgress;
@class tns1_ArrayOfElementProgress;
@class tns1_ElementProgress;
@class tns1_LegacyMemberProgress;
@class tns1_WellnessWalls;
@class tns1_MobileMember;
@class tns1_MemberQuiz;
@class tns1_Vo2MaxActivityLevel;
@class tns1_Vo2Max;
@class tns1_ProposedCardioProgram;
@class tns1_HealthTrendsChart;
@class tns1_ArrayOfNorm;
@class tns1_ArrayOfHealthTrendsSeries;
@class tns1_Norm;
@class tns1_HealthTrendsSeries;
@class tns1_ArrayOfHealthTrend;
@class tns1_HealthTrend;
#import "tns2.h"
#import "tns3.h"
#import "tns4.h"
@interface tns1_Star : NSObject {
	
/* elements */
	NSString * AvatarUrl;
	NSString * Name;
	NSString * Organization;
	NSNumber * Percent;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Star *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AvatarUrl;
@property (retain) NSString * Name;
@property (retain) NSString * Organization;
@property (retain) NSNumber * Percent;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfStar : NSObject {
	
/* elements */
	NSMutableArray *Star;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfStar *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addStar:(tns1_Star *)toAdd;
@property (readonly) NSMutableArray * Star;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_NuvitaStatus : NSObject {
	
/* elements */
	USBoolean * ErrorStatus;
	NSString * Message;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_NuvitaStatus *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) USBoolean * ErrorStatus;
@property (retain) NSString * Message;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Stars : NSObject {
	
/* elements */
	tns1_ArrayOfStar * CardioStars;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * ProgramName;
	NSString * ValueLabel;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Stars *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfStar * CardioStars;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * ProgramName;
@property (retain) NSString * ValueLabel;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_HRSession : NSObject {
	
/* elements */
	NSNumber * Calories;
	NSString * Date;
	NSNumber * Duration_;
	NSNumber * MinutesAboveZone;
	NSNumber * MinutesBelowZone;
	NSNumber * MinutesInZone;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_HRSession *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Calories;
@property (retain) NSString * Date;
@property (retain) NSNumber * Duration_;
@property (retain) NSNumber * MinutesAboveZone;
@property (retain) NSNumber * MinutesBelowZone;
@property (retain) NSNumber * MinutesInZone;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfHRSession : NSObject {
	
/* elements */
	NSMutableArray *HRSession;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfHRSession *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addHRSession:(tns1_HRSession *)toAdd;
@property (readonly) NSMutableArray * HRSession;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CardioWeek : NSObject {
	
/* elements */
	NSString * AboveZoneBpm;
	NSString * AboveZonePercent;
	NSString * BelowZoneBpm;
	NSString * BelowZonePercent;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * GoalLabel;
	NSNumber * GoalValue;
	tns1_ArrayOfHRSession * HRSessions;
	NSString * InZoneBpm;
	NSString * InZonePercent;
	NSNumber * LowerLimitBpm;
	NSString * MemberEid;
	NSString * ProgramName;
	NSString * ProgressPercent;
	NSString * ProgressValue;
	NSNumber * UpperLimitBpm;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CardioWeek *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AboveZoneBpm;
@property (retain) NSString * AboveZonePercent;
@property (retain) NSString * BelowZoneBpm;
@property (retain) NSString * BelowZonePercent;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * GoalLabel;
@property (retain) NSNumber * GoalValue;
@property (retain) tns1_ArrayOfHRSession * HRSessions;
@property (retain) NSString * InZoneBpm;
@property (retain) NSString * InZonePercent;
@property (retain) NSNumber * LowerLimitBpm;
@property (retain) NSString * MemberEid;
@property (retain) NSString * ProgramName;
@property (retain) NSString * ProgressPercent;
@property (retain) NSString * ProgressValue;
@property (retain) NSNumber * UpperLimitBpm;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Option : NSObject {
	
/* elements */
	NSNumber * OptionId;
	NSNumber * SortOrder;
	NSString * Text;
	USBoolean * isCorrect;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Option *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * OptionId;
@property (retain) NSNumber * SortOrder;
@property (retain) NSString * Text;
@property (retain) USBoolean * isCorrect;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfOption : NSObject {
	
/* elements */
	NSMutableArray *Option;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfOption *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addOption:(tns1_Option *)toAdd;
@property (readonly) NSMutableArray * Option;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Range : NSObject {
	
/* elements */
	NSNumber * MaxValue;
	NSString * MaxValueLabel;
	NSNumber * MinValue;
	NSString * MinValueLabel;
	NSNumber * Step;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Range *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * MaxValue;
@property (retain) NSString * MaxValueLabel;
@property (retain) NSNumber * MinValue;
@property (retain) NSString * MinValueLabel;
@property (retain) NSNumber * Step;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Question : NSObject {
	
/* elements */
	NSString * CorrectAnswer;
	NSString * MemberAnswer;
	tns1_ArrayOfOption * Options;
	NSNumber * QuestionId;
	tns1_Range * Range;
	NSString * Text;
	NSString * Type;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Question *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CorrectAnswer;
@property (retain) NSString * MemberAnswer;
@property (retain) tns1_ArrayOfOption * Options;
@property (retain) NSNumber * QuestionId;
@property (retain) tns1_Range * Range;
@property (retain) NSString * Text;
@property (retain) NSString * Type;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfQuestion : NSObject {
	
/* elements */
	NSMutableArray *Question;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfQuestion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addQuestion:(tns1_Question *)toAdd;
@property (readonly) NSMutableArray * Question;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Lesson : NSObject {
	
/* elements */
	NSDate * DateTaken;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * HtmlText;
	NSNumber * LessonNumber;
	NSNumber * MemberScore;
	USBoolean * Passed;
	NSNumber * PassingScore;
	tns1_ArrayOfQuestion * Questions;
	NSString * Title;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Lesson *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * DateTaken;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * HtmlText;
@property (retain) NSNumber * LessonNumber;
@property (retain) NSNumber * MemberScore;
@property (retain) USBoolean * Passed;
@property (retain) NSNumber * PassingScore;
@property (retain) tns1_ArrayOfQuestion * Questions;
@property (retain) NSString * Title;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CoachingResource : NSObject {
	
/* elements */
	NSString * Title;
	NSString * URL;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CoachingResource *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Title;
@property (retain) NSString * URL;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfCoachingResource : NSObject {
	
/* elements */
	NSMutableArray *CoachingResource;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfCoachingResource *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addCoachingResource:(tns1_CoachingResource *)toAdd;
@property (readonly) NSMutableArray * CoachingResource;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CoachingResources : NSObject {
	
/* elements */
	tns1_ArrayOfCoachingResource * Resources;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CoachingResources *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_ArrayOfCoachingResource * Resources;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CoachingWalls : NSObject {
	
/* elements */
	NSString * CSS;
	tns1_NuvitaStatus * ErrorStatus;
	tns3_ArrayOfstring * Posts;
	NSString * ProgramName;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CoachingWalls *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CSS;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns3_ArrayOfstring * Posts;
@property (retain) NSString * ProgramName;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LessonTopic : NSObject {
	
/* elements */
	NSString * LessonDescription;
	NSString * LessonName;
	NSNumber * LessonNumber;
	NSNumber * MemberScore;
	USBoolean * Passed;
	NSNumber * PassingScore;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LessonTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * LessonDescription;
@property (retain) NSString * LessonName;
@property (retain) NSNumber * LessonNumber;
@property (retain) NSNumber * MemberScore;
@property (retain) USBoolean * Passed;
@property (retain) NSNumber * PassingScore;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfLessonTopic : NSObject {
	
/* elements */
	NSMutableArray *LessonTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfLessonTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addLessonTopic:(tns1_LessonTopic *)toAdd;
@property (readonly) NSMutableArray * LessonTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_CoachWeek : NSObject {
	
/* elements */
	NSString * AboutCoach;
	NSString * CoachName;
	NSString * CoachPhotoUrl;
	tns1_NuvitaStatus * ErrorStatus;
	tns1_ArrayOfLessonTopic * LessonTopics;
	NSString * ProgramName;
	NSString * ScheduleUrl;
	NSString * WeekLabel;
	NSNumber * WeekNumber;
	NSString * WeeklyTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_CoachWeek *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AboutCoach;
@property (retain) NSString * CoachName;
@property (retain) NSString * CoachPhotoUrl;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns1_ArrayOfLessonTopic * LessonTopics;
@property (retain) NSString * ProgramName;
@property (retain) NSString * ScheduleUrl;
@property (retain) NSString * WeekLabel;
@property (retain) NSNumber * WeekNumber;
@property (retain) NSString * WeeklyTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_NameItem : NSObject {
	
/* elements */
	NSString * Avatar;
	NSString * Eid;
	NSString * Name;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_NameItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Avatar;
@property (retain) NSString * Eid;
@property (retain) NSString * Name;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfNameItem : NSObject {
	
/* elements */
	NSMutableArray *NameItem;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfNameItem *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNameItem:(tns1_NameItem *)toAdd;
@property (readonly) NSMutableArray * NameItem;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_NameList : NSObject {
	
/* elements */
	tns1_NuvitaStatus * ErrorStatus;
	tns1_ArrayOfNameItem * Names;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_NameList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns1_ArrayOfNameItem * Names;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Friend : NSObject {
	
/* elements */
	NSString * AvatarUrl;
	NSString * DisplayName;
	NSString * Eid;
	NSString * Email;
	NSString * ProgramType;
	NSNumber * ProgressPercent;
	NSNumber * SortOrder;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Friend *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AvatarUrl;
@property (retain) NSString * DisplayName;
@property (retain) NSString * Eid;
@property (retain) NSString * Email;
@property (retain) NSString * ProgramType;
@property (retain) NSNumber * ProgressPercent;
@property (retain) NSNumber * SortOrder;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfFriend : NSObject {
	
/* elements */
	NSMutableArray *Friend;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfFriend *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addFriend:(tns1_Friend *)toAdd;
@property (readonly) NSMutableArray * Friend;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Friends : NSObject {
	
/* elements */
	tns1_NuvitaStatus * ErrorStatus;
	tns1_ArrayOfFriend * MyFriends;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Friends *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns1_ArrayOfFriend * MyFriends;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MobilityWeek : NSObject {
	
/* elements */
	NSDate * Date;
	tns1_NuvitaStatus * ErrorStatus;
	USBoolean * FridaySelect;
	NSString * FridayText;
	NSString * MemberEid;
	USBoolean * MondaySelect;
	NSString * MondayText;
	NSString * ProgramName;
	USBoolean * SaturdaySelect;
	NSString * SaturdayText;
	USBoolean * SundaySelect;
	NSString * SundayText;
	USBoolean * ThursdaySelect;
	NSString * ThursdayText;
	USBoolean * TuesdaySelect;
	NSString * TuesdayText;
	USBoolean * WednesdaySelect;
	NSString * WednesdayText;
	NSString * WeekLabel;
	USBoolean * isMobilityX;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MobilityWeek *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * Date;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) USBoolean * FridaySelect;
@property (retain) NSString * FridayText;
@property (retain) NSString * MemberEid;
@property (retain) USBoolean * MondaySelect;
@property (retain) NSString * MondayText;
@property (retain) NSString * ProgramName;
@property (retain) USBoolean * SaturdaySelect;
@property (retain) NSString * SaturdayText;
@property (retain) USBoolean * SundaySelect;
@property (retain) NSString * SundayText;
@property (retain) USBoolean * ThursdaySelect;
@property (retain) NSString * ThursdayText;
@property (retain) USBoolean * TuesdaySelect;
@property (retain) NSString * TuesdayText;
@property (retain) USBoolean * WednesdaySelect;
@property (retain) NSString * WednesdayText;
@property (retain) NSString * WeekLabel;
@property (retain) USBoolean * isMobilityX;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MobilityWorkoutTopic : NSObject {
	
/* elements */
	NSString * WorkoutCode;
	NSNumber * WorkoutId;
	NSString * WorkoutName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MobilityWorkoutTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * WorkoutCode;
@property (retain) NSNumber * WorkoutId;
@property (retain) NSString * WorkoutName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfMobilityWorkoutTopic : NSObject {
	
/* elements */
	NSMutableArray *MobilityWorkoutTopic;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfMobilityWorkoutTopic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addMobilityWorkoutTopic:(tns1_MobilityWorkoutTopic *)toAdd;
@property (readonly) NSMutableArray * MobilityWorkoutTopic;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MobilityWorkouts : NSObject {
	
/* elements */
	tns1_NuvitaStatus * ErrorStatus;
	tns1_ArrayOfMobilityWorkoutTopic * Workouts;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MobilityWorkouts *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns1_ArrayOfMobilityWorkoutTopic * Workouts;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MobilityExercise : NSObject {
	
/* elements */
	NSString * ExerciseName;
	NSNumber * ExerciseNumber;
	NSString * ImageUrl;
	NSString * VideoUrl;
	NSNumber * WorkoutId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MobilityExercise *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ExerciseName;
@property (retain) NSNumber * ExerciseNumber;
@property (retain) NSString * ImageUrl;
@property (retain) NSString * VideoUrl;
@property (retain) NSNumber * WorkoutId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfMobilityExercise : NSObject {
	
/* elements */
	NSMutableArray *MobilityExercise;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfMobilityExercise *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addMobilityExercise:(tns1_MobilityExercise *)toAdd;
@property (readonly) NSMutableArray * MobilityExercise;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MobilityWorkout : NSObject {
	
/* elements */
	tns1_NuvitaStatus * ErrorStatus;
	tns1_ArrayOfMobilityExercise * Exercises;
	NSString * WorkoutCode;
	NSNumber * WorkoutId;
	NSString * WorkoutName;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MobilityWorkout *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns1_ArrayOfMobilityExercise * Exercises;
@property (retain) NSString * WorkoutCode;
@property (retain) NSNumber * WorkoutId;
@property (retain) NSString * WorkoutName;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ElementProgress : NSObject {
	
/* elements */
	NSString * Color;
	NSString * ElementName;
	NSNumber * PercentOrverall;
	NSNumber * ProgressPercent;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ElementProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Color;
@property (retain) NSString * ElementName;
@property (retain) NSNumber * PercentOrverall;
@property (retain) NSNumber * ProgressPercent;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfElementProgress : NSObject {
	
/* elements */
	NSMutableArray *ElementProgress;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfElementProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addElementProgress:(tns1_ElementProgress *)toAdd;
@property (readonly) NSMutableArray * ElementProgress;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_WeeklyProgress : NSObject {
	
/* elements */
	NSString * AvatarUrl;
	tns1_ArrayOfElementProgress * Elements;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * FirstName;
	NSString * PrivacyUrl;
	NSString * ProgramName;
	NSNumber * ProgressPercent;
	NSString * SettingsUrl;
	NSString * UrlBase;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_WeeklyProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * AvatarUrl;
@property (retain) tns1_ArrayOfElementProgress * Elements;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * FirstName;
@property (retain) NSString * PrivacyUrl;
@property (retain) NSString * ProgramName;
@property (retain) NSNumber * ProgressPercent;
@property (retain) NSString * SettingsUrl;
@property (retain) NSString * UrlBase;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_LegacyMemberProgress : NSObject {
	
/* elements */
	NSString * CardioLabel;
	NSNumber * CardioPercent;
	NSString * ErrorMessage;
	USBoolean * ErrorStatus;
	NSString * MobilityLabel;
	NSNumber * MobilityPercent;
	NSString * NutritionLabel;
	NSNumber * NutritionPercent;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_LegacyMemberProgress *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CardioLabel;
@property (retain) NSNumber * CardioPercent;
@property (retain) NSString * ErrorMessage;
@property (retain) USBoolean * ErrorStatus;
@property (retain) NSString * MobilityLabel;
@property (retain) NSNumber * MobilityPercent;
@property (retain) NSString * NutritionLabel;
@property (retain) NSNumber * NutritionPercent;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_WellnessWalls : NSObject {
	
/* elements */
	NSString * CSS;
	tns1_NuvitaStatus * ErrorStatus;
	tns3_ArrayOfstring * Posts;
	NSString * ProgramName;
	NSString * WeekLabel;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_WellnessWalls *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * CSS;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns3_ArrayOfstring * Posts;
@property (retain) NSString * ProgramName;
@property (retain) NSString * WeekLabel;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MobileMember : NSObject {
	
/* elements */
	NSNumber * Age;
	NSString * Avatar;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * FirstName;
	NSString * LastName;
	NSString * MemberId;
	NSString * ProgramName;
	USBoolean * UseCameraMeals;
	NSNumber * Weight;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MobileMember *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * Age;
@property (retain) NSString * Avatar;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * FirstName;
@property (retain) NSString * LastName;
@property (retain) NSString * MemberId;
@property (retain) NSString * ProgramName;
@property (retain) USBoolean * UseCameraMeals;
@property (retain) NSNumber * Weight;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_MemberQuiz : NSObject {
	
/* elements */
	NSDate * DateTaken;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * MemberEid;
	NSNumber * MemberScore;
	USBoolean * Passed;
	NSNumber * PassingScore;
	NSNumber * QuestionnaireId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_MemberQuiz *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSDate * DateTaken;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * MemberEid;
@property (retain) NSNumber * MemberScore;
@property (retain) USBoolean * Passed;
@property (retain) NSNumber * PassingScore;
@property (retain) NSNumber * QuestionnaireId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Vo2MaxActivityLevel : NSObject {
	
/* elements */
	tns4_ArrayOfKeyValuePairOfstringint * Answers;
	tns1_NuvitaStatus * ErrorStatus;
	NSString * Text;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Vo2MaxActivityLevel *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns4_ArrayOfKeyValuePairOfstringint * Answers;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSString * Text;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Vo2Max : NSObject {
	
/* elements */
	NSString * Date;
	tns1_NuvitaStatus * ErrorStatus;
	NSNumber * MeasurementTypeId;
	NSString * NormCategory;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Vo2Max *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Date;
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) NSNumber * MeasurementTypeId;
@property (retain) NSString * NormCategory;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ProposedCardioProgram : NSObject {
	
/* elements */
	NSNumber * CurrentMinutesInZone;
	NSNumber * CurrentTargetZonesHigh;
	NSNumber * CurrentTargetZonesLow;
	NSString * MemberId;
	NSNumber * NewMinutesInZone;
	NSNumber * NewTargetZonesHigh;
	NSNumber * NewTargetZonesLow;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ProposedCardioProgram *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSNumber * CurrentMinutesInZone;
@property (retain) NSNumber * CurrentTargetZonesHigh;
@property (retain) NSNumber * CurrentTargetZonesLow;
@property (retain) NSString * MemberId;
@property (retain) NSNumber * NewMinutesInZone;
@property (retain) NSNumber * NewTargetZonesHigh;
@property (retain) NSNumber * NewTargetZonesLow;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_Norm : NSObject {
	
/* elements */
	NSString * Label;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_Norm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Label;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfNorm : NSObject {
	
/* elements */
	NSMutableArray *Norm;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfNorm *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addNorm:(tns1_Norm *)toAdd;
@property (readonly) NSMutableArray * Norm;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_HealthTrend : NSObject {
	
/* elements */
	NSString * Date;
	NSNumber * Value;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_HealthTrend *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * Date;
@property (retain) NSNumber * Value;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfHealthTrend : NSObject {
	
/* elements */
	NSMutableArray *HealthTrend;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfHealthTrend *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addHealthTrend:(tns1_HealthTrend *)toAdd;
@property (readonly) NSMutableArray * HealthTrend;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_HealthTrendsSeries : NSObject {
	
/* elements */
	NSString * ChartName;
	NSNumber * ChartNumber;
	tns1_ArrayOfHealthTrend * HealthTrends;
	NSNumber * MeasurementTypeId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_HealthTrendsSeries *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) NSString * ChartName;
@property (retain) NSNumber * ChartNumber;
@property (retain) tns1_ArrayOfHealthTrend * HealthTrends;
@property (retain) NSNumber * MeasurementTypeId;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_ArrayOfHealthTrendsSeries : NSObject {
	
/* elements */
	NSMutableArray *HealthTrendsSeries;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_ArrayOfHealthTrendsSeries *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
- (void)addHealthTrendsSeries:(tns1_HealthTrendsSeries *)toAdd;
@property (readonly) NSMutableArray * HealthTrendsSeries;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface tns1_HealthTrendsChart : NSObject {
	
/* elements */
	tns1_NuvitaStatus * ErrorStatus;
	tns1_ArrayOfNorm * HealthTrendsNorms;
	tns1_ArrayOfHealthTrendsSeries * HealthTrendsSeriesSet;
	NSString * Label;
	NSNumber * MeasurementGroupId;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (tns1_HealthTrendsChart *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
/* elements */
@property (retain) tns1_NuvitaStatus * ErrorStatus;
@property (retain) tns1_ArrayOfNorm * HealthTrendsNorms;
@property (retain) tns1_ArrayOfHealthTrendsSeries * HealthTrendsSeriesSet;
@property (retain) NSString * Label;
@property (retain) NSNumber * MeasurementGroupId;
/* attributes */
- (NSDictionary *)attributes;
@end
