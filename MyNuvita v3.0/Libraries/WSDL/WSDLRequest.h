//
//  WSDLRequest.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/5/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@class WSDLRequestParameter;
@interface WSDLRequestParameter : NSObject
@property (assign) NSString *eid;
@property (assign) NSString *date;
@property (assign) NSString *skip;
@property (assign) NSString *take;
@property (assign) NSString *photo;
@property (assign) NSString *type;
@property (assign) NSString *dateTime;
@property (assign) NSString *lessonNumber;
@property (assign) NSString *answers;
@property (assign) NSString *message;
@property (assign) NSString *email;
@property (assign) NSString *password;
@property (assign) NSString *Id;
@property (assign) NSString *pageNumber;
@property (assign) NSString *pageSize;
@property (assign) NSString *memberID;
@property (assign) NSString *weekDate;
@property (assign) NSString *weekNumber;
@property (assign) NSString *memberId;
@property (assign) NSString *searchName;
@property (assign) NSString *friendEid;
@property (assign) NSString *accept;
@property (assign) NSString *messageText;
@property (assign) NSString *mealType;
@property (assign) NSDictionary *mobilityWeek;
@property (assign) NSDictionary *mealDetails;
@property (assign) NSDictionary *favoriteMealDetails;
@property (assign) NSDictionary *habitDetails;
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addElementsToNode:(xmlNodePtr)node;
@end

@class WSDLRequestBinding;
@interface WSDLRequest : NSObject
+ (WSDLRequestBinding *)WSDLBinding;
@end

@class WSDLRequestResponse;
@class WSDLRequestOperation;
@protocol WSDLRequestResponseDelegate <NSObject>
- (void)operation:(WSDLRequestOperation *)operation completedWithResponse:(WSDLRequestResponse *)response;
@end

@interface WSDLRequestBinding : NSObject <WSDLRequestResponseDelegate>
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) BOOL synchronousOperationComplete;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;
@property (retain, nonatomic) NSString *api;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(WSDLRequestOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (WSDLRequestResponse *)parseBindingRequestUsingParameter:(WSDLRequestParameter *)parameter;
@end

@interface WSDLRequestOperation : NSOperation{
    WSDLRequestBinding *binding;
    WSDLRequestResponse *response;
    id <WSDLRequestResponseDelegate> delegate;
    NSMutableData *responseData;
    NSURLConnection *urlConnection;
}

@property (retain) WSDLRequestBinding *binding;
@property (readonly) WSDLRequestResponse *response;
@property (nonatomic, retain) id <WSDLRequestResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(WSDLRequestBinding *)aBinding delegate:(id <WSDLRequestResponseDelegate> )aDelegate;
@end

@interface WSDLRequestBindingProcessRequest : WSDLRequestOperation
@property (retain) WSDLRequestParameter *parameters;
- (id)initWithBinding:(WSDLRequestBinding *)requestBinding
             delegate:(id <WSDLRequestResponseDelegate> )requestDelegate
           parameters:(WSDLRequestParameter *)requestParameters;
@end

@interface WSDLRequestBindingEnvelope : NSObject
+ (WSDLRequestBindingEnvelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end

@interface WSDLRequestResponse : NSObject
@property (retain) NSDictionary *status;
@property (retain) NSString *api;
@property (retain) NSArray *headers;
@property (retain) NSArray *bodyParts;
@property (retain) NSError *error;
@property (retain) NSDictionary *info;
@end
