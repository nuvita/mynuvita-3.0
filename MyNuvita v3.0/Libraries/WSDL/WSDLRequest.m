//
//  WSDLRequest.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/5/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//
#import "WSDLRequest.h"
#import <libxml/xmlstring.h>
#if TARGET_OS_IPHONE
#import <CFNetwork/CFNetwork.h>
#endif

@implementation WSDLRequestParameter
- (NSString *)nsPrefix {
    return @"";
}
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix {
    NSString *nodeName = nil;
    if(elNSPrefix != nil && [elNSPrefix length] > 0) {
        nodeName = [NSString stringWithFormat:@"%@:%@", elNSPrefix, elName];
    } else {
        nodeName = [NSString stringWithFormat:@"%@%@", @"", elName];
    }
    
    xmlNodePtr node = xmlNewDocNode(doc, NULL, [nodeName xmlString], NULL);
    [self addElementsToNode:node];
    return node;
}
- (void)addElementsToNode:(xmlNodePtr)node {
    if (_eid) {
        xmlNewChild(node, NULL, (const xmlChar *)"eid", [_eid xmlString]);
    }
    
    if (_weekDate) {
        xmlNewChild(node, NULL, (const xmlChar *)"weekDate", [_weekDate xmlString]);
    }
    
    if (_date) {
        xmlNewChild(node, NULL, (const xmlChar *)"date", [_date xmlString]);
    }
    
    if (_type) {
        xmlNewChild(node, NULL, (const xmlChar *)"type", [_type xmlString]);
    }
    
    if (_dateTime) {
       xmlNewChild(node, NULL, (const xmlChar *)"dateTime", [_dateTime xmlString]);
    }
    
    if (_lessonNumber) {
        xmlNewChild(node, NULL, (const xmlChar *)"lessonNumber", [_lessonNumber xmlString]);
    }
    
    if (_Id) {
        xmlNewChild(node, NULL, (const xmlChar *)"Id", [_Id xmlString]);
    }
    
    if (_answers) {
        xmlNewChild(node, NULL, (const xmlChar *)"answers", [_answers xmlString]);
    }
    
    if (_message) {
        xmlNewChild(node, NULL, (const xmlChar *)"message", [_message xmlString]);
    }
    
    if (_email) {
        xmlNewChild(node, NULL, (const xmlChar *)"email", [_email xmlString]);
    }
    
    if (_password) {
        xmlNewChild(node, NULL, (const xmlChar *)"password", [_password xmlString]);
    }
    
    if (_memberID) {
        xmlNewChild(node, NULL, (const xmlChar *)"memberID", [_memberID xmlString]);
    }
    
    if (_memberId) {
        xmlNewChild(node, NULL, (const xmlChar *)"memberId", [_memberId xmlString]);
    }
    
    if (_searchName) {
        xmlNewChild(node, NULL, (const xmlChar *)"searchName", [_searchName xmlString]);
    }
    
    if (_friendEid) {
        xmlNewChild(node, NULL, (const xmlChar *)"friendEid", [_friendEid xmlString]);
    }
    
    if (_messageText) {
        xmlNewChild(node, NULL, (const xmlChar *)"messageText", [_messageText xmlString]);
    }
    
    if (_accept) {
        xmlNewChild(node, NULL, (const xmlChar *)"Accept", [_accept xmlString]);
    }
    
    if (_weekNumber) {
        xmlNewChild(node, NULL, (const xmlChar *)"weekNumber", [_weekNumber xmlString]);
    }
    
    if (_skip) {
        xmlNewChild(node, NULL, (const xmlChar *)"skip", [_skip xmlString]);
    }
    
    if (_take) {
        xmlNewChild(node, NULL, (const xmlChar *)"take", [_take xmlString]);
    }
    
    if (_photo) {
        xmlNewChild(node, NULL, (const xmlChar *)"photo", [_photo xmlString]);
    }
    
    if (_pageNumber) {
        xmlNewChild(node, NULL, (const xmlChar *)"pageNumber", [_pageNumber xmlString]);
    }
    
    if (_pageSize) {
        xmlNewChild(node, NULL, (const xmlChar *)"pageSize", [_pageSize xmlString]);
    }
    
    if (_mobilityWeek) {
        [_mobilityWeek enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                xmlNodePtr childNode = xmlNewDocNode(node->doc, NULL, [key xmlString], NULL);
                xmlAddChild(node, childNode);
                
                for (NSString *childKey in [obj allKeys]) {
                    id object = [obj objectForKey:childKey];
                    xmlAddChild(childNode, [object xmlNodeForDoc:node->doc
                                                     elementName:[childKey stringByReplacingOccurrencesOfString:@"a:"
                                                                                                     withString:@""]
                                                 elementNSPrefix:nil]);
                }
            }
        }];
    }
    
    if (_mealDetails) {
        NSArray *keys = @[mIDKey, mDateTimeKey, mDescriptionKey, mRankKey, mTypeKey, mNumberKey, mFavoriteIdKey, mFavoriteEditModeKey, mPhotoKey];
        [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id object = _mealDetails[obj];
            if ([object isKindOfClass:[NSString class]]) {
                xmlAddChild(node, [object xmlNodeForDoc:node->doc elementName:obj]);
            }
        }];
    }
    
    if (_favoriteMealDetails) {
        NSArray *keys = @[mIDKey, mTypeSmallKey];
        [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id object = _favoriteMealDetails[obj];
            if ([object isKindOfClass:[NSString class]]) {
                xmlAddChild(node, [object xmlNodeForDoc:node->doc elementName:obj]);
            }
        }];
    }
    
    if (_habitDetails) {
        NSArray *keys = @[@"eid", @"date", @"habitId", @"note", @"hearts", @"action"];
        [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            id object = _habitDetails[obj];
            if ([object isKindOfClass:[NSString class]]) {
                xmlAddChild(node, [object xmlNodeForDoc:node->doc elementName:obj]);
            }
        }];
    }
    
//    public NuvitaStatus saveNutrtionHabitProgress(string eid, string date, int habitId, string note, int hearts, string action) {
//        actions include: “save”, “update” and “delete”
}

@end

@implementation WSDLRequest
+ (void)initialize {
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"xs" forKey:@"http://www.w3.org/2001/XMLSchema"];
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"MobileServiceSvc" forKey:@"http://tempuri.org/"];
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"ns1" forKey:@"/Imports"];
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"tns1" forKey:@"http://schemas.datacontract.org/2004/07/NuvitaMobile.wsClasses"];
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"tns2" forKey:@"http://schemas.microsoft.com/2003/10/Serialization/"];
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"tns3" forKey:@"http://schemas.microsoft.com/2003/10/Serialization/Arrays"];
    [[USGlobals sharedInstance].wsdlStandardNamespaces setObject:@"tns4" forKey:@"http://schemas.datacontract.org/2004/07/System.Collections.Generic"];
}
+ (WSDLRequestBinding *)WSDLBinding {
    return [[WSDLRequestBinding alloc] initWithAddress:@"https://ws.mynuvita.com/mobile/MobileService.svc"];
}
@end
@implementation WSDLRequestBinding
- (id)init {
    if((self = [super init])) {
        _address = nil;
        _cookies = nil;
        _defaultTimeout = 10;//seconds
        _logXMLInOut = NO;
        _synchronousOperationComplete = NO;
    }
    
    return self;
}
- (id)initWithAddress:(NSString *)anAddress {
    if((self = [self init])) {
        _address = [NSURL URLWithString:anAddress];
    }
    
    return self;
}
- (void)addCookie:(NSHTTPCookie *)toAdd {
    if(toAdd != nil) {
        if(_cookies == nil)
            _cookies = [[NSMutableArray alloc] init];
        [_cookies addObject:toAdd];
    }
}
- (WSDLRequestResponse *)performSynchronousOperation:(WSDLRequestOperation *)operation {
    _synchronousOperationComplete = NO;
    [operation start];
    
    // Now wait for response
    NSRunLoop *theRL = [NSRunLoop currentRunLoop];
    while (!_synchronousOperationComplete && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
    return operation.response;
}
- (void)performAsynchronousOperation:(WSDLRequestOperation *)operation {
    [operation start];
}
- (void) operation:(WSDLRequestOperation *)operation completedWithResponse:(WSDLRequestResponse *)response {
    _synchronousOperationComplete = YES;
}

- (WSDLRequestResponse *)parseBindingRequestUsingParameter:(WSDLRequestParameter *)parameter {
    return [self performSynchronousOperation:[[WSDLRequestBindingProcessRequest alloc] initWithBinding:self
                                                                                              delegate:self
                                                                                            parameters:parameter]];
}

- (void)sendHTTPCallUsingBody:(NSString *)outputBody soapAction:(NSString *)soapAction forOperation:(WSDLRequestOperation *)operation {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:_address
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:_defaultTimeout];
    NSData *bodyData = [outputBody dataUsingEncoding:NSUTF8StringEncoding];
    
    if(_cookies != nil) {
        [request setAllHTTPHeaderFields:[NSHTTPCookie requestHeaderFieldsWithCookies:_cookies]];
    }
    
    [request setValue:@"wsdl2objc" forHTTPHeaderField:@"User-Agent"];
    [request setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%u", [bodyData length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:self.address.host forHTTPHeaderField:@"Host"];
    [request setHTTPMethod: @"POST"];
    // set version 1.1 - how?
    [request setHTTPBody: bodyData];
    
//    if(self.logXMLInOut) {
//        NSLog(@"OutputHeaders:\n%@", [request allHTTPHeaderFields]);
//        NSLog(@"OutputBody:\n%@", outputBody);
//    }

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:operation];
    operation.urlConnection = connection;
}
@end
@implementation WSDLRequestOperation
@synthesize binding;
@synthesize response;
@synthesize delegate;
@synthesize responseData;
@synthesize urlConnection;
- (id)initWithBinding:(WSDLRequestBinding *)aBinding delegate:(id <WSDLRequestResponseDelegate> )aDelegate {
    if ((self = [super init])) {
        self.binding = aBinding;
        response = nil;
        self.delegate = aDelegate;
        self.responseData = nil;
        self.urlConnection = nil;
    }
    
    return self;
}
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
        newCredential=[NSURLCredential credentialWithUser:self.binding.authUsername
                                                 password:self.binding.authPassword
                                              persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"Authentication Error" forKey:NSLocalizedDescriptionKey];
        NSError *authError = [NSError errorWithDomain:@"Connection Authentication" code:0 userInfo:userInfo];
        [self connection:connection didFailWithError:authError];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)urlResponse {
    NSHTTPURLResponse *httpResponse;
    if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
        httpResponse = (NSHTTPURLResponse *) urlResponse;
    } else {
        httpResponse = nil;
    }
    
//    if(binding.logXMLInOut && ![binding.api isEqualToString:@"GetProgramType"]) {
//        NSLog(@"ResponseStatus: %ld\n", (long)[httpResponse statusCode]);
//        NSLog(@"ResponseHeaders:\n%@", [httpResponse allHeaderFields]);
//    }
    
    NSMutableArray *cookies = [[NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields]
                                                                      forURL:binding.address] mutableCopy];
    binding.cookies = cookies;
    if ([urlResponse.MIMEType rangeOfString:@"text/xml"].length == 0) {
        NSError *error = nil;
        [connection cancel];
        if ([httpResponse statusCode] >= 400) {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]] forKey:NSLocalizedDescriptionKey];
            
            error = [NSError errorWithDomain:@"BasicHttpBinding_IMobileServiceBindingResponseHTTP" code:[httpResponse statusCode] userInfo:userInfo];
        } else {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:
                                      [NSString stringWithFormat: @"Unexpected response MIME type to SOAP call:%@", urlResponse.MIMEType]
                                                                 forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"BasicHttpBinding_IMobileServiceBindingResponseHTTP" code:1 userInfo:userInfo];
        }
        
        [self connection:connection didFailWithError:error];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (responseData == nil) {
        responseData = [data mutableCopy];
    } else {
        [responseData
         appendData:data];
    }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (binding.logXMLInOut) {
        NSLog(@"ResponseError:\n%@", error);
    }
    
    response.error = error;
    [delegate operation:self completedWithResponse:response];
}
@end

@implementation WSDLRequestBindingProcessRequest

- (id)initWithBinding:(WSDLRequestBinding *)requestBinding
             delegate:(id<WSDLRequestResponseDelegate>)requestDelegate
           parameters:(WSDLRequestParameter *)requestParameters {
    
    if (self = [super initWithBinding:requestBinding delegate:requestDelegate]) {
        _parameters = requestParameters;
    }
    
    return self;
}

- (void)main {
    response = [WSDLRequestResponse new];
    response.api = binding.api;
    WSDLRequestBindingEnvelope *envelope = [WSDLRequestBindingEnvelope sharedInstance];
    
    NSMutableDictionary *headerElements = nil;
    headerElements = [NSMutableDictionary dictionary];
    NSMutableDictionary *bodyElements = nil;
    bodyElements = [NSMutableDictionary dictionary];
    if (_parameters != nil)
        [bodyElements setObject:self.parameters
                         forKey:binding.api];
    
    NSString *operationXMLString = [envelope serializedFormUsingHeaderElements:headerElements
                                                                  bodyElements:bodyElements];
    NSLog(@"xml: %@", operationXMLString);
    [binding sendHTTPCallUsingBody:operationXMLString
                        soapAction:[NSString stringWithFormat:@"urn:IMobileService/%@", binding.api]
                      forOperation:self];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (responseData != nil && delegate != nil) {
        if (binding.logXMLInOut) {
            NSDictionary *data = [NSDictionary dictionaryWithXMLData:responseData];
//            NSLog(@"data: %@", data);
            response.info = [data dictionaryValueForKeyPath:[NSString stringWithFormat:@"s:Envelope.s:Body.%@Response.%@Result", binding.api, binding.api]];
            response.status = response.info[kInfoErrorStatusKey];
            [delegate operation:self completedWithResponse:response];
        }
    }
}

@end

static WSDLRequestBindingEnvelope *wsdlRequestBindingEnvelopeSharedInstance = nil;
@implementation WSDLRequestBindingEnvelope
+ (WSDLRequestBindingEnvelope *)sharedInstance {
    if(wsdlRequestBindingEnvelopeSharedInstance == nil) {
        wsdlRequestBindingEnvelopeSharedInstance = [WSDLRequestBindingEnvelope new];
    }
    return wsdlRequestBindingEnvelopeSharedInstance;
}
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements {
    xmlDocPtr doc;
    
    doc = xmlNewDoc((const xmlChar*)XML_DEFAULT_VERSION);
    if (doc == NULL) {
        NSLog(@"Error creating the xml document tree");
        return @"";
    }

    xmlNodePtr root = xmlNewDocNode(doc, NULL, (const xmlChar*)"Envelope", NULL);
    xmlDocSetRootElement(doc, root);
    
    xmlNsPtr soapEnvelopeNs = xmlNewNs(root, (const xmlChar*)"http://schemas.xmlsoap.org/soap/envelope/", (const xmlChar*)"soap");
    xmlSetNs(root, soapEnvelopeNs);
    
    xmlNsPtr xslNs = xmlNewNs(root, (const xmlChar*)"http://www.w3.org/1999/XSL/Transform", (const xmlChar*)"xsl");
    xmlNewNs(root, (const xmlChar*)"http://www.w3.org/2001/XMLSchema-instance", (const xmlChar*)"xsi");
    
    xmlNewNsProp(root, xslNs, (const xmlChar*)"version", (const xmlChar*)"1.0");
    
    xmlNewNs(root, (const xmlChar*)"http://www.w3.org/2001/XMLSchema", (const xmlChar*)"xs");
    xmlNewNs(root, (const xmlChar*)"http://tempuri.org/", (const xmlChar*)"MobileServiceSvc");
    xmlNewNs(root, (const xmlChar*)"/Imports", (const xmlChar*)"ns1");
    xmlNewNs(root, (const xmlChar*)"http://schemas.datacontract.org/2004/07/NuvitaMobile.wsClasses", (const xmlChar*)"tns1");
    xmlNewNs(root, (const xmlChar*)"http://schemas.microsoft.com/2003/10/Serialization/", (const xmlChar*)"tns2");
    xmlNewNs(root, (const xmlChar*)"http://schemas.microsoft.com/2003/10/Serialization/Arrays", (const xmlChar*)"tns3");
    xmlNewNs(root, (const xmlChar*)"http://schemas.datacontract.org/2004/07/System.Collections.Generic", (const xmlChar*)"tns4");

    if((headerElements != nil) && ([headerElements count] > 0)) {
        xmlNodePtr headerNode = xmlNewDocNode(doc, soapEnvelopeNs, (const xmlChar*)"Header", NULL);
        xmlAddChild(root, headerNode);
        
        for(NSString *key in [headerElements allKeys]) {
            id header = [headerElements objectForKey:key];
            xmlAddChild(headerNode, [header xmlNodeForDoc:doc elementName:key elementNSPrefix:nil]);
        }
    }
    
    if((bodyElements != nil) && ([bodyElements count] > 0)) {
        xmlNodePtr bodyNode = xmlNewDocNode(doc, soapEnvelopeNs, (const xmlChar*)"Body", NULL);
        xmlAddChild(root, bodyNode);
        
        for(NSString *key in [bodyElements allKeys]) {
            id body = [bodyElements objectForKey:key];
            xmlAddChild(bodyNode, [body xmlNodeForDoc:doc elementName:key elementNSPrefix:nil]);
        }
    }
    
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    
    NSString *serializedForm = [NSString stringWithCString:(const char*)buf encoding:NSUTF8StringEncoding];
    xmlFree(buf);
    
    xmlFreeDoc(doc);
    return serializedForm;
}
@end
@implementation WSDLRequestResponse
- (id)init {
    if((self = [super init])) {
        _headers = nil;
        _bodyParts = nil;
        _info = nil;
        _error = nil;
    }
    
    return self;
}
@end



