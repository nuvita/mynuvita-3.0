//
//  CrashHandler.h
//  CrashHandler

#import "Globals.h"

@interface CrashHandler : NSObject 
	
+ (void) setupLogging:(BOOL)shouldEnableCrashHandler;
@end

void CrashHandlerExceptionHandler(NSException *exception);