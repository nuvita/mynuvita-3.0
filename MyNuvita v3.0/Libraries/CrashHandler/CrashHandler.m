//
//  CrashHandler.m
//  CrashHandler


#import "CrashHandler.h"
#import <sys/utsname.h>

#define MODEL @"model"
#define NAME @"name"
#define VERSION @"os_version"
#define BUILD_VERSION @"CFBundleShortVersionString"
#define BUILD_NUMBER @"CFBundleVersion"
#define UNCAUGHT_EXCEPTION @"uncaught exception"
#define WAIT_TIME 5

//NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
//NSString *appVersion = [infoDict objectForKey:@]; // example: 1.0.0
//NSNumber *buildNumber = [infoDict objectForKey:@];

static BOOL _shouldEnableCrashHandler = NO;

@implementation CrashHandler

+ (void) setupLogging:(BOOL)shouldEnableCrashHandler{
	
	NSSetUncaughtExceptionHandler (&CrashHandlerExceptionHandler);
	_shouldEnableCrashHandler = shouldEnableCrashHandler;
}

+ (NSString *)machineName {
    struct utsname systemInfo;
    uname(&systemInfo);

    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

// Method to get device info
+ (NSMutableDictionary *) getDeviceInfo {
	
	NSMutableDictionary *deviceInfo = [[NSMutableDictionary alloc] init];
	[deviceInfo setValue:[self machineName] forKey:MODEL];
	[deviceInfo setValue:[[UIDevice currentDevice] name] forKey:NAME];
	[deviceInfo setValue:[[UIDevice currentDevice] systemVersion] forKey:VERSION];
	return deviceInfo;
}

// Method which send crash report to Parse server
+(void) reportCrash:(NSString *) name andReason:(NSString *)reason andStackTrace:(NSString *)stackTrace {

    NSLog(@"reportCrash");
    NSMutableDictionary *deviceInfo = [CrashHandler getDeviceInfo];
    NSDictionary *buildInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *report = [NSString stringWithFormat:@"\n\n//Created by John Bariquit\n\nDevice Name: %@\nDevice Model: %@\niOS Version: %@\nBuild Version: %@\nBuild Number: %@\nDate: %@\n\nException: %@\nReason: %@\nTrace: %@", [deviceInfo valueForKey:NAME], [deviceInfo valueForKey:MODEL], [deviceInfo valueForKey:VERSION], [buildInfo objectForKey:BUILD_VERSION], [buildInfo objectForKey:BUILD_NUMBER], [NSDate date], name, reason, stackTrace];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];

    // create if needed
    if (![[NSFileManager defaultManager] fileExistsAtPath:logPath]){
        fprintf(stderr,"Creating file at %s",[logPath UTF8String]);
        [[NSData data] writeToFile:logPath atomically:YES];
    }

    // append
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:logPath];
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    [handle writeData:[report dataUsingEncoding:NSUTF8StringEncoding]];
    [handle closeFile];

    // prompt
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:kCrashLogReportKey];
    [defaults synchronize];
}

@end

void CrashHandlerExceptionHandler(NSException *exception) {
	
	NSArray *arr = [exception callStackSymbols];
	NSString *report = [arr componentsJoinedByString:@"\n"];
	
	if (_shouldEnableCrashHandler) {
		[CrashHandler reportCrash:exception.name andReason:exception.reason andStackTrace:report];
	}

	NSDate *date = [[NSDate date] dateByAddingTimeInterval:WAIT_TIME];
	while ([date compare:[NSDate date]] == NSOrderedDescending) {
		
		[[NSRunLoop currentRunLoop] runUntilDate:[NSDate date]];
	}
}

