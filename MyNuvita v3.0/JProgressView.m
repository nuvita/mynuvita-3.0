//
//  JProgressView.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/14/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "JProgressView.h"

@implementation JProgressView {
    
    float startAngle;
    float endAngle;
}

typedef void (^voidBlock)(void);
typedef float (^floatfloatBlock)(float);
typedef UIColor * (^floatColorBlock)(float);

//- (instancetype)initWithFrame:(CGRect)frame {
//    if (self == [super initWithFrame:frame]) {
//        [_percents removeAllObjects];
//        _percents = nil;
//    }
//    
//    return self;
//}


- (CGPoint) pointForTrapezoidWithAngle:(float)a andRadius:(float)r  forCenter:(CGPoint)p {
    return CGPointMake(p.x + r*cos(a), p.y + r*sin(a));
}

- (UIColor *)colorAtRangeIndex:(int)index {
    __block UIColor *color = nil;
    [_percents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSRange range = [obj[@"range"] rangeValue];
        if (NSLocationInRange(index, range)) {
            color = obj[kElementColorKey];
        }
    }];
    
    return color;
}

- (void) drawMultipleColorsInContext:(CGContextRef)ctx  startingAngle:(float)a endingAngle:(float)b percentAngle:(float)c intRadius:(floatfloatBlock)intRadiusBlock outRadius:(floatfloatBlock)outRadiusBlock withGradientBlock:(floatColorBlock)colorBlock withSubdiv:(int)subdivCount withCenter:(CGPoint)center withScale:(float)scale withBorderColor:(UIColor*)borderColor {
 
    float angleDelta = (b - a) / subdivCount;
    float fractionDelta = 1.0 / subdivCount;
    
    CGPoint p0, p1, p2, p3, p4, p5;
    float currentAngle = a;
    
    p4 = p0 = [self pointForTrapezoidWithAngle:currentAngle andRadius:intRadiusBlock(0) forCenter:center];
    p5 = p3 = [self pointForTrapezoidWithAngle:currentAngle andRadius:outRadiusBlock(0) forCenter:center];
    
    CGMutablePathRef innerEnveloppe = CGPathCreateMutable(),
    outerEnveloppe = CGPathCreateMutable();
    
    CGPathMoveToPoint(outerEnveloppe, 0, p3.x, p3.y);
    CGPathMoveToPoint(innerEnveloppe, 0, p0.x, p0.y);
    CGContextSaveGState(ctx);
    CGContextSetLineWidth(ctx, 1);
    
    for (int i = 0; i < subdivCount; i ++) {
        
        float fraction = (float)i / subdivCount;
        currentAngle = a + fraction * (b - a);
        
        CGMutablePathRef trapezoid = CGPathCreateMutable();
        
        p1 = [self pointForTrapezoidWithAngle:currentAngle+angleDelta andRadius:intRadiusBlock(fraction+fractionDelta) forCenter:center];
        p2 = [self pointForTrapezoidWithAngle:currentAngle+angleDelta andRadius:outRadiusBlock(fraction+fractionDelta) forCenter:center];
        
        CGPathMoveToPoint(trapezoid, 0, p0.x, p0.y);
        CGPathAddLineToPoint(trapezoid, 0, p1.x, p1.y);
        CGPathAddLineToPoint(trapezoid, 0, p2.x, p2.y);
        CGPathAddLineToPoint(trapezoid, 0, p3.x, p3.y);
        CGPathCloseSubpath(trapezoid);
        
        CGPoint centerofTrapezoid = CGPointMake((p0.x+p1.x+p2.x+p3.x)/4, (p0.y+p1.y+p2.y+p3.y)/4);
        
        CGAffineTransform t = CGAffineTransformMakeTranslation(-centerofTrapezoid.x, -centerofTrapezoid.y);
        CGAffineTransform s = CGAffineTransformMakeScale(scale, scale);
        CGAffineTransform concat = CGAffineTransformConcat(t, CGAffineTransformConcat(s, CGAffineTransformInvert(t)));
        CGPathRef scaledPath = CGPathCreateCopyByTransformingPath(trapezoid, &concat);
        
        CGContextAddPath(ctx, scaledPath);
        CGContextSetMiterLimit(ctx, 0);
        
        UIColor *color = [self colorAtRangeIndex:i] ? [self colorAtRangeIndex:i] : [UIColor clearColor];
        CGContextSetFillColorWithColor(ctx, color.CGColor);
        CGContextSetStrokeColorWithColor(ctx, color.CGColor);
        
        CGContextDrawPath(ctx, kCGPathFillStroke);
        CGPathRelease(trapezoid);
        p0 = p1;
        p3 = p2;
        
        CGPathAddLineToPoint(outerEnveloppe, 0, p3.x, p3.y);
        CGPathAddLineToPoint(innerEnveloppe, 0, p0.x, p0.y);
    }
    
    CGContextSetLineWidth(ctx, 1.0f);
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextAddPath(ctx, outerEnveloppe);
    CGContextAddPath(ctx, innerEnveloppe);
    CGContextMoveToPoint(ctx, p0.x, p0.y);
    CGContextAddLineToPoint(ctx, p3.x, p3.y);
    CGContextMoveToPoint(ctx, p4.x, p4.y);
    CGContextAddLineToPoint(ctx, p5.x, p5.y);
    CGContextStrokePath(ctx);
}

- (void) drawColorInContext:(CGContextRef)ctx  startingAngle:(float)a endingAngle:(float)b percentAngle:(float)c intRadius:(floatfloatBlock)intRadiusBlock outRadius:(floatfloatBlock)outRadiusBlock withGradientBlock:(floatColorBlock)colorBlock withSubdiv:(int)subdivCount withCenter:(CGPoint)center withScale:(float)scale withBorderColor:(UIColor*)borderColor {
    
    float angleDelta = (b - a) / subdivCount;
    float fractionDelta = 1.0 / subdivCount;
    
    CGPoint p0, p1, p2, p3, p4, p5;
    float currentAngle = a;
    
    p4 = p0 = [self pointForTrapezoidWithAngle:currentAngle andRadius:intRadiusBlock(0) forCenter:center];
    p5 = p3 = [self pointForTrapezoidWithAngle:currentAngle andRadius:outRadiusBlock(0) forCenter:center];
    
    CGMutablePathRef innerEnveloppe = CGPathCreateMutable(),
    outerEnveloppe = CGPathCreateMutable();
    
    CGPathMoveToPoint(outerEnveloppe, 0, p3.x, p3.y);
    CGPathMoveToPoint(innerEnveloppe, 0, p0.x, p0.y);
    CGContextSaveGState(ctx);
    CGContextSetLineWidth(ctx, 1);
    
    for (int i = 0; i < subdivCount; i ++) {
        
        float fraction = (float)i / subdivCount;
        currentAngle = a + fraction * (b - a);
        
        CGMutablePathRef trapezoid = CGPathCreateMutable();
        
        p1 = [self pointForTrapezoidWithAngle:currentAngle+angleDelta andRadius:intRadiusBlock(fraction+fractionDelta) forCenter:center];
        p2 = [self pointForTrapezoidWithAngle:currentAngle+angleDelta andRadius:outRadiusBlock(fraction+fractionDelta) forCenter:center];
        
        CGPathMoveToPoint(trapezoid, 0, p0.x, p0.y);
        CGPathAddLineToPoint(trapezoid, 0, p1.x, p1.y);
        CGPathAddLineToPoint(trapezoid, 0, p2.x, p2.y);
        CGPathAddLineToPoint(trapezoid, 0, p3.x, p3.y);
        CGPathCloseSubpath(trapezoid);
        
        CGPoint centerofTrapezoid = CGPointMake((p0.x+p1.x+p2.x+p3.x)/4, (p0.y+p1.y+p2.y+p3.y)/4);
        
        CGAffineTransform t = CGAffineTransformMakeTranslation(-centerofTrapezoid.x, -centerofTrapezoid.y);
        CGAffineTransform s = CGAffineTransformMakeScale(scale, scale);
        CGAffineTransform concat = CGAffineTransformConcat(t, CGAffineTransformConcat(s, CGAffineTransformInvert(t)));
        CGPathRef scaledPath = CGPathCreateCopyByTransformingPath(trapezoid, &concat);
        
        CGContextAddPath(ctx, scaledPath);
        CGContextSetFillColorWithColor(ctx, borderColor.CGColor);
        CGContextSetStrokeColorWithColor(ctx, borderColor.CGColor);
        CGContextSetMiterLimit(ctx, 0);
        
        if (currentAngle >= c) {
            
            CGContextSetFillColorWithColor(ctx, [UIColor clearColor].CGColor);
            CGContextSetStrokeColorWithColor(ctx, [UIColor clearColor].CGColor);
        }
        
        CGContextDrawPath(ctx, kCGPathFillStroke);
        CGPathRelease(trapezoid);
        p0 = p1;
        p3 = p2;
        
        CGPathAddLineToPoint(outerEnveloppe, 0, p3.x, p3.y);
        CGPathAddLineToPoint(innerEnveloppe, 0, p0.x, p0.y);
    }
    
    CGContextSetLineWidth(ctx, 1.0f);
    CGContextSetLineJoin(ctx, kCGLineJoinRound);
    CGContextSetStrokeColorWithColor(ctx, borderColor.CGColor);
    CGContextAddPath(ctx, outerEnveloppe);
    CGContextAddPath(ctx, innerEnveloppe);
    CGContextMoveToPoint(ctx, p0.x, p0.y);
    CGContextAddLineToPoint(ctx, p3.x, p3.y);
    CGContextMoveToPoint(ctx, p4.x, p4.y);
    CGContextAddLineToPoint(ctx, p5.x, p5.y);
    CGContextStrokePath(ctx);
}

- (CGSize)stringSize:(NSString *)str withFontAttribute:(NSDictionary *)font {
    NSString *myString = str;
    CGSize myStringSize = [myString sizeWithAttributes:font];
    return myStringSize;
}

- (UIColor *)getColorValue:(NSString *)value {
    if ([value isKindOfClass:[NSString class]]) {
        UIColor *color = kColorNuvitaGray;
        if ([value isEqualToString:@"Red"]) {
            color = kColorNuvitaRed;
        } else if ([value isEqualToString:@"Blue"]) {
            color = kColorNuvitaBlue;
        } else if ([value isEqualToString:@"Green"]) {
            color = kColorNuvitaGreen;
        } else if ([value isEqualToString:@"Orange"]) {
            color = kColorNuvitaOrange;
        }
        
        return color;
    }
    
    
    return nil;
}


- (UIColor *)getObjectColor:(NSDictionary *)info {
//    JLog(@"NI SUD PA DIRI ANG ANIMAL!!!!: %@", info);
    
    if ([info isKindOfClass:[NSDictionary class]]) {
        UIColor *color = kColorNuvitaGray;
        if ([[info objectForKey:@"a:Color"] isEqualToString:@"Red"]) {
            color = kColorNuvitaRed;
        } else if ([[info objectForKey:@"a:Color"] isEqualToString:@"Blue"]) {
            color = kColorNuvitaBlue;
        } else if ([[info objectForKey:@"a:Color"] isEqualToString:@"Green"]) {
            color = kColorNuvitaGreen;
        } else if ([[info objectForKey:@"a:Color"] isEqualToString:@"Orange"]) {
            color = kColorNuvitaOrange;
        }
        
//        JLog(@"RETURN: %@", color);
        return color;
    }
    
    
    return nil;
}

- (void)assigningValues:(NSArray *)values completion:(void(^)(NSArray *values))completion {
    NSMutableArray *assigned = [NSMutableArray arrayWithArray:values];
    __block CGFloat offset = 0;
    [values enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:obj];
        data[kElementColorKey] = [self getColorValue:obj[@"a:Color"]];
        
        CGFloat length = [obj[kElementOverAllKey] floatValue] / 100 * 256;
        data[@"range"] = [NSValue valueWithRange:(NSRange){offset, length}];
        [assigned replaceObjectAtIndex:idx withObject:data];
    }];
    
    if (completion) {
        completion(assigned);
    }
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();

    startAngle = M_PI;
    endAngle = startAngle + (M_PI * 1);
    
    float scaleWidth = (rect.size.width - 2) * 0.50;
    float scaleDiameter = scaleWidth * .75;
    
    NSDictionary *fonts = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:scaleDiameter * .25],
                            NSForegroundColorAttributeName : _progressTintColor};
    
    CGPoint center = CGPointMake(rect.size.width / 2, rect.size.height - 5);
    
    
//    JLog(@"percent: %@", _percents);
    if (_multiple) {
        
        if (_percents && [_percents isKindOfClass:[NSArray class]]) {
//            JLog(@"raw percents: %@", _percents);
            [self assigningValues:_percents completion:^(NSArray *values) {
                _percents = values;
//                JLog(@"percents: %@", _percents);
                [self drawMultipleColorsInContext:ctx
                                    startingAngle:startAngle
                                      endingAngle:endAngle
                                     percentAngle:(endAngle - startAngle) * (_percent / 100.0) + startAngle
                 
                                        intRadius:^float(float f) {
                                            return scaleWidth;
                                        } outRadius:^float(float f) {
                                            return scaleDiameter;
                                        } withGradientBlock:^UIColor *(float f) {
                                            return [UIColor clearColor];
                                        } withSubdiv:256
                 
                                       withCenter:center
                                        withScale:1
                                  withBorderColor:_progressTintColor];
                
                [_progressName drawAtPoint:CGPointMake(0, 0) withAttributes:fonts];
                NSString *string = [NSString stringWithFormat:@"%d%%", (int)_percent];
                CGSize size = [self stringSize:string withFontAttribute:fonts];
                [string drawAtPoint:CGPointMake(center.x - (size.width / 2), rect.size.height - size.height - 5) withAttributes:fonts];
                
            }];
        }
        
        
        
//        NSMutableArray *values = [NSMutableArray arrayWithArray:_percents];
//        if (values && [values count] > 0) {
//            __block CGFloat offset = 0;
//            [values enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                if ([obj isKindOfClass:[NSDictionary class]]) {
//                    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:obj];
//                    UIColor *color = [self getObjectColor:obj];
//                    [data setObject:color forKey:kElementColorKey];
//                    
//                    CGFloat length = [obj[kElementOverAllKey] floatValue] / 100 * 256;
//                    [data setObject:[NSValue valueWithRange:(NSRange){offset, length}] forKey:@"range"];
//                    
//                    [values replaceObjectAtIndex:idx withObject:data];
//                    offset += length;
//                }
//            }];
//        }
//        
//        _percents = values;
//        JLog(@"percents: %@", _percents);
//        if (_percents && [_percents count] > 0) {
//        
//
//        }
        
    } else {
        
        //...draw
        [self drawColorInContext:ctx
                      startingAngle:startAngle
                        endingAngle:endAngle
                       percentAngle:(endAngle - startAngle) * (_percent / 100.0) + startAngle
                          intRadius:^float(float f) {
                              return scaleWidth;
                          } outRadius:^float(float f) {
                              return scaleDiameter;
                          } withGradientBlock:^UIColor *(float f) {
                              return [UIColor clearColor];
                          } withSubdiv:256 withCenter:center withScale:1 withBorderColor:_progressTintColor];
        
        //...label
        [_progressName drawAtPoint:CGPointMake(0, 0) withAttributes:fonts];
        
        fonts = @{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:scaleDiameter * .35],
                  NSForegroundColorAttributeName : _progressTintColor};
        NSString *string = [NSString stringWithFormat:@"%d%%", (int)_percent];
        CGSize size = [self stringSize:string withFontAttribute:fonts];
        [string drawAtPoint:CGPointMake(center.x - (size.width / 2), rect.size.height - size.height - 5) withAttributes:fonts];
    
    }

    
}

@end
