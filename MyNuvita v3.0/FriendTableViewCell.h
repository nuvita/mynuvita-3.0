//
//  FriendTableViewCell.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/9/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"
#import "JProgressView.h"

@class FriendTableViewCell;
@protocol FriendTableViewCellDelegate <NSObject>

- (void)tapCheckmarkCell:(FriendTableViewCell *)cell;
- (void)removeCell:(FriendTableViewCell *)cell;

@end

@interface FriendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckbox;

@property (retain, nonatomic) NSDictionary *data;

@property (retain, nonatomic) id <FriendTableViewCellDelegate> delegate;

- (void)loadSettings;
- (void)getUserCellData:(NSDictionary *)details atDate:(NSDate *)date;

@end
