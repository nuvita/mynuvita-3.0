//
//  QuizViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/15/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "QuizViewController.h"

@interface QuizViewController ()

@property (retain, nonatomic) NSDictionary *currentQuestion;
@property (retain, nonatomic) NSMutableArray *listCheck;
@property (assign) NSInteger page;

@end

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"questions: %@", _questions);
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self loadQuestionAtPage:_page];
}

#pragma mark - Public Methods

- (void)didTapBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)moveNextClicked:(id)sender {
    _page += 1;
    [self loadQuestionAtPage:_page];
}

- (void)movePrevClicked:(id)sender {
    _page -= 1;
    [self loadQuestionAtPage:_page];
}

- (void)loadSettings {
    JBCustomButton *backButton =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 70, 30)];
    backButton.backgroundColor = [UIColor clearColor];
    backButton.tintColor = kColorNuvitaBlue;
    [backButton setButtonStyle:JBCustomButtonArrowTextLeftStyle];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton addTarget:self
                   action:@selector(didTapBack:)
         forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    buttonPrev.backgroundColor = [UIColor clearColor];
    buttonPrev.tintColor = kColorNuvitaBlue;
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                                [[UIBarButtonItem alloc] initWithCustomView:buttonPrev]];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    _page = 0;
    
    //... set defaults
    _listCheck = [[NSMutableArray alloc] init];
    for (int i = 0; i < [_questions count]; i ++) {
        [_listCheck addObject:@NO];
    }
    
    NSLog(@"list: %@", _listCheck);
}

- (NSString *)getAnswers {
    __block NSString *answers = @"";
    [_questions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *aid = obj[@"a:QuestionId"];
        NSString *selected = @"";
        for (NSDictionary *option in obj[kQuestionOptionKey][kOptionKey]) {
            if ([option[kOptionSelectedKey] boolValue]) {
                if ([selected length] == 0) {
                    selected = option[kOptionIDKey];
                }else {
                    selected = [selected stringByAppendingString:[NSString stringWithFormat:@",%@", option[kOptionIDKey]]];
                }
            }
            
            
        }
        
        if ([answers length] == 0) {
            answers = [NSString stringWithFormat:@"%@=%@", aid,selected];
        } else {
            answers = [answers stringByAppendingString:[NSString stringWithFormat:@"|%@=%@", aid,selected]];
        }
    }];
    
    return answers;
}

- (void)didTapSubmit {
    [JBLoadingView startAnimating];
    
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPISaveQuestionnaire;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.weekDate = [[NSDate date] description];
    parameter.answers = [NSString stringWithFormat:@"%@", [self getAnswers]];
    parameter.lessonNumber = [[loginInfo objectForKey:kAPIGetCoachLesson] objectForKey:kLessonNumberKey];
    parameter.weekNumber = [[loginInfo objectForKey:@"CoachInfo"] objectForKey:@"a:WeekNumber"];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self SaveQuestionnaireDidFinishWithResponse:response];
    });
}

- (void)loadQuestionAtPage:(NSInteger)page {
    NSArray *items = self.navigationItem.rightBarButtonItems;
    [[items lastObject] setEnabled:YES];
    [[items firstObject] setEnabled:YES];
    if (_page == 0)
        [[items lastObject] setEnabled:NO];
    if (_page == [_questions count] - 1) {
        [[items firstObject] setEnabled:NO];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Submit"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(didTapSubmit)];
    }
    
    _currentQuestion = [_questions objectAtIndex:page];
    [_tableView reloadData];

    _lblTitle.text = [NSString stringWithFormat:@"Question %d of %d", (int)(_page + 1), (int)[_questions count]];
}

- (void)loadResultView:(NSDictionary *)info {
    __block NSInteger count = 0;
    [_listCheck enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj boolValue]) {
            count += 1;
        }
    }];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:info];
    [data setObject:[NSNumber numberWithInteger:[_questions count]] forKey:@"numberOfQuestion"];
    [data setObject:[NSNumber numberWithInteger:count] forKey:@"numberOfCheck"];
    ResultViewController *viewController = (ResultViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                  bundle:[NSBundle mainBundle]]
                                                                    instantiateViewControllerWithIdentifier:@"Result"];
    viewController.resultInfo = data;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

#pragma mark - WSDLRequestResponseDelegate

- (void)SaveQuestionnaireDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"response: %@", [response info]);
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }

    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }

    [self loadResultView:[response info]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_currentQuestion[kQuestionOptionKey][kOptionKey] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    RTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil)
    {
        cell = [[RTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                 reuseIdentifier:cellIdentifier];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.numberOfLines = 0;
        
        if (indexPath.row == 0)
            cell.top = YES;
        if (indexPath.row == [_currentQuestion[kQuestionOptionKey][kOptionKey] count] - 1)
            cell.down = YES;
        
    }
    
    NSDictionary *data = [_currentQuestion[kQuestionOptionKey][kOptionKey] objectAtIndex:indexPath.row];
    cell.textLabel.text = data[kQuestionTextKey];
    cell.imageView.image = [data[kOptionSelectedKey] boolValue] ? [UIImage imageNamed:@"check-blue"] : [UIImage imageNamed:@"uncheck-blue"];
    
    cell.imageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.imageView.layer.borderWidth = 1;
    
    CGRect frame = CGRectMake(0, 0, 30, 30);
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, UIScreen.mainScreen.scale);
    [cell.imageView.image drawInRect:frame];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = [_currentQuestion[kQuestionOptionKey][kOptionKey] objectAtIndex:indexPath.row];
    NSString *text = data[kQuestionTextKey];
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:12]
                   constrainedToSize:CGSizeMake(200, 300)];
    return size.height + 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSDictionary *data = _questions[_page];
    CGFloat height = [self minHeightForText:data[kQuestionTextKey]];
    return height + 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor clearColor];
    
    NSDictionary *data = _questions[_page];
    
    CGFloat height = [self minHeightForText:data[kQuestionTextKey]];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 300, height + 20)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.numberOfLines = 0;
    lblTitle.text = data[kQuestionTextKey];
    
    [view addSubview:lblTitle];
    return view;
}

- (void)resetOptions {
    [_currentQuestion[kQuestionOptionKey][kOptionKey] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:obj];
        [data setObject:@NO forKey:kOptionSelectedKey];
        [_currentQuestion[kQuestionOptionKey][kOptionKey] replaceObjectAtIndex:idx withObject:data];
    }];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self resetOptions];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[_currentQuestion[kQuestionOptionKey][kOptionKey] objectAtIndex:indexPath.row]];
    [data setObject:@YES forKey:kOptionSelectedKey];
    if ([data[@"a:OptionId"] isEqualToString:_currentQuestion[@"a:CorrectAnswer"]]) {
        [_listCheck replaceObjectAtIndex:_page withObject:@YES];
    }
    [_currentQuestion[kQuestionOptionKey][kOptionKey] replaceObjectAtIndex:indexPath.row withObject:data];
    [_tableView performSelector:@selector(reloadData)
                     withObject:nil
                     afterDelay:0.3];
}

- (CGFloat)minHeightForText:(NSString *)_text {
    return [_text
            sizeWithFont:[UIFont boldSystemFontOfSize:14]
            constrainedToSize:CGSizeMake(300, 999999)
            lineBreakMode:NSLineBreakByWordWrapping
            ].height;
}

@end
