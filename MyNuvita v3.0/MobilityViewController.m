//
//  MobilityViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/25/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "MobilityViewController.h"
#import "RTableCell.h"

@interface MobilityViewController ()

@property (retain, nonatomic) NSMutableDictionary *data;
@property (retain, nonatomic) NSMutableArray *list;

@property (retain, nonatomic) NSDate *today;

@end

@implementation MobilityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _today = [NSDate date];
    _data = [[NSMutableDictionary alloc] init];
    _list = [[NSMutableArray alloc] initWithObjects:@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday", @"Mobility Video",  nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getMobilityWeek];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void)loadMobilityWeekData:(NSDictionary *)info {
    _lblProgram.text = info[kInfoProgramKey];
    _lblWeek.text = info[kInfoWeekKey];

    [_data setDictionary:info];
    [_data removeObjectForKey:@"a:ErrorStatus"];
    [_data removeObjectForKey:@"a:WeekLabel"];
    [_tableView reloadData];
}

- (void)getMobilityWeek {
    [JBLoadingView startAnimating];
    
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetMobilityWeek;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.memberID = [loginInfo objectForKey:kInfoIDKey];
    parameter.weekDate = [_today description];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetMobilityWeekDidFinishWithResponse:response];
    });
    
}

- (void)saveMobilityWeek {
    [JBLoadingView startAnimating];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPISaveMobilityWeek;
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:_data];
    [dictionary removeObjectForKey:@"a:Mobility VideoText"];
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.mobilityWeek = [NSDictionary dictionaryWithObject:_data forKey:@"mobilityWeek"];
    
    [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self SaveMobilityWeekDidFinishWithResponse:response];
    });
}

#pragma mark - WSDLRequestResponseDelegate

- (void)GetMobilityWeekDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"GetMobilityWeekDidFinishWithResponse: %@", [response info]);
    if ([[response status] isSuccess]) {
        [self loadMobilityWeekData:[response info]];
    }
    
}

- (void)SaveMobilityWeekDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"SaveMobilityWeekDidFinishWithResponse: %@", [response info]);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    RTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    if (indexPath.row == 0)
        cell.top = YES;
    if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1)
        cell.down = YES;

    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.text = [_list objectAtIndex:indexPath.row];
//    [_data setObject:@"" forKey:[NSString stringWithFormat:@"a:%@Text", cell.textLabel.text]];
    UIImage *image = [[_data objectForKey:[self getStaticKey:cell.textLabel.text]] boolValue] ?  [UIImage imageNamed:@"check-blue"] : [UIImage imageNamed:@"uncheck-blue"];
    cell.imageView.image = [image colorizedImage:kColorNuvitaBlue];
    if (indexPath.row == [_list count] - 1) {
        cell.imageView.image = [UIImage imageNamed:@"video-icon"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    cell.imageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.imageView.layer.borderWidth = 1;
    cell.imageView.layer.cornerRadius = 2;
    cell.imageView.layer.masksToBounds = YES;
    return cell;
}

- (NSString *)getStaticKey:(NSString *)key {
    return [NSString stringWithFormat:@"a:%@Select", key];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    
    CGFloat width = 200;
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake((view.frame.size.width / 2) - (width / 2), 10, width, 40)];
    [button setBackgroundImage:[[UIImage imageNamed:@"button-default"] colorizedImage:kColorNuvitaBlue]
                      forState:UIControlStateNormal];
    [button setTitle:@"Save"
            forState:UIControlStateNormal];
    [button addTarget:self
               action:@selector(saveMobilityWeek)
     forControlEvents:UIControlEventTouchUpInside];
    
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    
    [view addSubview:button];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == [_list count] - 1) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                      bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"MobilityOptionViewController"];
        [self.navigationController pushViewController:viewController
                                             animated:YES];
        return;
    }
    
    RTableCell *cell = (RTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSString *key = [self getStaticKey:cell.textLabel.text];
    BOOL selected = [[_data objectForKey:key] boolValue];
    [_data setObject:[NSString stringWithFormat:@"%@", selected ? @"false" : @"true"] forKey:key];
    
    NSLog(@"_data: %@", _data);
    [tableView performSelector:@selector(reloadData)];
   
}

@end
