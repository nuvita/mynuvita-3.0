//
//  WellnessWallViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/21/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "WellnessWallViewController.h"

@interface WellnessWallViewController () <UIWebViewDelegate>

@property (retain, nonatomic) NSMutableArray *posts;

@property (retain, nonatomic) NSMutableDictionary *cellHeights;
@property (retain, nonatomic) NSString *htmlStyle;

@property (assign) BOOL willContinueLoading;

@end

@implementation WellnessWallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
    
    _willContinueLoading = NO;
    _posts = [[NSMutableArray alloc] init];
    _cellHeights = [NSMutableDictionary dictionary];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"New Post"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(addNewPost)];
    
}

- (void)addNewPost {
    [_posts removeAllObjects];
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                  bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"AddNewPost"];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getWellnessWall];
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:YES];
//    [_posts removeAllObjects];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Public Methods

- (void)loadWellnessWallInfo:(NSDictionary *)info {
    _htmlStyle = info[@"a:CSS"];
//    JLog(@"html: %@", _htmlStyle);

    id object = info[kInfoPostsKey][kPostStringKey];
    if ([object isKindOfClass:[NSArray class]])
        [_posts addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_posts addObject:object];
 
    [_tableView reloadData];
}

- (void)getWellnessWall {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetWellnessWall;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.skip = [NSString stringWithFormat:@"%d", (int)[_posts count]];
        parameter.take = @"10";
        
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetWellnessWallResponse:response];
        });
    });
    
}

#pragma mark - WSDLRequestResponse Delegate

- (void)GetWellnessWallResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    [self loadWellnessWallInfo:response.info];
}

- (NSString *)addHtmlStyle:(NSString *)html {
    //CSS helper
    NSString *helperCSS = @".WallItem {font-family: 'Segoe UI', Calibri, sans-serif;font-size: 14px;background-color:#fff ;margin: 4px 4px 4px 4px;}.WallItemDate {vertical-align:top;font-size: 11px;color: #000;}.WallItemImage {width: 100% !important;height: auto !important;padding: auto !important;}.WallItemText {padding-top: 10px;}";
    
    //Add the content to the another string with styling and the original html content
    NSString *htmlStyling = [NSString stringWithFormat:@"<html>"
                             "<style type=\"text/css\">"
                             "%@"
                             "</style>"
                             "<body>"
                             "<p>%@</p>"
                             "</body></html>", [_htmlStyle stringByAppendingString:helperCSS], html];
    
    return htmlStyling;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        NSString *raw = _posts[indexPath.row];
//        NSLog(@"raw: %@", raw);
        if ([raw length] > 0) {

            NSString *wrappedHtml = [self addHtmlStyle:raw];
            CGFloat height = [self minHeightForText:wrappedHtml];
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, tableView.frame.size.width - 20, height)];
            webView.backgroundColor = [UIColor clearColor];
            webView.tag = indexPath.row;
            webView.layer.cornerRadius = 3.0f;
            webView.layer.masksToBounds = YES;
            webView.layer.borderColor = [kbordercolor CGColor];
            webView.layer.borderWidth = 1.0f;
            [cell.contentView addSubview:webView];
        }

    }

    NSString *raw = _posts[indexPath.row];
    if ([raw length] > 0) {
        
        
        
        __block UIWebView *webView = nil;
        [cell.contentView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[UIWebView class]]) {
                webView = obj;
                *stop = YES;
            }
        }];

        if (webView) {
            //...resize
            if ([_cellHeights objectForKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]]) {
                CGFloat height = [[_cellHeights objectForKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]] floatValue];
                CGRect frame = webView.frame;
                frame.size.height = height - 10;
                webView.frame = frame;
                webView.delegate = nil;
                webView.scrollView.scrollEnabled = NO;
                webView.scrollView.bounces = NO;
                
                NSString *wrappedHtml = [self addHtmlStyle:raw];
                [webView loadHTMLString:wrappedHtml
                                baseURL:nil];
                return cell;
                
            } else {
                webView.tag = indexPath.row;
                webView.delegate = self;
                webView.userInteractionEnabled = YES;
                webView.multipleTouchEnabled = YES;
                webView.scalesPageToFit = NO;
                webView.scrollView.scrollEnabled = NO;
                webView.scrollView.bounces = NO;
                
                NSString *wrappedHtml = [self addHtmlStyle:raw];
                [webView loadHTMLString:wrappedHtml
                                baseURL:nil];
                return cell;
            }
        }

    }
    
    return cell;
}

- (CGFloat)minHeightForText:(NSString *)_text {
    return [_text sizeWithFont:[UIFont boldSystemFontOfSize:15]
             constrainedToSize:CGSizeMake(300, 999999)
                 lineBreakMode:NSLineBreakByWordWrapping].height;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_cellHeights objectForKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]]) {
        return [[_cellHeights objectForKey:[NSString stringWithFormat:@"%d", (int)indexPath.row]] floatValue];
    }
    
    return 1;
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    CGSize size = [webView sizeThatFits:CGSizeZero];
    frame.size = size;
    webView.frame = frame;
    
    [_cellHeights setObject:[NSString stringWithFormat:@"%f", frame.size.height + 10]
                     forKey:[NSString stringWithFormat:@"%d", (int)webView.tag]];
    [_tableView beginUpdates];
    [_tableView endUpdates];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView.contentSize.height < scrollView.contentOffset.y + scrollView.bounds.size.height && [[_cellHeights allKeys] count] == [_posts count]) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        _willContinueLoading = YES;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (_willContinueLoading) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            _willContinueLoading = NO;
            [self performSelector:@selector(getWellnessWall) withObject:nil afterDelay:1.0f];
        });
    }
}

@end
