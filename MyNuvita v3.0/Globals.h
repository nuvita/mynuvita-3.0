//
//  Globals.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/14/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#ifndef MyNuvita_v3_0_Globals_h
#define MyNuvita_v3_0_Globals_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>

#endif

#import "AppDelegate.h"
#import "MKNumberBadgeView.h"

#import "UIImageView+WebCache.h"
#import "UIImage+BlurredFrame.h"
#import "NSData+Base64.h"
#import "ClassHelpers.h"
#import "JProgressView.h"
#import "JBCustomButton.h"
#import "RTableCell.h"
#import "WebViewCell.h"
#import "FriendTableViewCell.h"
#import "JBLoadingView.h"
#import "CMPopTipView.h"

#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
#import <libxml/parser.h>
#import "xs.h"
#import "ns1.h"
#import "tns1.h"
#import "tns2.h"
#import "tns3.h"
#import "tns4.h"
#import "WSDLRequest.h"
#import "XMLDictionary.h"

#import "CardioViewController.h"
#import "MobilityOptionViewController.h"
#import "WebViewController.h"
#import "QuizViewController.h"
#import "AddCoachPostViewController.h"
#import "ResultViewController.h"
#import "ExerciseViewController.h"
#import "JustForYouViewController.h"
#import "FindFriendsViewController.h"

#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "InitialSlidingViewController.h"
#import "FriendViewController.h"
#import "FriendSlidingViewController.h"
#import "FriendTableViewController.h"

#import "AddMealViewController.h"

#define RDX -15
#define LDX 15

#define METHOD_URL                                      @"http://tempuri.org/IMobileService/"
#define APIURLBETA                                      @"http://dev1.ws.mynuvita.com/NuvitaMobile/mobileservice.svc"
#define APIURLOLD                                       @"http://ws.mynuvita.com/MobileService.svc"

#define kCrashLogReportKey                              @"crash_log_report"

#define kbordercolor                                    [UIColor colorWithHexString:@"#bcbfc3"]
#define kbackgroundcolor                                [UIColor colorWithHexString:@"#d3d6db"]

#define kColorNuvitaGray                                [UIColor colorWithWhite:0.7 alpha:1]
#define kColorNuvitaBlue                                [UIColor colorWithHexString:@"#699bc7"]
#define kColorNuvitaGreen                               [UIColor colorWithHexString:@"#9ec145"]
#define kColorNuvitaOrange                              [UIColor colorWithHexString:@"#FAA634"] //f8a542
#define kColorNuvitaRed                                 [UIColor colorWithHexString:@"#c56563"]
#define kNuvitaColors                                   [NSArray arrayWithObjects:kColorNuvitaBlue, kColorNuvitaGreen, kColorNuvitaOrange, kColorNuvitaRed, nil]

#define kUserAutoLogin                                  @"log.once.open"
#define kFieldValueKey                                  @"object"
#define kFieldPlaceholderKey                            @"placeholder"
#define kInfoUsernameKey                                @"userId"
#define kInfoPasswordKey                                @"password"

#define kInfoDateKey                                    @"date"
#define kInfoDateTimeKey                                @"dateTime"
#define kInfoIDKey                                      @"eid"
#define kInfoTypeKey                                    @"type"
#define kInfoSkipKey                                    @"skip"
#define kInfoTakeKey                                    @"take"

#define kInfoErrorStatusKey                             @"a:ErrorStatus"
#define kErrorStatus                                    @"a:ErrorStatus"
#define kErrorMessage                                   @"a:Message"
#define kMessageKey                                     @"a:Message"
#define kInfoErrorKey                                   @"a:ErrorMessage"
#define kInfoAvatarKey                                  @"a:AvatarUrl"
#define kInfoProgramKey                                 @"a:ProgramName"
#define kInfoWeekKey                                    @"a:WeekLabel"
#define kInfoProgressPercentKey                         @"a:ProgressPercent"
#define kInfoFirstNameKey                               @"a:FirstName"
#define kInfoBaseURLKey                                 @"a:UrlBase"
#define kInfoPrivacyURLKey                              @"a:PrivacyUrl"
#define kInfoSettingURLKey                              @"a:SettingsUrl"
#define kInfoURLKey                                     @""
#define kInfoElementKey                                 @"a:Elements"
#define kInfoElementProgressKey                         @"a:ElementProgress"
#define kElementNameKey                                 @"a:ElementName"
#define kElementOverAllKey                              @"a:PercentOrverall"
#define kElementProgressKey                             @"a:ProgressPercent"
#define kElementColorKey                                @"a:Color"

#define kInfoCardioStarKey                              @"a:CardioStars"
#define kCardioStarsKey                                 @"a:Star"
#define kInfoNameKey                                    @"a:Name"
#define kInfoPercentKey                                 @"a:Percent"
#define kInfoValueKey                                   @"a:Value"

#define kInfoLessonKey                                  @"a:LessonTopics"
#define kLessonsKey                                     @"a:LessonTopic"
#define kLessonNameKey                                  @"a:LessonName"
#define kLessonNumberKey                                @"a:LessonNumber"

#define kInfoQuestionKey                                @"a:Questions"
#define kQuestionsKey                                   @"a:Question"
#define kQuestionIDKey                                  @"a:QuestionId"
#define kQuestionTextKey                                @"a:Text"
#define kQuestionTypeKey                                @"a:Type"
#define kQuestionOptionKey                              @"a:Options"
#define kOptionKey                                      @"a:Option"
#define kOptionIDKey                                    @"a:OptionId"
#define kOptionCorrectKey                               @"a:isCorrect"
#define kOptionSelectedKey                              @"a:isSelected"

#define kInfoBelowZonePercentKey                        @"a:BelowZonePercent"
#define kInfoBelowZoneBPMKey                            @"a:BelowZoneBpm"
#define kInfoAboveZonePercentKey                        @"a:AboveZonePercent"
#define kInfoAboveZoneBPMKey                            @"a:AboveZoneBpm"
#define kInfoInZonePercentKey                           @"a:InZonePercent"
#define kInfoInZoneBPMKey                               @"a:InZoneBpm"
#define kInfoProgressPercentKey                         @"a:ProgressPercent"
#define kInfoProgressValueKey                           @"a:ProgressValue"
#define kInfoGoalLabelKey                               @"a:GoalLabel"
#define kInfoGoalValueKey                               @"a:GoalValue"

#define kHRSessionsKey                                  @"a:HRSessions"
#define kSessionKey                                     @"a:HRSession"
#define kSessionCalorieKey                              @"a:Calories"
#define kSessionDateKey                                 @"a:Date"
#define kSessionDurationKey                             @"a:Duration"
#define kSessionAboveKey                                @"a:MinutesAboveZone"
#define kSessionBelowKey                                @"a:MinutesBelowZone"
#define kSessionInKey                                   @"a:MinutesInZone"

#define kInfoPostsKey                                   @"a:Posts"
#define kPostStringKey                                  @"b:string"
#define kPostCoachWallKey                               @"a:CoachingWall"
#define kInfoFriendsKey                                 @"a:MyFriends"
#define kFriendKey                                      @"a:Friend"
#define kFriendNameKey                                  @"a:DisplayName"
#define kFriendIDKey                                    @"a:Eid"
#define kFriendEmailKey                                 @"a:Email"
#define kFriendProgramTypeKey                           @"a:ProgramType"
#define kFriendProgressPercentKey                       @"a:ProgressPercent"
#define kFriendAvatarKey                                @"a:AvatarUrl"

#define mIDKey                                          @"eid"
#define mDateTimeKey                                    @"dateTime"
#define mDescriptionKey                                 @"description"
#define mRankKey                                        @"Rank"
#define mTypeKey                                        @"MealType"
#define mTypeSmallKey                                   @"mealType"
#define mNumberKey                                      @"MealNumber"
#define mFavoriteIdKey                                  @"FavoriteId"
#define mFavoriteEditModeKey                            @"FavoriteEditMode"
#define mPhotoKey                                       @"photo"

#define kMealMemberIDKey                                @"a:MemberEid"
#define kMealDescriptionKey                             @"a:Description"
#define kMealRankKey                                    @"a:MealRank"
#define kMealTypeKey                                    @"a:MealType"
#define kMealNumberKey                                  @"a:MealNumber"
#define kMealFavoriteIdKey                              @"a:FavoriteId"
#define kMealFavoriteNumberKey                          @"a:FavoriteNumber"
#define kMealFavoriteEditModeKey                        @"a:FavoriteEditMode"
#define kMealPhotoUrlKey                                @"a:PhotoUrl"

//... API - SERVICE
#define kAPIService                                     @"API-SERVICE"
#define kAPILogin                                       @"LogIn"
#define kAPIGetProgramType                              @"GetProgramType"
#define kAPIGetWeeklyProgress                           @"GetWeeklyProgress"
#define kAPIGetCardioStars                              @"GetCardioStars"
#define kAPIGetFriends                                  @"GetFriends"
#define kAPIGetMessageList                              @"GetMessageList"
#define kAPIGetFriendInviteList                         @"GetFriendInviteList"
#define kAPIFindFriends                                 @"FindFriends"
#define kAPIAcceptFriendInvite                          @"AcceptFriendInvite"
#define kAPISendFriendInvite                            @"SendFriendInvite"
#define kAPIRemoveFriend                                @"RemoveFriend"
#define kAPIGetWellnessWall                             @"GetWellnessWall"
#define kAPIPostWellnessWall                            @"PostWellnessWall"
#define kAPIGetCardioWeek                               @"GetCardioWeek"
#define kAPIGetMobilityWeek                             @"GetMobilityWeek"
#define kAPISaveMobilityWeek                            @"SaveMobilityWeek"
#define kAPIGetMobilityWorkoutList                      @"GetMobilityWorkoutList"
#define kAPIGetCoachWeek                                @"GetCoachWeek"
#define kAPIGetAppointmentScheduler                     @"GetAppointmentScheduler"
#define kAPIGetCoachLesson                              @"GetCoachLesson"
#define kAPISaveQuestionnaire                           @"SaveQuestionnaire"
#define kAPIGetCoachingWall                             @"GetCoachingWall"
#define kAPIPostCoachingWall                            @"PostCoachingWall"
#define kAPIGetCoachResources                           @"GetCoachResources"
#define kAPIGetMemberMeals                              @"getMemberMeals"
#define kAPIGetMemberFavoriteMeals                      @"getMemberFavoriteMeals"
#define kAPIGetMemberNutritionHabits                    @"getMemberNutritionHabits"
#define kAPISaveMemberMeal                              @"SaveMemberMeal"


//.... dummy
#define kDummyAvatar                                    [[UIImage imageNamed:@"profile-avatar"] colorizedImage:kColorNuvitaBlue]
#define kDummyID2                                       @"edfc4b8c-04b8-4129-af19-51a57feff153"

#define JLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)