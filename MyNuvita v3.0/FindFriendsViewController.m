
//
//  FindFriendsViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/28/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "FindFriendsViewController.h"

@interface FindFriendsViewController () <UISearchBarDelegate>
{
    FriendOperation operation;
}

@property (retain, nonatomic) NSMutableArray *results;

@end

@implementation FindFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor lightGrayColor];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _results = [[NSMutableArray alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    _lblOption.text = self.title;
    if ([self.title isEqualToString:@"Friend Request"])
        [self getFriendInviteList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Observer

- (void)keyboardWillShow:(NSNotification *)notification  {
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    CGRect frame = _tableView.frame;
    frame.size.height -= height;
    [UIView animateWithDuration:0.3
                     animations:^{
                         _tableView.frame = frame;
                     }];
}

- (void)keyboardDidHide:(NSNotification *)notification  {
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    CGRect frame = _tableView.frame;
    frame.size.height += height;
    [UIView animateWithDuration:0.3
                     animations:^{
                         _tableView.frame = frame;
                     }];
}

#pragma mark - Public Methods

- (void)didTapAccept:(UIButton *)sender {
    [JBLoadingView startAnimating];
    [self.view endEditing:YES];
    
    NSDictionary *data = [_results objectAtIndex:[sender tag]];
    [_results objectAtIndex:[sender tag]];
    
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIAcceptFriendInvite;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.friendEid = data[@"a:Eid"];
    parameter.accept = @"true";
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self AcceptFriendInviteDidFinishWithResponse:response];
        
    });
}

- (void)didTapDecline:(UIButton *)sender {
    [JBLoadingView startAnimating];
    [self.view endEditing:YES];

    NSDictionary *data = [_results objectAtIndex:[sender tag]];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIAcceptFriendInvite;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.friendEid = data[@"a:Eid"];
    parameter.accept = @"false";
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self AcceptFriendInviteDidFinishWithResponse:response];
        [_results objectAtIndex:[sender tag]];
    });
}

- (void)didTapInvite:(UIButton *)sender {
    [JBLoadingView startAnimating];
    [self.view endEditing:YES];
    
    NSDictionary *data = [_results objectAtIndex:[sender tag]];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPISendFriendInvite;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.friendEid = data[@"a:Eid"];

    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self SendFriendInviteDidFinishWithResponse:response withUserData:[_results objectAtIndex:[sender tag]]];
    });
}

- (void)getFriendInviteList {
    [JBLoadingView startAnimating];
    
    operation = FriendOperationListRequest;
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetFriendInviteList;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetFriendInviteListDidFinishWithResponse:response];
    });
}

- (void)getUserCellData:(NSDictionary *)details atIndexPath:(NSIndexPath *)indexPath {
  
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetWeeklyProgress;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = details[@"a:Eid"];
    parameter.date = [[NSDate date] description];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetWeeklyProgressDidFinishWithResponse:response atIndexPath:indexPath];
    });
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    operation = FriendOperationInvitationRequest;
    
    [JBLoadingView startAnimating];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIFindFriends;
    
    NSString *text = [_searchBar.text description];
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [[loginInfo objectForKey:kInfoIDKey] description];
    parameter.searchName = text;
//    parameter.skip = @"0";
//    parameter.take = @"10";
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self FindFriendsDidFinishWithResponse:response];
    });
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO];
}

#pragma mark - WSDLRequestResponseDelegate

- (void)SendFriendInviteDidFinishWithResponse:(WSDLRequestResponse *)response withUserData:(NSDictionary *)data {
    [JBLoadingView stopAnimating];
    NSLog(@"SendFriendInviteDidFinishWithResponse: %@", [response info]);
    if (![[[response info] objectForKey:@"a:ErrorStatus"] boolValue]) {
        [UIAlertView displayAlert:[NSString stringWithFormat:@"You have successfully invited %@", data[@"a:Name"]]];
    }
}

- (void)AcceptFriendInviteDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    [_tableView reloadData];
    [self getFriendInviteList];
}

- (void)GetFriendInviteListDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"GetFriendInviteListDidFinishWithResponse: %@", [response info]);
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:@"You currently have no friend requests."];
    }
    
    [self loadResultResponse:response];
}

- (void)FindFriendsDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    NSLog(@"FindFriendsDidFinishWithResponse: %@", [response info]);
    [self loadResultResponse:response];
}

- (void)loadResultResponse:(WSDLRequestResponse *)response {
    [_results removeAllObjects];
    
    if ([[response status] isSuccess]) {
        id object = [response info][@"a:Names"][@"a:NameItem"];
        if ([object isKindOfClass:[NSArray class]]) {
            [_results addObjectsFromArray:object];
        }
        
        if ([object isKindOfClass:[NSDictionary class]]) {
            [_results addObject:object];
        }
    }
    
    [_tableView reloadData];
}

- (void)GetWeeklyProgressDidFinishWithResponse:(WSDLRequestResponse *)response
                                   atIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    [cell.imageView setImageWithURL:[NSURL URLWithString:[response info][kInfoAvatarKey]]
                   placeholderImage:[[UIImage imageNamed:@"profile-avatar"] colorizedImage:kColorNuvitaBlue]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        CALayer *layer = [CALayer drawLineWithSize:CGSizeMake(tableView.frame.size.width, 60)];
        [cell.contentView.layer addSublayer:layer];
    }

    NSDictionary *data = _results[indexPath.row];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.text = data[@"a:Name"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self getUserCellData:data atIndexPath:indexPath];
    });
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    
    if (operation == FriendOperationListRequest) {
        view.frame = CGRectMake(0, 0, 100, 60);
        
        UIButton *accept = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 60)];
        accept.tag = indexPath.row;
        [accept setImage:[[UIImage imageNamed:@"accept-icon"] colorizedImage:kColorNuvitaGreen]
                forState:UIControlStateNormal];
        [accept addTarget:self
                   action:@selector(didTapAccept:)
         forControlEvents:UIControlEventTouchUpInside];
        UIButton *decline = [[UIButton alloc] initWithFrame:CGRectMake(50, 0, 50, 60)];
        decline.tag = indexPath.row;
        [decline setImage:[[UIImage imageNamed:@"decline-icon"] colorizedImage:kColorNuvitaRed]
                 forState:UIControlStateNormal];
        [decline addTarget:self
                    action:@selector(didTapDecline:)
          forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:accept];
        [view addSubview:decline];
        
    } else {
        view.frame = CGRectMake(0, 0, 50, 60);
        UIButton *add = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 60)];
        add.tag = indexPath.row;
        [add setImage:[[UIImage imageNamed:@"add-icon"] colorizedImage:kColorNuvitaGreen]
                forState:UIControlStateNormal];
        [add addTarget:self
                   action:@selector(didTapInvite:)
         forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:add];
    }
    
    cell.accessoryView = view;
    
    cell.imageView.layer.cornerRadius = 25;
    cell.imageView.layer.masksToBounds = YES;
    cell.imageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.imageView.layer.borderWidth = 1;
    
    CGRect frame = CGRectMake(0, 0, 50, 50);
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, UIScreen.mainScreen.scale);
    [cell.imageView.image drawInRect:frame];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ([self.title isEqualToString:@"Friend Request"])
        return 1;
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if ([self.title isEqualToString:@"Friend Request"])
        return nil;
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
    _searchBar.delegate = self;
    return _searchBar;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
