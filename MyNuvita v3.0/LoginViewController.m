//
//  LoginViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/24/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@property (retain, nonatomic) NSMutableArray *details;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    UILabel *label = (UILabel *)[self.view viewWithTag:1000];
    label.text = [NSString stringWithFormat:@"v. %@", version];

    _details = [[NSMutableArray alloc] initWithObjects:@{kFieldValueKey : @"",
                                                         kFieldPlaceholderKey : @"Username",
                                                         @"key" : kInfoUsernameKey},
                                                        @{kFieldValueKey : @"",
                                                          kFieldPlaceholderKey : @"Password",
                                                          @"key" : kInfoPasswordKey},
                nil];
    
    _imgCheckmark.layer.borderColor = [[UIColor whiteColor] CGColor];
    _imgCheckmark.layer.borderWidth = 1;
    _imgCheckmark.layer.cornerRadius = 2;
    _imgCheckmark.layer.masksToBounds = YES;
    _imgCheckmark.image = [[UIImage imageNamed:@"uncheck-blue"] colorizedImage:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [[NSUserDefaults standardUserDefaults] setObject:nil
                                              forKey:kInfoIDKey];
}

#pragma mark - Public Methods

- (NSString *)textFromObjectKey:(NSString *)key {
    __block NSString *text = @"";
    [_details enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj objectForKey:@"key"] isEqualToString:key]) {
            text = [(UITextField *)[obj objectForKey:kFieldValueKey] text];
        }
        
    }];
    
    return text;
}

- (IBAction)didTapCheckbox:(id)sender {
//    UIButton *button = (UIButton *)sender;
//    if ([button tag] == 9999) {
//        button.tag = 0;
//        _imgCheckmark.tag = button.tag;
//        _imgCheckmark.image = [[UIImage imageNamed:@"uncheck-blue"] colorizedImage:[UIColor whiteColor]];
//        return;
//    }
//    
//    button.tag = 9999;
//    _imgCheckmark.tag = button.tag;
//    _imgCheckmark.image = [[UIImage imageNamed:@"check-blue"] colorizedImage:[UIColor whiteColor]];
}

- (void)getProgramType {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        if (![loginInfo objectForKey:kInfoIDKey]) {
            [UIAlertView displayAlert:@"Session is invalid!"];
            return;
        }
        
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetProgramType;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [[NSDate date] description];
        
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetProgramTypeDidFinishWithResponse:response];
        });
    });
}

- (void)didTapLoginButton:(id)sender {
    [self.view endEditing:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        UIButton *button = (UIButton *)sender;
        button.alpha = 0.7;
        [UIView animateWithDuration:0.3
                         animations:^{
                             button.alpha = 1;
                         }];
        
        if ([[self textFromObjectKey:kInfoUsernameKey] length] == 0) {
            [UIAlertView displayAlert:@"Please enter username."
                                title:@"Error"];
            return;
        }
        
        if ([[self textFromObjectKey:kInfoPasswordKey] length] == 0) {
            [UIAlertView displayAlert:@"Please enter password."
                                title:@"Error"];
            return;
        }

        [JBLoadingView startAnimatingWithText:@"Logging in..."];
        NSString *username = [self textFromObjectKey:kInfoUsernameKey];
        NSString *password = [self textFromObjectKey:kInfoPasswordKey];
        
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPILogin;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.email = username;
        parameter.password = password;
        
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self LoginDidFinishWithResponse:response];
        });
    });
}

- (void)loadHomeView {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate loadTabarController];
}

#pragma mark - WSDLRequestResponseDelegate

- (void)GetProgramTypeDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"response: %@", [response info]);
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if ([[response.info textValue] rangeOfString:@"Unable"].location != NSNotFound) {
        [UIAlertView displayAlert:[response.info textValue]];
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[[response info] textValue] forKey:kInfoProgramKey];
//    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadHomeView];
//    });
}

- (void)LoginDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if ([[response.info textValue] rangeOfString:@"Unable"].location != NSNotFound) {
        [UIAlertView displayAlert:[response.info textValue]];
        return;
    }
    
    //...no error
    [[NSUserDefaults standardUserDefaults] setObject:[self textFromObjectKey:kInfoUsernameKey]
                                              forKey:kInfoUsernameKey];
    [[NSUserDefaults standardUserDefaults] setObject:[self textFromObjectKey:kInfoPasswordKey]
                                              forKey:kInfoPasswordKey];
    [[NSUserDefaults standardUserDefaults] setObject:[response.info textValue]
                                              forKey:kInfoIDKey];
//    [[NSUserDefaults standardUserDefaults] setObject:[_imgCheckmark tag] == 9999 ? @YES : @NO
//                                              forKey:kUserAutoLogin];
    [self getProgramType];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:_details[indexPath.row]];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    CGRect frame = tableView.frame;
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(10, 2, frame.size.width - 20, 40)];
    textfield.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
    textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textfield.autocorrectionType = UITextAutocorrectionTypeNo;
    textfield.borderStyle = UITextBorderStyleNone;
    textfield.clearButtonMode = UITextFieldViewModeWhileEditing;
    textfield.placeholder = data[kFieldPlaceholderKey];
    textfield.font = [UIFont systemFontOfSize:16];
    textfield.opaque = YES;
    textfield.secureTextEntry = [data[kFieldPlaceholderKey] isEqualToString:@"Password"];
    textfield.text = [[NSUserDefaults standardUserDefaults] objectForKey:data[@"key"]];
    
    UIView *padding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textfield.leftView = padding;
    textfield.leftViewMode = UITextFieldViewModeAlways;
    
    textfield.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textfield.layer.borderWidth = 2;
    textfield.layer.cornerRadius = 4;
    textfield.clipsToBounds = YES;
    
    [data setObject:textfield forKey:kFieldValueKey];
    [_details replaceObjectAtIndex:indexPath.row withObject:data];
    [cell.contentView addSubview:textfield];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    
    CGRect frame = tableView.frame;
    CGFloat width = 150;
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake((frame.size.width / 2) - (width / 2), 10, width, 40)];
    button.backgroundColor = [UIColor colorWithHexString:@"#145ac6"];
    button.tintColor = [UIColor whiteColor];
    [button setTitle:@"Login" forState:UIControlStateNormal];
    [button addTarget:self
               action:@selector(didTapLoginButton:)
     forControlEvents:UIControlEventTouchUpInside];
    
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    
    [view addSubview:button];
    return view;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
