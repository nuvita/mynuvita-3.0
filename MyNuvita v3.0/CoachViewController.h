//
//  CoachViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/21/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface CoachViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *coachTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblWeek;
@end
