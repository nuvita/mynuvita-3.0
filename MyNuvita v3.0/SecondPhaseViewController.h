//
//  SecondPhaseViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 2/6/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface SecondPhaseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
