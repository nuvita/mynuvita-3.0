//
//  MobilityOptionViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/26/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "MobilityOptionViewController.h"

@interface MobilityOptionViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSMutableArray *list;

@property (retain, nonatomic) NSDate *today;

@end

@implementation MobilityOptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _list = [[NSMutableArray alloc] init];
    _today = [NSDate date];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getMobilityWorkout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void)getMobilityWorkout {
    [JBLoadingView startAnimating];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetMobilityWorkoutList;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.memberId = [loginInfo objectForKey:kInfoIDKey];
    parameter.date = @"12-8-14";//[_today description];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetMobilityWorkoutListDidFinishWithResponse:response];
    });
}

- (void)loadWorkoutData:(NSDictionary *)info {
    if ([info[@"a:Workouts"] isKindOfClass:[NSArray class]]) {
        [_list removeAllObjects];
        [_list addObjectsFromArray:info[@"a:Workouts"]];
        [_list removeObject:[_list firstObject]];
        
        [_tableView reloadData];
    }
}

#pragma mark - WSDLRequestResponseDelegate

- (void)GetMobilityWorkoutListDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"GetMobilityWorkoutListDidFinishWithResponse: %@", [response info]);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;//[_list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    RTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = indexPath.row % 2 == 0 ? [UIColor lightGrayColor] : [UIColor whiteColor];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (indexPath.row == 0)
        cell.top = YES;
    if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] -1)
        cell.down = YES;

    cell.textLabel.text = @"Test";
    return cell;
}

#pragma mark - UITableViewDelegate
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ExerciseViewController *viewController = (ExerciseViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                 bundle:[NSBundle mainBundle]]
                                        instantiateViewControllerWithIdentifier:@"Exercise"];
    
    viewController.navigationController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

@end
