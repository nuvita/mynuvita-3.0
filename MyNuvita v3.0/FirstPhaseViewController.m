//
//  FirstPhaseViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 2/6/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "FirstPhaseViewController.h"

//@interface UIView (Ratings)
//+ (UIView *)createCustomViewSize:(CGSize)size withRating:(NSInteger)rating;
//@end

@interface FirstPhaseViewController ()

@property (retain, nonatomic) NSDictionary *memberMeals;
@property (retain, nonatomic) NSMutableArray *meals;
@property (retain, nonatomic) NSDate *today;

@end

@implementation FirstPhaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kbackgroundcolor;
    
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self
                   action:@selector(didTapPrev)
         forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self
                   action:@selector(didTapNext)
         forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonPrev],
                                               [[UIBarButtonItem alloc] initWithCustomView:buttonNext]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                           target:self
                                                                                           action:@selector(didTapAddMeal)];
    
    _meals = [NSMutableArray new];
    _today = [NSDate date];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getMemberMeals];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Public Methods

- (void)didTapPrev {
    _today = [_today dateByAddingTimeInterval:-86400];
    [self getMemberMeals];
}

- (void)didTapNext {
    _today = [_today dateByAddingTimeInterval:86400];
    [self getMemberMeals];
}

- (void)loadMemberMeals:(NSDictionary *)info {
    [_meals removeAllObjects];
    _memberMeals = [NSDictionary dictionaryWithDictionary:info];
    
    id object = info[@"a:Meals"][@"a:MemberMeal"];
    if ([object isKindOfClass:[NSArray class]])
        [_meals addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_meals addObject:object];
    
    [_tableView reloadData];
}

- (void)getMemberMeals {
    UIBarButtonItem *item = self.navigationItem.leftBarButtonItems.lastObject;
    item.enabled = ![[_today stringDateNutritionStyle] isEqualToString:[[NSDate date] stringDateNutritionStyle]];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetMemberMeals;
    
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [_today stringDateTimeFormat];
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetMemberMealsFinishWithResponse:response];
        });
    });
}

- (NSString *)stringMealType:(NSInteger)type {
    if (type == 0) return @"unknown";
    
    NSArray *string = @[@"breakfast", @"AM snack", @"lunch", @"PM snack", @"dinner"];
    return string[type - 1];
}

- (void)showMealDetails:(NSDictionary *)details {
    AddMealViewController *viewController = (AddMealViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                                bundle:[NSBundle mainBundle]]
                                                                      instantiateViewControllerWithIdentifier:@"AddMeal"];
    viewController.mealInfo = details;
    viewController.today = _today;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didTapAddMeal {
    [self showMealDetails:nil];
}

#pragma mark - WSDLRequestResponse 

- (void)GetMemberMealsFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    [self loadMemberMeals:[response info]];
//    NSLog(@"GetMemberMealsFinishWithResponse: %@", [response info]);
//
//    if ([response error]) {
//        [UIAlertView displayAlert:[[response error] localizedDescription]];
//        return;
//    }
//    
//    if (![[response status] isSuccess]) {
//        [UIAlertView displayAlert:[[response status] errorMessage]];
//        return;
//    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_meals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    CGRect frame = CGRectMake(0, 0, 50, 50);
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, UIScreen.mainScreen.scale);
    [cell.imageView.image drawInRect:frame];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = _meals[indexPath.row];
    CGFloat offset = 75;
    CGFloat rateHeight = 20;
    CGFloat height = tableView.rowHeight + [self calculateOffsetHeight:data[kMealDescriptionKey] ?: @"---"
                                                                 width:(tableView.width - offset) * .90];

    cell.imageView.layer.backgroundColor = [kColorNuvitaBlue CGColor];
    cell.imageView.layer.cornerRadius = 25;
    cell.imageView.layer.borderColor = [kColorNuvitaBlue CGColor];
    cell.imageView.layer.borderWidth = 1;
    cell.imageView.layer.masksToBounds = YES;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(17, 0, 46, 0)];
    label.backgroundColor = [UIColor clearColor];
    label.alpha = 0.8;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11];
    label.numberOfLines = 0;
    label.text = [self stringMealType:[data[kMealTypeKey] integerValue]];
    [label autoHeight];
    label.top = (height - label.height) / 2;
    [cell.contentView addSubview:label];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(offset, 0, (tableView.width - offset) * .90, 0)];
    title.backgroundColor = [UIColor clearColor];
    title.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    title.numberOfLines = 0;
    title.text = [data[kMealDescriptionKey] uppercaseString] ?: [@"---" uppercaseString];
    [title autoHeight];
    title.top = (height - title.height - rateHeight) / 2;
    [cell.contentView addSubview:title];
    
    UIView *view = [UIView createCustomViewSize:CGSizeMake(rateHeight * 5, rateHeight)
                                     withRating:[data[kMealRankKey] integerValue]];
    view.backgroundColor = [UIColor clearColor];
    view.top = title.bottom;
    view.left = offset;
    [cell.contentView addSubview:view];
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(label.right, cell.height, cell.width, 1.0f);
    layer.backgroundColor = [UIColor colorWithWhite:0.8f alpha:0.5f].CGColor;
    [cell.contentView.layer addSublayer:layer];
    
}

- (NSInteger)calculateOffsetHeight:(NSString *)text width:(CGFloat)width {
    CGSize maxSize = CGSizeMake(width, 9999);
    UIFont *font = [UIFont systemFontOfSize:14];
    CGRect textRect = [text boundingRectWithSize:maxSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName : font}
                                         context:nil];
    return textRect.size.height - font.lineHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = _meals[indexPath.row];
    CGFloat offset = [self calculateOffsetHeight:data[kMealDescriptionKey] ?: @"---"
                                           width:(tableView.width * .90) - 75];
    return 60 + offset;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.9f];
    
    CGFloat offset = 10;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(offset, 0, tableView.width / 2 - offset, tableView.sectionHeaderHeight)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    label.text = [_today stringDateNutritionStyle];
    [view addSubview:label];
    
    NSInteger rate = _memberMeals ? [_memberMeals[@"a:AverageRank"] integerValue] : 0;
    UIView *ratingView = [UIView createCustomViewSize:CGSizeMake(30 * 5, 30)
                                           withRating:rate];
    ratingView.layer.masksToBounds = YES;
    ratingView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    ratingView.layer.shadowOpacity = 0.5f;
    ratingView.layer.shadowRadius = 1.0f;
    ratingView.layer.shadowOffset = CGSizeMake(0, 1.0f);
    ratingView.backgroundColor = [UIColor clearColor];
    ratingView.left = tableView.width - offset - ratingView.width;
    ratingView.top = (tableView.sectionHeaderHeight - ratingView.height) / 2;
    [view addSubview:ratingView];
    
    CALayer *layer = [CALayer drawLineWithSize:CGSizeMake(tableView.width, tableView.sectionHeaderHeight)];
    [view.layer addSublayer:layer];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self showMealDetails:_meals[indexPath.row]];
}

@end
