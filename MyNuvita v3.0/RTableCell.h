//
//  RTableCell.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/26/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface RTableCell : UITableViewCell

@property (nonatomic, assign) BOOL top, down;

@end
