//
//  AppDelegate.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/13/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)setUpNavigationController:(UIWindow *)window {
    if ([[window rootViewController] isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
        tabBarController.view.backgroundColor = [UIColor whiteColor];
        [tabBarController.tabBar setTintColor:kColorNuvitaBlue];
        [tabBarController.tabBar setBarTintColor:[UIColor whiteColor]];
        [tabBarController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UINavigationController *navigationController = obj;
            navigationController.view.backgroundColor = [UIColor whiteColor];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
            [navigationController.navigationBar.topItem setTitleView:imageView];
            [navigationController.navigationBar setTintColor:kColorNuvitaBlue];
            [navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
            [navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f]}];
        }];
    }
}

- (void)loadLogin {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserAutoLogin];
    UIViewController *rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    self.window.rootViewController = navigationController;
}

- (void)loadTabarController {
    UITabBarController *tabBar = (UITabBarController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                  bundle:[NSBundle mainBundle]]
                                                        instantiateViewControllerWithIdentifier:@"defaultTabBar"];
    self.window.rootViewController = tabBar;
    [self setUpNavigationController:[[[UIApplication sharedApplication] delegate] window]];
}

- (void)loadCoachTabarController {
    UITabBarController *tabBar = (UITabBarController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                  bundle:[NSBundle mainBundle]]
                                                        instantiateViewControllerWithIdentifier:@"tabBar"];
    tabBar.selectedIndex = 1;
    self.window.rootViewController = tabBar;
    [self setUpNavigationController:[[[UIApplication sharedApplication] delegate] window]];
}

- (void)loadNutritionTabarController {
    UITabBarController *tabBar = (UITabBarController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                  bundle:[NSBundle mainBundle]]
                                                        instantiateViewControllerWithIdentifier:@"Nutrition"];
    tabBar.selectedIndex = 1;
    self.window.rootViewController = tabBar;
    [self setUpNavigationController:[[[UIApplication sharedApplication] delegate] window]];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [CrashHandler setupLogging:YES];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kUserAutoLogin]) {
        [self loadTabarController];
    } else {
        [self loadLogin];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] cleanDisk];
    if ([self crashExist]) {
        [self openMailForm];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Crash Log

- (BOOL)crashExist {
    BOOL crash = [[NSUserDefaults standardUserDefaults] boolForKey:kCrashLogReportKey];
    return crash;
}

- (NSData *)getAppCrashLog {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    NSData *myData = [NSData dataWithContentsOfFile:logPath];
    return myData;
}

- (void)propmtCrashReport {
    
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Report"
                                                        message:@"MyNuvita experienced a problem. Would you like to report this issue??"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertview show];
}

- (void)openMailForm {
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSData *myData = [self getAppCrashLog];
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [picker setToRecipients:@[@"john.bariquit@ripeconcepts.com", @"dev@nuvita.com"]];
        [picker setSubject:@"MyNuvita Crash Report"];
        [picker setMessageBody:@"Attached is the txt file of app problem report. <br/><br/>"
                        isHTML:YES];
        [picker addAttachmentData:myData
                         mimeType:@"application/log"
                         fileName:@"console.log"];
        
        [self.window.rootViewController presentViewController:picker animated:YES completion:^{
            NSLog(@".....loaded mail view");
        }];
        
    } else {
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Unable to load mail. Please set-up an email account in Settings > Mail,Contacts,Calendar."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    
    
    if (result == MFMailComposeResultSent)
        NSLog(@"Message has been sent");
    
    
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        //...done sending email
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCrashLogReportKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

@end
