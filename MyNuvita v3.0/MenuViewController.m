//
//  MenuViewController.m
//  NuvitaCardio
//
//  Created by John on 3/31/14.
//
//

#import "MenuViewController.h"

@interface MenuViewController ()

@property (retain, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _tableview.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _tableview.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);

    [self.slidingViewController setAnchorRightRevealAmount:200];
    self.slidingViewController.underLeftWidthLayout = ECFixedRevealWidth;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self loadSlidingMenu];
}

- (void)loadSlidingMenu {
    _imgAvatar.layer.cornerRadius = _imgAvatar.frame.size.width * 0.50;
    _imgAvatar.layer.masksToBounds = YES;
    [_imgAvatar setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:kInfoAvatarKey]]
               placeholderImage:[[UIImage imageNamed:@"profile-avatar"] colorizedImage:kColorNuvitaBlue]];
    _lblName.text = [[[NSUserDefaults standardUserDefaults] objectForKey:kInfoFirstNameKey] description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    switch ([indexPath row]) {
        case 0:
            cell.textLabel.text = @"My Account";
            cell.imageView.image = [[UIImage imageNamed:@"gear-icon"] colorizedImage:kColorNuvitaBlue];
            break;
            
        case 1:
            cell.textLabel.text = @"Set Avatar";
            cell.imageView.image = [[UIImage imageNamed:@"man-icon.png"] colorizedImage:kColorNuvitaBlue];
            break;
            
        case 2:
            cell.textLabel.text = @"Logout";
            cell.imageView.image = [[UIImage imageNamed:@"lock-icon.png"] colorizedImage:kColorNuvitaBlue];
            cell.accessoryType = UITableViewCellAccessoryNone;
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(menuViewController:didSelectMenu:)]) {
        [self.delegate menuViewController:self didSelectMenu:indexPath.row];
    }
}

@end
