//
//  WebViewCell.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/17/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface WebViewCell : UITableViewCell

@property (retain, nonatomic) UIWebView *webView;

@end
