//
//  ResultViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/19/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface ResultViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfCorrect;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (retain, nonatomic) NSDictionary *resultInfo;

@end
