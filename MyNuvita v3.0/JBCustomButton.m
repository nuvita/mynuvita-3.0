//
//  JBCustomButton.m
//  mynuvita
//
//  Created by John on 7/28/14.
//
//

#import "JBCustomButton.h"

@implementation JBCustomButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setShowsTouchWhenHighlighted:YES];
        
    }
    
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
//    [self setClearsContextBeforeDrawing:YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *tintColor = (self.enabled) ? kColorNuvitaBlue : [UIColor colorWithWhite:0.3 alpha:0.5];
    NSDictionary *fonts = @{
                            NSFontAttributeName : [UIFont systemFontOfSize:17],
                            NSForegroundColorAttributeName : tintColor
                            };

    CGContextSetLineWidth(context, 2.0f);
    CGContextSetStrokeColorWithColor(context, tintColor.CGColor);
    CGContextSetFillColorWithColor(context, tintColor.CGColor);

    int weight = self.frame.size.width;
    if (_buttonStyle == JBCustomButtonArrowRightStyle) {
        CGContextMoveToPoint(context, weight - 14, 5);
        CGContextAddLineToPoint(context, weight - 4, 14);
        CGContextAddLineToPoint(context, weight - 14, 23);
    }else if (_buttonStyle == JBCustomButtonArrowLeftStyle) {
        CGContextMoveToPoint(context, 14, 5);
        CGContextAddLineToPoint(context, 4, 14);
        CGContextAddLineToPoint(context, 14, 23);
    }else if (_buttonStyle == JBCustomButtonArrowTextLeftStyle) {

        CGContextMoveToPoint(context, 14, 5);
        CGContextAddLineToPoint(context, 4, 14);
        CGContextAddLineToPoint(context, 14, 23);
        CGContextStrokePath(context);
        
        NSString *title = [self titleForState:UIControlStateNormal];
        [title drawAtPoint:CGPointMake(19, 3) withAttributes:fonts];

    }else if (_buttonStyle == JBCustomButtonArrowTextRightStyle){

        CGContextMoveToPoint(context, weight - 14, 5);
        CGContextAddLineToPoint(context, weight - 4, 14);
        CGContextAddLineToPoint(context, weight - 14, 23);
        CGContextStrokePath(context);

        NSString *title = [self titleForState:UIControlStateNormal];
        CGSize size = [title sizeWithAttributes:fonts];
        [title drawAtPoint:CGPointMake(weight - 14 - size.width - 5, 3) withAttributes:fonts];

    }

    CGContextStrokePath(context);
    [self setTitle:@"" forState:UIControlStateNormal];
}

@end
