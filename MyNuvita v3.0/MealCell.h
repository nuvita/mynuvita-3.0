//
//  MealCell.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 3/3/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface MealCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgMeal;
@property (weak, nonatomic) IBOutlet UILabel *lblMeal;
@property (weak, nonatomic) IBOutlet UIView *viewRate;
@end
