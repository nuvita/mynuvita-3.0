//
//  WebViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/28/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface WebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (retain, nonatomic) NSString *urlString;

@end
