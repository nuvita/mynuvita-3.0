//
//  RTableCell.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/26/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "RTableCell.h"

@implementation RTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if(self.top && self.down) {
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
    } else if (self.top) {
        CAShapeLayer *shape = [[CAShapeLayer alloc] init];
        shape.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
        self.layer.mask = shape;
        self.layer.masksToBounds = YES; 
    } else if (self.down) {
        CAShapeLayer *shape = [[CAShapeLayer alloc] init];
        shape.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(10, 10)].CGPath;
        self.layer.mask = shape;
        self.layer.masksToBounds = YES;
    }
    
    if (!self.down) {
        CALayer *layer = [CALayer drawLineWithSize:self.bounds.size];
        [self.contentView.layer addSublayer:layer];
    }
}

@end
