//
//  CoachingWallViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/8/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "CoachingWallViewController.h"

@interface CoachingWallViewController () <UIWebViewDelegate>

@property (retain, nonatomic) NSMutableArray *posts;
@property (retain, nonatomic) NSDate *today;

//@property (strong, nonatomic) UIWebView *webViewDisplay;
@property (retain, nonatomic) NSMutableDictionary *cellHeights;
@property (retain, nonatomic) NSString *htmlStyle;

@end

@implementation CoachingWallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
    
    _cellHeights = [NSMutableDictionary dictionary];
    _posts = [[NSMutableArray alloc] init];
    _today = [NSDate getSavedWeek];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"New Post"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(addNewPost)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getCoachingWall];
}

#pragma mark - Public Methods

- (void)addNewPost {
    AddCoachPostViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                            bundle:[NSBundle mainBundle]]
                                                  instantiateViewControllerWithIdentifier:@"addcoach"];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)loadCoachingWallData:(NSDictionary *)info {
    _htmlStyle = info[@"a:CSS"];
    
    [_posts removeAllObjects];
    id object = info[kInfoPostsKey][kPostStringKey];
    if ([object isKindOfClass:[NSArray class]])
        [_posts addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_posts addObject:object];
    
    [_tableView reloadData];
}

- (void)getCoachingWall {
    [JBLoadingView startAnimating];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetCoachingWall;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.skip = @"0";
    parameter.take = @"10";
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetCoachingWallDidFinishWithResponse:response];
    });
}

- (NSString *)addHtmlStyle:(NSString *)html {
    //Add the content to the another string with styling and the original html content
    NSString *htmlStyling = [NSString stringWithFormat:@"<html>"
                             "<style type=\"text/css\">"
                             "%@"
                             "</style>"
                             "<body>"
                             "<p>%@</p>"
                             "</body></html>", _htmlStyle, html];
    
    NSLog(@"html: %@", htmlStyling);
    return htmlStyling;
}

#pragma mark - WSDLRequestResponse Delegate

- (void)GetCoachingWallDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    [self loadCoachingWallData:[response info]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
//    }

    NSString *raw = _posts[indexPath.row];
    if ([raw length] > 0) {
        NSString *wrappedHtml = [self addHtmlStyle:raw];
        CGFloat height = [self minHeightForText:wrappedHtml];
        UIWebView *webViewDisplay = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, tableView.frame.size.width - 20, height - 10)];
        [webViewDisplay loadHTMLString:wrappedHtml
                               baseURL:nil];
        webViewDisplay.tag = indexPath.row;
        webViewDisplay.delegate = self;
        webViewDisplay.layer.cornerRadius = 0;
        webViewDisplay.userInteractionEnabled = YES;
        webViewDisplay.multipleTouchEnabled = YES;
        webViewDisplay.clipsToBounds = YES;
        webViewDisplay.scalesPageToFit = NO;
        webViewDisplay.backgroundColor = [UIColor clearColor];
        webViewDisplay.scrollView.scrollEnabled = NO;
        webViewDisplay.scrollView.bounces = NO;
        
        
        [cell.contentView addSubview:webViewDisplay];
    }

    return cell;
    
}

- (CGFloat)minHeightForText:(NSString *)_text {
    return [_text
            sizeWithFont:[UIFont boldSystemFontOfSize:16]
            constrainedToSize:CGSizeMake(300, 999999)
            lineBreakMode:NSLineBreakByWordWrapping
            ].height;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_cellHeights objectForKey:[NSString stringWithFormat:@"%d", (indexPath.row)]]) {
        return [[_cellHeights objectForKey:[NSString stringWithFormat:@"%d", (indexPath.row)]] floatValue];
    }
    
    return 0;
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    // Asks the view to calculate and return the size that best fits its subviews.
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    frame.size.height = fittingSize.height;
    webView.frame = frame;
    webView.scalesPageToFit = NO;
    
    [_cellHeights setObject:[NSString stringWithFormat:@"%f", (frame.size.height + 10)]
                     forKey:[NSString stringWithFormat:@"%d", (webView.tag)]];
    
    [self.tableView beginUpdates];
    [self.tableView  endUpdates];
}

@end
