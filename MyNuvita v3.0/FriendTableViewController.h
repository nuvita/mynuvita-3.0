//
//  FriendTableViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/21/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@class FriendTableViewController;
@protocol FriendTableViewControllerDelegate <NSObject>

- (void)friendTableViewController:(FriendTableViewController *)viewController
                  didSelectOption:(NSString *)option;

@end
@interface FriendTableViewController : UITableViewController

@property (retain, nonatomic) id <FriendTableViewControllerDelegate> delegate;
@property (assign) NSInteger invites;

@end
