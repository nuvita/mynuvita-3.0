//
//  JustForYouViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/8/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "JustForYouViewController.h"

@interface JustForYouViewController ()

@property (retain, nonatomic) NSDate *today;
@property (retain, nonatomic) NSMutableArray *questions;

@end

@implementation JustForYouViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _questions = [[NSMutableArray alloc] init];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Quiz"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapQuiz)];
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    _today = [NSDate date];
    [self getCoachLesson];
}

#pragma mark - Public Methods

- (void)didTapQuiz {
    QuizViewController *viewController = (QuizViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                 bundle:[NSBundle mainBundle]]
                                        instantiateViewControllerWithIdentifier:@"Quiz"];
    
    viewController.questions = _questions;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (NSString *)addHtmlStyle:(NSString *)html {
    
    //Add the content to the another string with styling and the original html content
    NSString *htmlStyling = [NSString stringWithFormat:@"<html>"
                             "<style type=\"text/css\">"
                             "%@"
                             "</style>"
                             "<body>"
                             "<p>%@</p>"
                             "</body></html>", @"", html];
    
    return htmlStyling;
}

- (void)loadCoachLesson:(NSDictionary *)info {
    _lblTitle.text = info[@"a:Title"];
    
    if (info[@"a:HtmlText"]) {
        NSString *html = [[self addHtmlStyle:info[@"a:HtmlText"]]
                          stringByReplacingOccurrencesOfString:@"src=\"//player.vimeo.com/video/75608323?title=0&amp;amp;byline=0\""
                                                    withString:@"src=\"http://player.vimeo.com/video/75608323?title=0&amp;amp;byline=0"];
        [_webView loadHTMLString:html
                         baseURL:nil];
        
    }
    
    [_questions removeAllObjects];
    if ([info[kInfoQuestionKey][kQuestionsKey] isKindOfClass:[NSArray class]]) {
        [_questions addObjectsFromArray:info[kInfoQuestionKey][kQuestionsKey]];
    }
    
    if ([_questions count] == 0) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

- (void)getCoachLesson {
    [JBLoadingView startAnimating];
    
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetCoachLesson;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.weekDate = [_today description];
    parameter.lessonNumber = [[loginInfo objectForKey:kAPIGetCoachLesson] objectForKey:@"a:LessonNumber"];
    parameter.weekNumber = [[loginInfo objectForKey:@"CoachInfo"] objectForKey:@"a:WeekNumber"];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetCoachLessonDidFinishWithResponse:response];
    });
}

#pragma mark - WSDLRequestResponse Delegate

- (void)GetCoachLessonDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"GetCoachLessonDidFinishWithResponse: %@", [response info]);
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    [self loadCoachLesson:[response info]];
}

@end
