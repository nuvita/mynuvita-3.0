//
//  SecondPhaseViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 2/6/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "SecondPhaseViewController.h"
#import "HabitCell.h"

#define DEFAULT_OFFSET 10

@interface SecondPhaseViewController () <HabitCellDelegate>

@property (retain, nonatomic) NSDate *today;
@property (retain, nonatomic) NSMutableArray *habits;
@property (retain, nonatomic) NSMutableDictionary *habitInfo;
@end

@implementation SecondPhaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kbackgroundcolor;
    
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self
                   action:@selector(didTapPrev)
         forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self
                   action:@selector(didTapNext)
         forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonPrev],
                                               [[UIBarButtonItem alloc] initWithCustomView:buttonNext]];
    
    _today = [NSDate date];
    _habits = [NSMutableArray new];
    _habitInfo = [NSMutableDictionary new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
}

- (void)keyboardDidShow:(NSNotification*)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    JLog(@"HEIGHT: %f", keyboardFrameBeginRect.size.height);
    
//    [self.navigationController set]
//    _tableView.height = _tableView.height - keyboardFrameBeginRect.size.height;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getMemberNutritionHabits];
}

#pragma mark - Public Methods

- (void)didTapPrev {
    _today = [_today dateByAddingTimeInterval:-86400];
    [self getMemberNutritionHabits];
}

- (void)didTapNext {
    _today = [_today dateByAddingTimeInterval:86400];
    [self getMemberNutritionHabits];
}

- (void)loadMemberNutritionHabits:(NSDictionary *)info {
    [_habitInfo setDictionary:info];
    [_habits removeAllObjects];
    
    id object = info[@"a:Habits"][@"a:wsNutrtionHabit"];
    if ([object isKindOfClass:[NSArray class]])
        [_habits addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_habits addObject:object];
    
    JLog(@"habits: %@", _habits);
    [_tableView reloadData];
}

- (void)getMemberNutritionHabits {
    UIBarButtonItem *item = self.navigationItem.leftBarButtonItems.lastObject;
    item.enabled = ![[_today stringDateNutritionStyle] isEqualToString:[[NSDate date] stringDateNutritionStyle]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetMemberNutritionHabits;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [_today stringDateTimeFormat];
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetMemberNutritionHabitsFinishWithResponse:response];
        });
    });
}

#pragma mark - HabitCellDelegate

- (void)habitCellDidBeginEditing:(NSInteger)idx {
    NSLog(@"habitCellDidBeginEditing");
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
//    [_tableView scrollToRowAtIndexPath:indexPath
//                      atScrollPosition:UITableViewScrollPositionBottom
//                              animated:YES];
    
//    [_tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)habitCellDidEndEditing:(NSInteger)idx {

}

#pragma mark - WSDLRequestResponseDelegate

- (void)GetMemberNutritionHabitsFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    JLog(@"INFO: %@", [response info]);
    [self loadMemberNutritionHabits:[response info]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_habits count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    HabitCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[HabitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.tag = indexPath.row;
        cell.delegate = self;
    }
    
    if (indexPath.row == 0)
        cell.top = YES;
    if (indexPath.row == [_habits count] - 1)
        cell.bottom = YES;
    
    id object = _habits[indexPath.row];
    cell.tag = indexPath.row;
    [cell.btnHabit setTitle:object[@"a:Name"] forState:UIControlStateNormal];

    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    NSString *text = object[@"a:DailyGoal"];
    
    cell.lblDescription.width = tableView.width - 20;
    cell.lblDescription.backgroundColor = [UIColor clearColor];
    cell.lblDescription.text = text;
    cell.lblDescription.font = font;
    cell.lblDescription.height = [text stringHeightWithFont:font
                                                      width:tableView.width - 40];
    cell.lblDescription.numberOfLines = 0;
    cell.lblDescription.top = cell.btnHabit.bottom + DEFAULT_OFFSET;
    
    //a:Score
    cell.viewRate.backgroundColor = [UIColor clearColor];
    cell.viewRate.top = cell.lblDescription.bottom + DEFAULT_OFFSET;
    
    cell.textView.top = cell.viewRate.bottom + DEFAULT_OFFSET;
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id object = _habits[indexPath.row];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    NSString *text = object[@"a:DailyGoal"];
    CGFloat offset = 0;
    if (indexPath.row == 0 || indexPath.row == [_habits count] - 1)
        offset += 10;
    offset += [text stringHeightWithFont:font width:tableView.width - 40] - font.lineHeight;
    return 200 + offset;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.9f];
    view.layer.masksToBounds = NO;
    view.layer.borderColor = [kbordercolor CGColor];
    view.layer.borderWidth = 0.5f;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowRadius = 1.0f;
    view.layer.shadowOffset = CGSizeMake(0, 1.0f);
    
    CGFloat offset = 10;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(offset, 0, tableView.width / 2 - offset, tableView.sectionHeaderHeight)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    label.text = [_today stringDateNutritionStyle];
    [view addSubview:label];
    
    UIView *ratingView = [UIView createCustomViewSize:CGSizeMake(30 * 5, 30) withRating:[_habitInfo[@"a:DailyScore"] integerValue]];
    ratingView.layer.masksToBounds = YES;
    ratingView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    ratingView.layer.shadowOpacity = 0.5f;
    ratingView.layer.shadowRadius = 1.0f;
    ratingView.layer.shadowOffset = CGSizeMake(0, 1.0f);
    ratingView.backgroundColor = [UIColor clearColor];
    ratingView.left = tableView.width - offset - ratingView.width;
    ratingView.top = (tableView.sectionHeaderHeight - ratingView.height) / 2;
    [view addSubview:ratingView];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

@end
