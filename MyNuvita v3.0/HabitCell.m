//
//  HabitCell.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 3/19/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "HabitCell.h"

@interface HabitCell () <UITextViewDelegate>

@end

@implementation HabitCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [[[NSBundle mainBundle] loadNibNamed:@"HabitCell" owner:nil options:nil] lastObject];
    if (self) {
    }
    
    self.backgroundColor = [UIColor clearColor];
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    
    _btnHabit.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    _btnHabit.layer.cornerRadius = 4.0f;
    _btnHabit.layer.masksToBounds = NO;
    _btnHabit.layer.borderColor = [kbordercolor CGColor];
    _btnHabit.layer.borderWidth = 0.5f;
    _btnHabit.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _btnHabit.layer.shadowOpacity = 0.5f;
    _btnHabit.layer.shadowRadius = 1.0f;
    _btnHabit.layer.shadowOffset = CGSizeMake(0, 1.0f);
    [_btnHabit addTarget:self
                  action:@selector(didTapHabit:)
        forControlEvents:UIControlEventTouchUpInside];
    
    _textView.delegate = self;
    _textView.text = @"Make a note about your ranking!";
    _textView.textColor = [UIColor lightGrayColor];
    _textView.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    _textView.layer.cornerRadius = 2.0f;
    _textView.layer.masksToBounds = NO;
    _textView.layer.borderColor = [kbordercolor CGColor];
    _textView.layer.borderWidth = 0.5f;
    _textView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _textView.layer.shadowOpacity = 0.5f;
    _textView.layer.shadowRadius = 1.0f;
    _textView.layer.shadowOffset = CGSizeMake(0, 1.0f);
    
    [_viewRate.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        UIImage *image = [[obj imageForState:UIControlStateNormal] colorizedImage:kColorNuvitaOrange];
        [obj setImage:image forState:UIControlStateNormal];
        
        [obj addTarget:self
                action:@selector(didTapRate:)
      forControlEvents:UIControlEventTouchUpInside];
        
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.contentView.frame;
    frame.origin.x = 10;
    frame.size.width = frame.size.width - 20;
    
    if(self.top && self.bottom) {
        frame.origin.y = 10;
        frame.size.height -= 20;
        self.contentView.layer.cornerRadius = 2.0f;
        self.contentView.layer.masksToBounds = NO;
    } else if (self.top) {
        frame.origin.y = 10;
        frame.size.height = frame.size.height - 10;
        CAShapeLayer *shape = [[CAShapeLayer alloc] init];
        shape.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, frame.size.width, frame.size.height)
                                           byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                 cornerRadii:CGSizeMake(2, 2)].CGPath;
        self.contentView.layer.mask = shape;
        self.contentView.layer.masksToBounds = NO;
    } else if (self.bottom) {
        frame.size.height = frame.size.height - 10;
        CAShapeLayer *shape = [[CAShapeLayer alloc] init];
        shape.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, frame.size.width, frame.size.height)
                                           byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                 cornerRadii:CGSizeMake(2, 2)].CGPath;
        self.contentView.layer.mask = shape;
        self.contentView.layer.masksToBounds = NO;
    }

    
    if (!self.bottom) {
        CALayer *layer = [CALayer drawLineWithSize:CGSizeMake(frame.size.width, frame.size.height)];
        [self.contentView.layer addSublayer:layer];
    }
    
    self.contentView.frame = frame;
}

- (void)didTapRate:(UIButton *)button {
    
}

- (void)didTapHabit:(UIButton *)button {
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:kColorNuvitaBlue];
    button.layer.borderColor = [[kColorNuvitaBlue darkerColor] CGColor];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"Make a note about your ranking!"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
//    if (_delegate && [_delegate respondsToSelector:@selector(habitCellDidBeginEditing:)]) {
//        [_delegate habitCellDidBeginEditing:self.tag];
//    }
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([textView.text length] == 0) {
        textView.text = @"Make a note about your ranking!";
        textView.textColor = [UIColor lightGrayColor];
        [textView resignFirstResponder];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text length] == 0) {
        textView.text = @"Make a note about your ranking!";
        textView.textColor = [UIColor lightGrayColor];
        [textView resignFirstResponder];
    }
    
//    if (_delegate && [_delegate respondsToSelector:@selector(habitCellDidEndEditing:)]) {
//        [_delegate habitCellDidEndEditing:self.tag];
//    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

@end
