//
//  CardioViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/21/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "CardioViewController.h"

@interface CardioViewController ()

@property (retain, nonatomic) NSMutableArray *sessions;
@property (retain, nonatomic) NSDate *today;

@end

@implementation CardioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSInteger width = (_tableView.frame.size.width / 6);
    CGRect frame = _tableView.frame;
    frame.size.width = width * 6;
    _tableView.frame = frame;
    
    _sessions = [[NSMutableArray alloc] init];
    _today = [NSDate getSavedWeek];
    [self loadSettings];
    [self loadNavigationItems];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getCardioWeek];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Public Methods

- (void)moveNextClicked:(id)sender {
    _today = [_today getNextWeek];
    [self getCardioWeek];
}

- (void)movePrevClicked:(id)sender {
    _today = [_today getLastWeek];
    [self getCardioWeek];
}

- (void)loadNavigationItems {
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    buttonPrev.backgroundColor = [UIColor clearColor];
    buttonPrev.tintColor = kColorNuvitaBlue;
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self action:@selector(movePrevClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self action:@selector(moveNextClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[
                                                [[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                                [[UIBarButtonItem alloc] initWithCustomView:buttonPrev],
                                                ];
}

- (void)loadSettings {
    NSInteger backgroundWidth = (self.view.frame.size.width - 18) * .78;
    NSInteger boxWidth = backgroundWidth / 4;
    
    int x = 10;
    UIView *background = [[UIView alloc] initWithFrame:CGRectMake(9, 149, backgroundWidth - 1, 92)];
    background.backgroundColor = [UIColor clearColor];
    background.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    background.layer.borderWidth = 1;
    
    
    for (int i = 0; i < 4; i++) {
        
        UIView *boxView = [[UIView alloc] initWithFrame:CGRectMake(x, 150, boxWidth/*55*/, 90)];
        boxView.tag = i + 222;
        boxView.backgroundColor = [UIColor clearColor];
//        boxView.layer.borderWidth = 1;
//        boxView.layer.borderColor = [[UIColor yellowColor] CGColor];
        
        
        if (i < 3) {
            CALayer *cornerBorder = [CALayer layer];
            cornerBorder.frame = CGRectMake(/*54*/boxWidth - 1, 0, 1, 90);
            cornerBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
            [boxView.layer addSublayer:cornerBorder];
        }
        
        
        int y = 0;
        for (int x = 0; x < 3; x ++) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, y, boxWidth/*54*/, 30)];
            label.tag = x;
            label.numberOfLines = 0;
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:12];
            label.textColor = [UIColor blackColor];
            label.text = @"";
            
            [boxView addSubview:label];
            y += 30;
        }

        x += boxWidth/*55*/;
        [self.view addSubview:boxView];
    }
    
    [self.view addSubview:background];

    NSInteger progressWidth = (self.view.frame.size.width - 18) * .20;
    NSInteger progressOffset = self.view.frame.size.width - 9 - progressWidth;
    UIView *progressView = [[UIView alloc] initWithFrame:CGRectMake(progressOffset, 149, progressWidth, 92)];
    progressView.backgroundColor = [UIColor clearColor];
    progressView.tag = 333;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(/*240*/progressOffset, 149, progressWidth, 92)];
    label.backgroundColor = [UIColor clearColor];
    label.tag = 444;
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.layer.borderWidth = 1;
    label.textColor = kColorNuvitaBlue;
    label.text = @"0%";
    
    [self.view addSubview:progressView];
    [self.view addSubview:label];
    
    UILabel *lblgoal = [[UILabel alloc] initWithFrame:CGRectMake(9, 250, backgroundWidth, 16)];
    lblgoal.backgroundColor = [UIColor clearColor];
    lblgoal.textAlignment = NSTextAlignmentRight;
    lblgoal.font = [UIFont boldSystemFontOfSize:14];
    lblgoal.textColor = [UIColor colorWithWhite:0.1 alpha:1];
    lblgoal.text = @"This weeks goal:";
    
    UILabel *lblGoalValue = [[UILabel alloc] initWithFrame:CGRectMake(progressOffset, 250, progressWidth, 16)];
    lblGoalValue.backgroundColor = [UIColor clearColor];
    lblGoalValue.tag = 99999;
    lblGoalValue.textAlignment = NSTextAlignmentRight;
    lblGoalValue.font = [UIFont boldSystemFontOfSize:14];
    lblGoalValue.textColor = [UIColor colorWithWhite:0.1 alpha:1];
    lblGoalValue.text = @"0 min";
    
    [self.view addSubview:lblgoal];
    [self.view addSubview:lblGoalValue];
    
    UILabel *lblprogress = [[UILabel alloc] initWithFrame:CGRectMake(9, 270, backgroundWidth, 16)];
    lblprogress.backgroundColor = [UIColor clearColor];
    lblprogress.textAlignment = NSTextAlignmentRight;
    lblprogress.font = [UIFont boldSystemFontOfSize:14];
    lblgoal.textColor = [UIColor colorWithWhite:0.1 alpha:1];
    lblprogress.text = @"Your progress:";
    
    UILabel *lblProgressValue = [[UILabel alloc] initWithFrame:CGRectMake(progressOffset, 270, progressWidth, 16)];
    lblProgressValue.backgroundColor = [UIColor clearColor];
    lblProgressValue.tag = 88888;
    lblProgressValue.textAlignment = NSTextAlignmentRight;
    lblProgressValue.font = [UIFont boldSystemFontOfSize:14];
    lblProgressValue.textColor = [UIColor colorWithWhite:0.1 alpha:1];
    lblProgressValue.text = @"0 min";
    
    [self.view addSubview:lblprogress];
    [self.view addSubview:lblProgressValue];
}

- (void)loadCardioWeekInfo:(NSDictionary *)info {
    _lblWeek.text = info[kInfoWeekKey];
    
    [_sessions removeAllObjects];
    id object = info[kHRSessionsKey][kSessionKey];
    if ([object isKindOfClass:[NSArray class]])
        [_sessions addObjectsFromArray:info[kHRSessionsKey][kSessionKey]];
    if ([object isKindOfClass:[NSDictionary class]])
        [_sessions addObject:object];
    [_tableView reloadData];
    
    UIView *box1 = [self.view viewWithTag:222];
    [box1.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = obj;
            switch (idx) {
                case 0:
                    label.textColor = kColorNuvitaBlue;
                    label.text = @"Target";
                    break;
                
                case 1:
                    label.textColor = kColorNuvitaBlue;
                    label.text = @"HR";
                    break;
                    
                case 2:
                    label.textColor = kColorNuvitaBlue;
                    label.text = @"Zones";
                    break;
                    
                default:
                    break;
            }
        }
    }];
    
    UIView *box2 = [self.view viewWithTag:223];
    [box2.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = obj;
            switch (idx) {
                case 0:
                    label.textColor = kColorNuvitaBlue;
                    label.text = @"Below";
                    break;
                    
                case 1:
                    label.font = [UIFont boldSystemFontOfSize:10];
                    label.backgroundColor = kColorNuvitaOrange;
                    label.text = info[kInfoBelowZonePercentKey];
                    break;
                    
                case 2:
                    label.font = [UIFont boldSystemFontOfSize:10];
                    label.backgroundColor = kColorNuvitaOrange;
                    label.text = info[kInfoBelowZoneBPMKey];
                    break;
                    
                default:
                    break;
            }
        }
    }];
    
    UIView *box3 = [self.view viewWithTag:224];
    [box3.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = obj;
            switch (idx) {
                case 0:
                    label.textColor = kColorNuvitaBlue;
                    label.text = @"In Zone";
                    break;
                    
                case 1:
                    label.font = [UIFont boldSystemFontOfSize:10];
                    label.backgroundColor = kColorNuvitaGreen;
                    label.text = info[kInfoInZonePercentKey];
                    break;
                    
                case 2:
                    label.font = [UIFont boldSystemFontOfSize:10];
                    label.backgroundColor = kColorNuvitaGreen;
                    label.text = info[kInfoInZoneBPMKey];
                    break;
                    
                default:
                    break;
            }
        }
    }];
    
    UIView *box4 = [self.view viewWithTag:225];
    [box4.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = obj;
            switch (idx) {
                case 0:
                    label.textColor = kColorNuvitaBlue;
                    label.text = @"Above";
                    break;
                    
                case 1:
                    label.font = [UIFont boldSystemFontOfSize:10];
                    label.backgroundColor = kColorNuvitaRed;
                    label.text = info[kInfoAboveZonePercentKey];
                    break;
                    
                case 2:
                    label.font = [UIFont boldSystemFontOfSize:10];
                    label.backgroundColor = kColorNuvitaRed;
                    label.text = info[kInfoAboveZoneBPMKey];
                    break;
                    
                default:
                    break;
            }
        }
    }];
    
    //... progress
    UIColor *color;
    int progress = [info[kInfoProgressPercentKey] intValue];
    if (progress < 69) {
        color = kColorNuvitaGray;
    } else if (progress >= 69 && progress <= 89) {
        color = kColorNuvitaOrange;
    } else if (progress >= 89){
        color = kColorNuvitaGreen;
    }
    
    UIView *progressView = (UIView *)[self.view viewWithTag:333];
    progressView.backgroundColor = color;
    CGRect frame = progressView.frame;
    frame.origin.y = 149;
    frame.size.height = 92;
    progressView.frame = frame;
    if (progress >= 100) {
        frame.origin.y = 149;
        frame.size.height = frame.size.height;
    } else {
        int progressPercent = (frame.size.height * progress) / 100.0;
        frame.size.height = progressPercent;
        frame.origin.y += 92 - progressPercent;
    }
    progressView.frame = frame;
    
    UILabel *label = (UILabel *)[self.view viewWithTag:444];
    label.text = [NSString stringWithFormat:@"%d%%", progress];
    //......
    
    UILabel *lblGoalValue = (UILabel *)[self.view viewWithTag:99999];
    lblGoalValue.text = [NSString stringWithFormat:@"%@ min", info[kInfoGoalValueKey]];
    
    UILabel *lblProgressValue = (UILabel *)[self.view viewWithTag:88888];
    lblProgressValue.text = [NSString stringWithFormat:@"%d min", [info[kInfoProgressValueKey] intValue]];
}

- (void)getCardioWeek {
    if ([[_today getLastWeek] isEqualToDate:[[NSDate date] getLastWeek]]) {
        [[self.navigationItem.rightBarButtonItems firstObject] setEnabled:NO];
    } else {
        [[self.navigationItem.rightBarButtonItems firstObject] setEnabled:YES];
    }
    
    [JBLoadingView startAnimating];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetCardioWeek;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.date = [_today description];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetCardioWeekDidFinishWithResponse:response];
    });
    
}
    
#pragma mark - WSDLRequestResponse Delegate
    
- (void)GetCardioWeekDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"response: %@", response.info);
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if (![[response status] isSuccess]) {
        [_sessions removeAllObjects];
        [_tableView reloadData];
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    [self loadCardioWeekInfo:response.info];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_sessions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor lightGrayColor];
    }
    
    NSDictionary *data = _sessions[indexPath.row];
    
    __block CGFloat width = tableView.frame.size.width / 6;
    NSLog(@"width: %f", width);
    NSArray *keys = @[kSessionDateKey, kSessionCalorieKey, kSessionBelowKey, kSessionInKey, kSessionAboveKey, kSessionDurationKey];
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        
        int x = idx * width;
        int w = width - 1;
        if (idx == 0) {
            x = 1;
            w = width - 2;
        }
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, w, 25)];
        label.backgroundColor = [self colorAtIndex:idx];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:10];
        label.textColor = [UIColor blackColor];
        label.text = [obj isEqualToString:kSessionDateKey] ? [self shortenDateString:data[obj]] : data[obj];

        [cell.contentView addSubview:label];
    }];
    
    
    CGFloat height = [self tableView:_tableView heightForRowAtIndexPath:indexPath];
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, height - 1, tableView.frame.size.width, 1)];
    separator.backgroundColor = [UIColor lightGrayColor];
    [cell.contentView addSubview:separator];
    return cell;
}

- (NSString *)shortenDateString:(NSString *)string {
    NSArray *array = [string componentsSeparatedByString:@"/"];
    return [NSString stringWithFormat:@"%@/%@", array[0], array[1]];
}

- (UIColor *)colorAtIndex:(NSInteger)idx {
    
    if (idx == 2)
        return kColorNuvitaOrange;
    
    if (idx == 3)
        return kColorNuvitaGreen;
    
    if (idx == 4)
        return kColorNuvitaRed;
    
    return [UIColor whiteColor];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor lightGrayColor];
    view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    view.layer.borderWidth = 1;
    
    __block CGFloat width = tableView.frame.size.width / 6;
    NSArray *array = @[@"Date", @"Cals", @"Below", @"In Zone", @"Above", @"Total"];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(idx * width, 0, width - 1, 25)];
        label.backgroundColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:12];
        label.textColor = kColorNuvitaBlue;
        label.text = obj;
        
        [view addSubview:label];
    }];
    
    return view;
}

- (NSString *)getTotalValueForKey:(NSString *)key {
    __block int total = 0;
    [_sessions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        total += [obj[key] integerValue];
    }];
    
    return [NSString stringWithFormat:@"%d", total];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor lightGrayColor];
    view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    view.layer.borderWidth = 1;
    
    __block CGFloat width = tableView.frame.size.width / 6;
    NSArray *array = @[@"Total", [self getTotalValueForKey:kSessionCalorieKey], [self getTotalValueForKey:kSessionBelowKey], [self getTotalValueForKey:kSessionInKey], [self getTotalValueForKey:kSessionAboveKey], [self getTotalValueForKey:kSessionDurationKey]];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(idx * width, 0, width - 1, 25)];
        label.backgroundColor = [self colorAtIndex:idx];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont boldSystemFontOfSize:12];
        label.textColor = [UIColor blackColor];
        label.text = obj;
        
        [view addSubview:label];
    }];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

@end
