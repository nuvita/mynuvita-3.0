//
//  AddMealViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 2/7/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "AddMealViewController.h"

@interface AddMealViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, CMPopTipViewDelegate>

@property (retain, nonatomic) NSMutableDictionary *editMealDetails;
@property (retain, nonatomic) NSMutableArray *favorites;
@property (assign) MealEditingMode editingMode;

@end

@implementation AddMealViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:kbackgroundcolor];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    [self.navigationItem setTitleView:imageView];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapSave)];
    _editMealDetails = [NSMutableDictionary new];
    _favorites = [NSMutableArray new];
    _editingMode = /*(_mealInfo && [[_mealInfo allKeys] count] > 1) ? EditFavorite :*/ NotAFavorite;
    [self loadControllerSettings:_mealInfo
                 withEditingMode:_editingMode];
    
    
    UIImage *defaultImage = [[UIImage imageNamed:@"camera-icon-128"] colorizedImage:kColorNuvitaOrange];
    _imgDefault.image = defaultImage;
    [_imgDefault autoResizeContent];
    
    _tableView.layer.cornerRadius = 3.0f;
    _tableView.layer.masksToBounds = YES;
    UIView *content = (UIView *)[_tableView superview];
    content.layer.cornerRadius = 2.0f;
    content.layer.masksToBounds = NO;
    [content addShadow];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] cleanDisk];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Public Methods

- (void)loadMemberFavoriteMeals:(NSDictionary *)info {
    [_favorites removeAllObjects];
    
    id object = info[@"a:Meals"][@"a:FavoriteMeal"];
    if ([object isKindOfClass:[NSArray class]])
        [_favorites addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_favorites addObject:object];
    
    if ([_favorites count] == 0) {
        CMPopTipView *popup = [self createTipPopup];
        popup.message = @"Enable this button to add new favorite.";
        [popup presentPointingAtView:_btnFavorite
                              inView:self.view
                            animated:YES];
    }
    
    [_tableView reloadData];
}

- (BOOL)isMealExist:(NSDictionary *)meal {
    BOOL exist = NO;
    for (NSDictionary *object in _favorites) {
        if ([object[kMealFavoriteIdKey] isEqualToString:_editMealDetails[kMealFavoriteIdKey]]) {
            exist = YES;
        }
    }

    return exist;
}

- (void)loadControllerSettings:(NSDictionary *)details withEditingMode:(MealEditingMode)mode {
    [_editMealDetails removeAllObjects];
    [_editMealDetails addEntriesFromDictionary:details];
    
    BOOL editing = ![[details allKeys] count] == 0;
    _editMealDetails[@"editing"] = [NSNumber numberWithBool:editing];
    _editingMode = mode;
    _imageView.image = nil;
    
    JLog(@"details: %@", _editMealDetails);
    
    [_imageView setImageWithURL:[NSURL URLWithString:details[kMealPhotoUrlKey]]];
    _imageView.layer.masksToBounds = YES;
    _imageView.layer.cornerRadius = 3.0f;
    [_imageView autoResizeContent];
    
    _textView.text = details[kMealDescriptionKey];
    [self textViewDidChange:_textView];
    
    UIView *content = (UIView *)[self.view viewWithTag:1000];
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, content.height - 0.5, content.width - 0.5, 0.5);
    layer.backgroundColor = [kbordercolor CGColor];
    [content.layer insertSublayer:layer atIndex:0];
    [content.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        [obj setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        obj.backgroundColor = kColorNuvitaBlue;
        obj.layer.cornerRadius = obj.width / 2;
        obj.layer.masksToBounds = NO;
        obj.layer.shadowOffset = CGSizeMake(-1, 1);
        obj.layer.shadowRadius = 2.0f;
        obj.layer.shadowOpacity = 0.5f;
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            if ([obj tag] == [details[kMealTypeKey] integerValue]) {
                [self didTapType:obj];
            }
        });
    }];
    
    UIColor *defaultColor = [UIColor colorForRating:1];
    [self.view.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIView class]] && ([obj tag] == 2000 || [obj tag] == 4000)) {
            obj.layer.cornerRadius = 2.0f;
            obj.layer.masksToBounds = NO;
            [obj addShadow];
        }
    }];
    
    UIView *ratings = [self.view viewWithTag:3000];
    ratings.layer.cornerRadius = 2.0f;
    ratings.layer.masksToBounds = NO;
    [ratings addShadow];
    [ratings.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]] && ([obj tag] >= 1 && [obj tag] <= 5)) {
            [obj setImage:[[UIImage imageNamed:@"icon-heart-64"] colorizedImage:defaultColor]
                 forState:UIControlStateNormal];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                if ([obj tag] == [details[kMealRankKey] integerValue]) {
                    [self didTapRate:obj];
                }
            });
        }
        
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel *)obj;
            label.textColor = defaultColor;
        }
        
        if ([obj isKindOfClass:[UIButton class]] && [obj tag] == 10) {
            [obj setEnabled:NO];
            [obj setImage:[[UIImage imageNamed:@"favorite-icon-64"] colorizedImage:defaultColor]
                 forState:UIControlStateNormal];
            obj.layer.masksToBounds = NO;
            obj.layer.shadowOffset = CGSizeMake(-0.5, 0.5);
            obj.layer.shadowRadius = 1.0f;
            obj.layer.shadowOpacity = 0.4f;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                if (details[kMealFavoriteNumberKey] || details[kMealFavoriteIdKey]) {
                    [obj setEnabled:YES];
                    [self didTapFavorite:obj];
                }
            });
            
        }
        
        if ([obj isKindOfClass:[UIButton class]] && [obj tag] == 20) {
            [obj setHidden:YES];
            [obj setImage:[[UIImage imageNamed:@"delete-icon64"]
                           colorizedImage:kColorNuvitaRed]
                 forState:UIControlStateNormal];
            obj.layer.masksToBounds = NO;
            obj.layer.shadowOffset = CGSizeMake(-0.5, 0.5);
            obj.layer.shadowRadius = 1.0f;
            obj.layer.shadowOpacity = 0.4f;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                if (details[kMealFavoriteNumberKey] || details[kMealFavoriteIdKey] || details[kMealNumberKey]) {
                    [obj setHidden:NO];
                    [obj addTarget:self
                            action:@selector(didTapDelete:)
                  forControlEvents:UIControlEventTouchUpInside];
                }
            });
        }
    }];

}

- (void)assignKeysFromDetails:(NSDictionary *)details
              withEditingMode:(MealEditingMode)mode
                   completion:(void(^)(NSDictionary *data))completion {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *holder = [NSMutableDictionary keysWithMealDefaults];
    holder[mIDKey] = [defaults objectForKey:kInfoIDKey];
    holder[mDateTimeKey] = [_today stringDateTimeFormat];//[[NSDate date] stringDateTimeFormat];
    holder[mDescriptionKey] = [_textView.text description];
    holder[mRankKey] = details[mRankKey];
    holder[mTypeKey] = details[mTypeKey];
    holder[mPhotoKey] = [[_imageView.image data] base64Encoding] ?: [NSNull null];
    holder[mFavoriteEditModeKey] = [NSString stringWithFormat:@"%d", mode];
    
    switch (mode) {
        case NotAFavorite:
            holder[mNumberKey] = details[kMealNumberKey] ?: [NSNull null];
            holder[mFavoriteIdKey] = [NSNull null];
            break;
            
        case FromFavorite:
            holder[mDescriptionKey] = [NSNull null];
            holder[mTypeKey] = [NSNull null];
            holder[mRankKey] = [NSNull null];
            holder[mNumberKey] = [NSNull null];
            holder[mPhotoKey] = [NSNull null];
            holder[mFavoriteIdKey] = details[kMealFavoriteIdKey];
            break;
            
        case NewFavorite:
            holder[mNumberKey] = details[kMealNumberKey] ?: [NSNull null];
            holder[mFavoriteIdKey] = [NSNull null];
            break;
            
        case EditFavorite:
            holder[mNumberKey] = [NSNull null];
            holder[mFavoriteIdKey] = details[kMealFavoriteIdKey];
            break;
            
        case NoEditFavorite:
            holder[mNumberKey] = details[kMealNumberKey];
            holder[mFavoriteIdKey] = [NSNull null];
            if (details[kMealFavoriteIdKey])
                holder[mFavoriteIdKey] = details[kMealFavoriteIdKey];
            if (details[kMealFavoriteNumberKey])
                holder[mFavoriteIdKey] = details[kMealFavoriteNumberKey];
            break;
            
        case DeleteFavorite:
            holder[mPhotoKey] = [NSNull null];
            holder[mDateTimeKey] = [_today stringDateOnlyFormat];
            holder[mNumberKey] = details[kMealNumberKey] ?: @"0";
            holder[mFavoriteIdKey] = [NSNull null];
            if (details[kMealFavoriteIdKey])
                holder[mFavoriteIdKey] = details[kMealFavoriteIdKey];
            break;
            
        default:
            break;
    }
    
    if (completion) {
        completion(holder);
    }
}

- (NSString *)stringRateValue:(NSInteger)rank {
    NSArray *ratings = @[@"Very Unhealthy", @"Unhealthy", @"Moderate", @"Healthy", @"Very Healthy"];
    return ratings[rank - 1];
}

- (NSString *)stringMealTypeValue:(MealType)type {
    NSArray *types = @[@"breakfast", @"AM snack", @"lunch", @"PM snack", @"dinner"];
    return types[type - 1];
}

- (void)getMemberFavoriteMealsType:(MealType)type {
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    NSDictionary *data = @{kInfoIDKey:[loginInfo objectForKey:kInfoIDKey],
                           mTypeSmallKey:_editMealDetails[mTypeKey]};
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetMemberFavoriteMeals;
    
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.favoriteMealDetails = data;
    WSDLRequestResponse *response = [binding parseBindingRequestUsingParameter:parameter];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self GetMemberFavoriteMealsFinishWithResponse:response];
    });
}

#pragma mark - WSDLRequestResponseDelegate

- (void)SaveMemberMealFinishWithResponse:(WSDLRequestResponse *)response favorite:(BOOL)favorite {
    [JBLoadingView stopAnimating];
    JLog(@"%@", [response info]);
    if (![response info]) {
        [UIAlertView displayAlert:@"Unable to process your request! Please try again later."
                            title:@"Error"];
        return;
    }
    
    if (!favorite && ![response.info[kInfoErrorKey] boolValue]) {
        [UIAlertView displayAlert:response.info[kMessageKey]];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if (favorite && ![response.info[kInfoErrorKey] boolValue]) {
        [UIAlertView displayAlert:response.info[kMessageKey]];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self getMemberFavoriteMealsType:(MealType)[_editMealDetails[mTypeKey] integerValue]];
    });
}

- (void)GetMemberFavoriteMealsFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
//    JLog(@"GetMemberFavoriteMealsFinishWithResponse: %@", [response info]);
    [self loadMemberFavoriteMeals:[response info]];
}

#pragma mark - IBActions UIAlertViewDelegate

- (void)saveDetailsFromFavorite:(BOOL)flag {
    CMPopTipView *popup = [self createTipPopup];
    popup.delegate = self;
    
    if ([_textView.text length] == 0) {
        popup.message = @"Please enter meal description!";
        [popup presentPointingAtView:[self.view viewWithTag:2000]
                              inView:self.view
                            animated:YES];
        [_textView becomeFirstResponder];
        return;
    }
    
    if (!_editMealDetails[mTypeKey]) {
        popup.message = @"Please choose meal type!";
        [popup presentPointingAtView:[self.view viewWithTag:1000]
                              inView:self.view
                            animated:YES];
        return;
    }
    
    if (!_editMealDetails[mRankKey]) {
        popup.message = @"Please rate your meal!";
        [popup presentPointingAtView:[self.view viewWithTag:3000]
                              inView:self.view
                            animated:YES];
        return;
    }
    
    [JBLoadingView startAnimating];
    [self assignKeysFromDetails:_editMealDetails
                withEditingMode:_editingMode
                     completion:^(NSDictionary *data) {
//                         NSLog(@"data: %@", data);
                         [_editMealDetails removeAllObjects];
                         [_editMealDetails addEntriesFromDictionary:data];
                         
                         WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
                         binding.logXMLInOut = YES;
                         binding.api = kAPISaveMemberMeal;
                         WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
                         parameter.mealDetails = data;
                         
                         WSDLRequestResponse *response = [binding parseBindingRequestUsingParameter:parameter];
                         dispatch_async(dispatch_get_main_queue(), ^(void) {
                             [self SaveMemberMealFinishWithResponse:response favorite:flag];
                         });
                     }];
}

- (void)didTapSave {
    [self.view endEditing:YES];
    [self saveDetailsFromFavorite:NO];
}

- (IBAction)didTapType:(id)sender {
    [self.view endEditing:YES];
    UIButton *button = sender;
    UIView *ratings = [sender superview];
    if ([button isTouchInside]) {
        [_btnDelete setHidden:YES];
        [_btnFavorite setTag:11];
        [self didTapFavorite:_btnFavorite];
    }
    
    [ratings.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        [obj setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [obj setBackgroundColor:kColorNuvitaBlue];
    }];
    
    [button setBackgroundColor:kColorNuvitaOrange];
    _editMealDetails[mTypeKey] = [NSString stringWithFormat:@"%d", (int)[button tag]];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        BOOL isEditing = [_editMealDetails[@"editing"] boolValue];
        if ([_favorites count] == 0 || !isEditing) {
            [_favorites removeAllObjects];
            [_tableView reloadData];
            [self getMemberFavoriteMealsType:(int)[button tag]];
        }
        _editMealDetails[@"editing"] = @NO;
    });
}

- (IBAction)didTapRate:(id)sender {
    [self.view endEditing:YES];
    UIButton *button = sender;
    UIView *ratings = [sender superview];
    if ([button isTouchInside]) {
        [_btnDelete setHidden:YES];
        [_btnFavorite setTag:11];
        [self didTapFavorite:_btnFavorite];
    }
    
    UIColor *rateColor = [UIColor colorForRating:[button tag]];
    _lblRating.text = [self stringRateValue:(int)[button tag]];
    _lblRating.textColor = rateColor;
    
    [ratings.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]] && ([obj tag] >= 1 && [obj tag] <= 5)) {
            [obj setImage:[[UIImage imageNamed:@"icon-heart-64"] colorizedImage:rateColor]
                 forState:UIControlStateNormal];
        }
        
        if ([obj isKindOfClass:[UIButton class]] && [obj tag] <= [button tag]) {
            [obj setImage:[[UIImage imageNamed:@"icon-fill-heart-64"]
                              colorizedImage:rateColor]
                    forState:UIControlStateNormal];
        }
    }];
    
    _editMealDetails[mRankKey] = [NSString stringWithFormat:@"%d", (int)[button tag]];
}

- (IBAction)didTapFavorite:(id)sender {
    UIButton *button = sender;
    if ([button tag] == 10) {
        [button setTag:11];
        [button setImage:[[UIImage imageNamed:@"favorite-fill-icon-64"] colorizedImage:kColorNuvitaBlue]
             forState:UIControlStateNormal];
//        _editingMode = (_editMealDetails[kMealFavoriteIdKey] || _editMealDetails[kMealFavoriteNumberKey]) ? _editingMode : NewFavorite;
        _editingMode = [button isTouchInside] ? NewFavorite : _editingMode;
        JLog(@"mode: %d", _editingMode);
        return;
    }
    
    [button setTag:10];
    [button setImage:[[UIImage imageNamed:@"favorite-icon-64"] colorizedImage:kColorNuvitaGray]
            forState:UIControlStateNormal];
//    _editingMode = (_editMealDetails[kMealFavoriteIdKey] || _editMealDetails[kMealFavoriteNumberKey]) ? FromFavorite : NotAFavorite;
    _editingMode = /*_editMealDetails[kMealNumberKey] ? _editingMode :*/ NotAFavorite;
    JLog(@"mode: %d", _editingMode);
}

- (void)didTapDelete:(UIButton *)button {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Are you sure to delete this meal?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertView show];
}

- (void)processDeletedFromFavorite:(BOOL)favorite {
    [JBLoadingView startAnimating];
    [self assignKeysFromDetails:_editMealDetails
                withEditingMode:_editingMode
                     completion:^(NSDictionary *data) {
                         NSLog(@"data: %@", data);
                         [_editMealDetails removeAllObjects];
                         [_editMealDetails addEntriesFromDictionary:data];
                         
                         WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
                         binding.logXMLInOut = YES;
                         binding.api = kAPISaveMemberMeal;
                         WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
                         parameter.mealDetails = data;
                         
                         WSDLRequestResponse *response = [binding parseBindingRequestUsingParameter:parameter];
                         dispatch_async(dispatch_get_main_queue(), ^(void) {
                             [self SaveMemberMealFinishWithResponse:response favorite:favorite];
                         });
                     }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        _editingMode = DeleteFavorite;
        NSIndexPath *indexPath = [_tableView indexPathForSelectedRow];
        if (indexPath) {
            [_favorites removeObjectAtIndex:[indexPath row]];
            [_tableView beginUpdates];
            [_tableView deleteRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
            [_tableView endUpdates];
            
            [self processDeletedFromFavorite:YES];
            return;
        }
        
        [self processDeletedFromFavorite:NO];
    }
}

#pragma mark - CMPopTipView + Delegate

- (CMPopTipView *)createTipPopup {
    CMPopTipView *popup = [[CMPopTipView alloc] init];
//    popup.delegate = self;
    popup.has3DStyle = NO;
    popup.dismissTapAnywhere = YES;
    popup.cornerRadius = 10.0f;
    popup.borderColor = [UIColor clearColor];
    popup.backgroundColor = [kColorNuvitaRed darkerColor];
    popup.hasGradientBackground = NO;
    popup.textFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    [popup autoDismissAnimated:YES atTimeInterval:3.0];
    return popup;
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView {
    NSLog(@"pop: %@", popTipView);
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    UILabel *placeholder = (UILabel *)[[self.view viewWithTag:2000] viewWithTag:1000];
    placeholder.textColor = [textView.text length] > 0 ? [UIColor whiteColor] : [UIColor lightGrayColor];
    
    UIView *ratings = [self.view viewWithTag:3000];
    [ratings.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            obj.enabled = [textView.text length] > 0;
        }
    }];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text length] > 0) {
        [_btnDelete setHidden:YES];
        [_btnFavorite setTag:11];
        [self didTapFavorite:_btnFavorite];
    }
}

//- (void)textViewDidEndEditing:(UITextView *)textView {
//    UIView *ratings = [self.view viewWithTag:3000];
//    ratings.userInteractionEnabled = [_textView.text length] > 0;
//}

#pragma mark - UIImagePickerControllerDelegate

- (IBAction)didTapCamera:(id)sender {
    [self.view endEditing:YES];
    
    UIButton *button = (UIButton *)sender;
    if ([button isTouchInside]) {
        [_btnDelete setHidden:YES];
        [_btnFavorite setTag:11];
        [self didTapFavorite:_btnFavorite];
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose from library", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (!selectedImage) {
        selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    if (selectedImage) {
        _imageView.image = selectedImage;
        [_imageView autoResizeContent];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != 2) {
        [self showImagePickerWithSourceType:buttonIndex == 0 ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

- (void)showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        
    } else {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_favorites count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MealCell";
    MealCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (!cell)
    {
        cell = [[MealCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    id data = _favorites[indexPath.row];
    CGFloat height = tableView.rowHeight + [self calculateOffsetHeight:data[kMealDescriptionKey]];
    CGFloat rateHeight = 20;

//    UIImage *defaultImage = [[UIImage imageNamed:@"camera-icon-128"] colorizedImage:kColorNuvitaOrange];
    [cell.imgMeal setImageWithURL:[NSURL URLWithString:data[kMealPhotoUrlKey]]];
    
    cell.lblMeal.text = data[kMealDescriptionKey];
    [cell.lblMeal autoHeight];
    cell.lblMeal.left = cell.imgMeal.right + 15;
    cell.lblMeal.top = (height - cell.lblMeal.height - rateHeight) / 2;

    
    UIView *view = [UIView createCustomViewSize:CGSizeMake(rateHeight * 5, rateHeight)
                                     withRating:[data[kMealRankKey] integerValue]];
    view.backgroundColor = [UIColor clearColor];
    view.top = cell.lblMeal.bottom;
    view.left = cell.imgMeal.right + 15;
    cell.viewRate = view;
    [cell.contentView addSubview:cell.viewRate];
    view = nil;

    CALayer *layer = [CALayer drawLineWithSize:CGSizeMake(tableView.frame.size.width, height)];
    [cell.contentView.layer addSublayer:layer];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = _favorites[indexPath.row];
    CGFloat offset = [self calculateOffsetHeight:data[kMealDescriptionKey]];
    return 50 + offset;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (NSInteger)calculateOffsetHeight:(NSString *)text {
    CGSize maxSize = CGSizeMake(_tableView.width * .75, 9999);
    UIFont *font = [UIFont systemFontOfSize:14];
    CGRect textRect = [text boundingRectWithSize:maxSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName : font}
                                         context:nil];
    return textRect.size.height - font.lineHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor colorWithHexString:@"#699bc7" withAlpha:0.99f];
    NSString *string = @" Favorites";
    if (_editMealDetails[mTypeKey])
        string = [[self stringMealTypeValue:[_editMealDetails[mTypeKey] intValue]] stringByAppendingString:string];
    
    CGFloat offset = 10;
    UIImageView *star = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"favorite-fill-icon-64"] colorizedImage:[UIColor whiteColor]]];
    star.left = offset;
    star.top = (tableView.sectionHeaderHeight - star.height) / 2;
    [view addSubview:star];
    
    UILabel *label = [[UILabel alloc] initWithFrame:star.frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    label.text = [string uppercaseString];
    label.left = star.right + 5;
    label.width = tableView.width - (offset * 2 + star.right);
    [view addSubview:label];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^ {
        _editingMode = FromFavorite;
        [self loadControllerSettings:_favorites[indexPath.row]
                     withEditingMode:_editingMode];
    });
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end



