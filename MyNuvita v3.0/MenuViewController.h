//
//  MenuViewController.h
//  NuvitaCardio
//
//  Created by John on 3/31/14.
//
//

#import "Globals.h"
#import "ECSlidingViewController.h"

@class MenuViewController;
@protocol MenuViewControllerDelegate <NSObject>

- (void)menuViewController:(MenuViewController *)viewController didSelectMenu:(NSInteger)index;

@end

@interface MenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (strong, nonatomic) id <MenuViewControllerDelegate> delegate;

@end
