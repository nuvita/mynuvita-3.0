//
//  FriendViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/14/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "FriendViewController.h"

@interface FriendViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, ECSlidingViewControllerDelegate, FriendTableViewControllerDelegate, FriendTableViewCellDelegate>

@property (retain, nonatomic) NSDate *today;
@property (retain, nonatomic) NSMutableArray *friends;
@property (assign) BOOL isSelectAll;

@end

@implementation FriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initLeftMenuViewController];
    [self loadSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getFriends];
    [self getFriendInviteListFromMenu:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.slidingViewController resetTopView];
}

#pragma mark - Menu

- (void)initLeftMenuViewController {
 
    self.view.layer.shadowOpacity = 1;
    self.view.layer.shadowRadius = 1;
    self.view.layer.shadowColor = [UIColor grayColor].CGColor;
    
    FriendTableViewController *friendTableViewController = [[FriendTableViewController alloc] init];
    friendTableViewController.delegate = self;
    self.slidingViewController.underLeftViewController = friendTableViewController;
}

- (void)showLeftBarMenu {
    FriendTableViewController *viewController = (FriendTableViewController *)self.slidingViewController.underLeftViewController;
    if ([viewController.slidingViewController underLeftShowing]) {
        [viewController.slidingViewController resetTopView];
        
    }else {
        [viewController.slidingViewController anchorTopViewTo:ECRight animations:^{
            
        } onComplete:^{
            
        }];
    }
}

#pragma mark - FriendViewControllerDelegate

- (void)friendTableViewController:(FriendTableViewController *)viewController
                  didSelectOption:(NSString *)option {
    
    [self.slidingViewController resetTopView];
    FindFriendsViewController *findFriendsViewController = (FindFriendsViewController *)[[UIStoryboard storyboardWithName:@"Main"
                                                                                                                   bundle:[NSBundle mainBundle]]
                                                                              instantiateViewControllerWithIdentifier:@"FindFriends"];
    findFriendsViewController.title = [option capitalizedString];
    findFriendsViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:findFriendsViewController
                                         animated:YES];
}

#pragma mark - Public Methods

- (void)moveNextClicked {
    _today = [_today getNextWeek];
    [self getFriends];
}

- (void)movePrevClicked {
    _today = [_today getLastWeek];
    [self getFriends];
}

- (void)loadSettings {

    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[[UIImage imageNamed:@"menu-icon"] colorizedImage:kColorNuvitaBlue] forState:UIControlStateNormal];
    [button addTarget:self
               action:@selector(showLeftBarMenu)
     forControlEvents:UIControlEventTouchUpInside];
    self.slidingViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    JBCustomButton *buttonPrev =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonPrev setButtonStyle:JBCustomButtonArrowLeftStyle];
    [buttonPrev addTarget:self
                   action:@selector(movePrevClicked)
         forControlEvents:UIControlEventTouchUpInside];
    
    JBCustomButton *buttonNext =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [buttonNext setButtonStyle:JBCustomButtonArrowRightStyle];
    [buttonNext addTarget:self
                   action:@selector(moveNextClicked)
         forControlEvents:UIControlEventTouchUpInside];
    
    self.slidingViewController.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:buttonNext],
                                                                      [[UIBarButtonItem alloc] initWithCustomView:buttonPrev]];
    _today = [NSDate date];
    _friends = [[NSMutableArray alloc] init];
    _isSelectAll = NO;
}

- (void)loadFriendsData:(NSDictionary *)info {
    _lblWeek.text = info[kInfoWeekKey];
   
//    [_friends removeAllObjects];
    id object = info[kInfoFriendsKey][kFriendKey];
    if ([object isKindOfClass:[NSArray class]]) {
        NSArray *array = [NSArray arrayWithArray:info[kInfoFriendsKey][kFriendKey]];
        [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [obj setObject:@NO forKey:@"selected"];
        }];
        
        [_friends addObjectsFromArray:array];
    }
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        [object setObject:@NO forKey:@"selected"];
        [_friends addObject:object];
    }
    
    [_tableView reloadData];
}

- (void)getFriends {
    _isSelectAll = NO;
    if ([[_today getLastWeek] isEqualToDate:[[NSDate date] getLastWeek]]) {
        [[self.slidingViewController.navigationItem.rightBarButtonItems firstObject] setEnabled:NO];
    } else {
        [[self.slidingViewController.navigationItem.rightBarButtonItems firstObject] setEnabled:YES];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimating];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        if (![loginInfo objectForKey:kInfoIDKey]) {
            [UIAlertView displayAlert:@"Session is invalid!"];
            return;
        }
        
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetFriends;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.date = [_today description];
        parameter.skip = [NSString stringWithFormat:@"%d", [_friends count]];
        parameter.take = @"10";
            
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetFriendsDidFinishWithResponse:response];
        });
    });
}

- (void)getFriendInviteListFromMenu:(BOOL)flag {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIGetFriendInviteList;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        response = [binding parseBindingRequestUsingParameter:parameter];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self GetFriendInviteListDidFinishWithResponse:response fromMenu:flag];
        });
    });
}

- (void)removeFriend:(NSDictionary *)data {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [JBLoadingView startAnimatingWithText:[NSString stringWithFormat:@"Removing %@ from your friends list...", data[@"a:DisplayName"]]];
        NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
        WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
        binding.logXMLInOut = YES;
        binding.api = kAPIRemoveFriend;
        
        WSDLRequestResponse *response;
        WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
        parameter.eid = [loginInfo objectForKey:kInfoIDKey];
        parameter.friendEid = data[@"a:Eid"];
        response = [binding parseBindingRequestUsingParameter:parameter];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self RemoveFriendDidFinishWithResponse:response];
        });
    });
}

- (void)didTapMore {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"remove friends", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)didTapCheckbox:(id)sender {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    _isSelectAll = !_isSelectAll;
    [_friends enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj setObject:[NSNumber numberWithBool:_isSelectAll] forKey:@"selected"];
    }];
    
    [self performSelector:@selector(reloadTableView)
               withObject:nil
               afterDelay:0.3];
}

- (void)reloadTableView {
    [_tableView reloadData];
}

- (void)didTapEmail:(id)sender {
    NSMutableArray *recipients = [NSMutableArray array];
    [_friends enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[@"selected"] boolValue]) {
            [recipients addObject:obj[@"a:Email"]];
        }
    }];
    
    [self loadEmailView:recipients];
}

- (void)didTapRemoveFriends {
    [_friends enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj[@"selected"] boolValue]) {
            [self removeFriend:obj];
        }
    }];
}

#pragma mark - FriendTableViewCellDelegate

- (void)tapCheckmarkCell:(FriendTableViewCell *)cell {
    NSInteger row = [[_tableView indexPathForCell:cell] row];
    NSMutableDictionary *info = [NSMutableDictionary dictionaryWithDictionary:_friends[row]];
    [info setObject:[NSNumber numberWithBool:![info[@"selected"] boolValue]]
             forKey:@"selected"];
    _friends[row] = info;
    
    cell.imgCheckbox.image = [info[@"selected"] boolValue] ? [UIImage imageNamed:@"check-blue"] : [UIImage imageNamed:@"uncheck-blue"];
}

- (void)removeCell:(FriendTableViewCell *)cell {

}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self didTapRemoveFriends];
    }
}

#pragma mark - WSDLRequestResponse Delegate

- (void)RemoveFriendDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    [self getFriends];
    NSLog(@"RemoveFriendDidFinishWithResponse: %@", [response info]);
}

- (void)AcceptFriendInviteDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    NSLog(@"AcceptFriendInviteDidFinishWithResponse: %@", [response info]);
}

- (void)GetFriendInviteListDidFinishWithResponse:(WSDLRequestResponse *)response fromMenu:(BOOL)flag {
    if (flag && ![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    NSLog(@"GetFriendInviteListDidFinishWithResponse: %@", [response info]);
    NSInteger count = 0;
    if ([[response status] isSuccess]) {
        id object = [response info][@"a:Names"][@"a:NameItem"];
        if ([object isKindOfClass:[NSArray class]]) {
            count = [object count];
        }
        
        if ([object isKindOfClass:[NSDictionary class]]) {
            count = 1;
        }
    }
    
    FriendTableViewController *viewController = (FriendTableViewController *)self.slidingViewController.underLeftViewController;
    viewController.invites = count;
    self.slidingViewController.navigationItem.leftBarButtonItem = nil;

    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[[UIImage imageNamed:@"menu-icon"] colorizedImage:kColorNuvitaBlue]
            forState:UIControlStateNormal];
    [button addTarget:self
               action:@selector(showLeftBarMenu)
     forControlEvents:UIControlEventTouchUpInside];
    
    MKNumberBadgeView *badge = [MKNumberBadgeView drawBadgeWithValue:[NSNumber numberWithInteger:count]
                                                             atFrame:[button frame]];
    [button addSubview:badge];
    self.slidingViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)GetFriendsDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    if (![[response status] isSuccess]) {
        [UIAlertView displayAlert:[[response status] errorMessage]];
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:_today forKey:@"kFriendsDate"];
    [self loadFriendsData:[response info]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_friends count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell"
                                              owner:self
                                            options:0] objectAtIndex:1];
        [cell loadSettings];
        [cell setDelegate:self];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }

    NSDictionary *data = _friends[indexPath.row];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [cell getUserCellData:data atDate:_today];
    });
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 45)];
    view.backgroundColor = [UIColor clearColor];
    
    UIView *transparent = [[UIView alloc] initWithFrame:view.frame];
    transparent.backgroundColor = [UIColor whiteColor];
    transparent.alpha = 0.9;
    
    UIButton *btnCheckbox = [[UIButton alloc] initWithFrame:CGRectMake(8, 10, 25, 25)];
    btnCheckbox.layer.borderColor = [kColorNuvitaBlue CGColor];
    btnCheckbox.layer.borderWidth = 1;
    btnCheckbox.layer.cornerRadius = 2;
    btnCheckbox.layer.masksToBounds = YES;
    UIImage *image = _isSelectAll ? [[UIImage imageNamed:@"check-blue"] colorizedImage:kColorNuvitaBlue] : [[UIImage imageNamed:@"uncheck-blue"] colorizedImage:kColorNuvitaBlue];
    [btnCheckbox setImage:image
                 forState:UIControlStateNormal];
    [btnCheckbox addTarget:self
                    action:@selector(didTapCheckbox:)
          forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame = btnCheckbox.frame;
    UILabel *lblSelectAll = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x + frame.size.width + 5, 5, 100, 35)];
    lblSelectAll.backgroundColor = [UIColor clearColor];
    lblSelectAll.font = [UIFont systemFontOfSize:14];
    lblSelectAll.text = @"select all";
    
    frame = CGRectMake(tableView.frame.size.width - 85 - 8, 5, 35, 35);
    UIButton *btnMore = [[UIButton alloc] initWithFrame:frame];
    [btnMore setImage:[[UIImage imageNamed:@"cellmore-icon"] colorizedImage:kColorNuvitaBlue]
                 forState:UIControlStateNormal];
    [btnMore addTarget:self
                    action:@selector(didTapMore)
          forControlEvents:UIControlEventTouchUpInside];
    
    frame = CGRectMake(tableView.frame.size.width - 35 - 8, 5, 35, 35);
    UIButton *btnMessage = [[UIButton alloc] initWithFrame:frame];
    [btnMessage setImage:[[UIImage imageNamed:@"message-icon"] colorizedImage:kColorNuvitaBlue]
             forState:UIControlStateNormal];
    [btnMessage addTarget:self
                   action:@selector(didTapEmail:)
         forControlEvents:UIControlEventTouchUpInside];
    
    [transparent addSubview:btnCheckbox];
    [transparent addSubview:lblSelectAll];
    [transparent addSubview:btnMore];
    [transparent addSubview:btnMessage];
    
    CALayer *layer = [CALayer drawLineWithSize:CGSizeMake(tableView.frame.size.width, 44)];
    [transparent.layer addSublayer:layer];
    [view addSubview:transparent];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (_tableView.contentSize.height < scrollView.contentOffset.y + _tableView.bounds.size.height) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getFriends];
        });
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)loadEmailView:(NSArray *)recipients {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setToRecipients:recipients];
    [picker setSubject:[NSString stringWithFormat:@"myNuvita message from your friend %@", [[NSUserDefaults standardUserDefaults] objectForKey:kInfoFirstNameKey]]];

    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if ([mailClass canSendMail]) {
        //..
        [self presentViewController:picker animated:YES completion:^{
        }];
        
    } else {
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Unable to load mail. Please set-up an email account in Settings > Mail,Contacts,Calendar."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertview show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled: {
            NSLog(@"email sending cancelled");
        }
            break;
        case MFMailComposeResultSaved: {
            NSLog(@"email sending saved");
        }
            break;
        case MFMailComposeResultSent: {
            NSLog(@"email sending sent");
            
        }
            break;
        case MFMailComposeResultFailed: {
            NSLog(@"email sending failed");
        }
            break;
            
        default:
            break;
    }
    NSLog(@"error: %@",[error localizedDescription]);
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
