//
//  WebViewCell.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/17/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "WebViewCell.h"

@implementation WebViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize size = self.frame.size;
    CALayer *layer = [CALayer drawWebViewLineWithSize:CGSizeMake(size.width, size.height + 10)];
    [self.contentView.layer addSublayer:layer];
}

@end
