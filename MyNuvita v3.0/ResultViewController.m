//
//  ResultViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/19/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    JBCustomButton *backButton =  [[JBCustomButton alloc] initWithFrame:CGRectMake(0, 20, 70, 30)];
    backButton.backgroundColor = [UIColor clearColor];
    backButton.tintColor = kColorNuvitaBlue;
    [backButton setButtonStyle:JBCustomButtonArrowTextLeftStyle];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton addTarget:self
                   action:@selector(didTapBack)
         forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTapBack {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    _lblNumberOfQuestion.text = [_resultInfo[@"numberOfQuestion"] stringValue];
    _lblNumberOfCorrect.text = [_resultInfo[@"numberOfCheck"] stringValue];
    _lblScore.text = [NSString stringWithFormat:@"%@%%", _resultInfo[@"a:MemberScore"]];
}

@end
