//
//  AddCoachPostViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/8/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "AddCoachPostViewController.h"

@interface AddCoachPostViewController ()

@end

@implementation AddCoachPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Send"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapSave)];
    _textView.contentInset = UIEdgeInsetsMake(-69, 0, 0, 0);
    _textView.textColor = [UIColor lightGrayColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void)didTapSave {
    [JBLoadingView startAnimatingWithText:@"Saving message..."];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIPostCoachingWall;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.dateTime = [[NSDate date] description];
    parameter.message = [NSString stringWithFormat:@"%@", [[_textView text] description]];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self PostCoachingWallDidFinishWithResponse:response];
    });
}

#pragma mark - WSDLRequestResponse Delegate

- (void)PostCoachingWallDidFinishWithResponse:(WSDLRequestResponse *)response {
    [JBLoadingView stopAnimating];
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
    }
    
//    if (![[response status] isSuccess]) {
//        [UIAlertView displayAlert:[[response status] errorMessage]];
//    }
    
    [UIAlertView displayAlert:[[response info] objectForKey:@"a:Message"]];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    textView.textColor = [UIColor blackColor];
    textView.text = @"";
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([textView.text length] == 0) {
        [textView endEditing:YES];
    }
}


@end
