//
//  HomeViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/13/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblProgram;
@property (weak, nonatomic) IBOutlet UILabel *lblWeek;


@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;

@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

- (void)showLeftBarMenu;
- (void)movePrevClicked;
- (void)moveNextClicked;

@end
