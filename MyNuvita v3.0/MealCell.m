//
//  MealCell.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 3/3/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "MealCell.h"

@implementation MealCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [[[NSBundle mainBundle] loadNibNamed:@"MealCell" owner:nil options:nil] lastObject];
    if (self) {
        
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    _imgMeal.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _imgMeal.layer.borderWidth = 0.5f;
    _imgMeal.layer.cornerRadius = 2.0f;
    _imgMeal.layer.masksToBounds = YES;
    [_imgMeal autoResizeContent];
    
    _lblMeal.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    _lblMeal.numberOfLines = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
