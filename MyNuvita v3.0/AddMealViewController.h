//
//  AddMealViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 2/7/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "Globals.h"
#import "MealCell.h"

@interface AddMealViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *imgDefault;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@property (retain, nonatomic) NSDictionary *mealInfo;
@property (retain, nonatomic) NSDate *today;

@end
