//
//  AddNewPostViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/10/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "AddNewPostViewController.h"

@interface AddNewPostViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (retain, nonatomic) UIImagePickerController *imagePicker;
@property (retain, nonatomic) NSData *imageData;
@property (retain, nonatomic) NSDate *today;

@end

@implementation AddNewPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Post"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(postWellnessWall)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _today = [NSDate date];
    _textView.textColor = [UIColor lightGrayColor];
    _textView.text = @"Share your thoughts...";
    
//    _imgSelected.layer.cornerRadius = 4.0f;
    _imgSelected.layer.masksToBounds = YES;
    [_imgSelected autoResizeContent];
    
    _imgAvatar.layer.cornerRadius = _imgAvatar.frame.size.width / 2;
    _imgAvatar.layer.masksToBounds = YES;
    [_imgAvatar setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:kInfoAvatarKey]]
               placeholderImage:[[UIImage imageNamed:@"profile-avatar"] colorizedImage:kColorNuvitaBlue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillShow:(NSNotification *)notification  {
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    CGRect frame = _editingView.frame;
    frame.origin.y -= height - 50;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.editingView.frame = frame;
                     }];
}

- (void)keyboardDidHide:(NSNotification *)notification  {
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    CGRect frame = _editingView.frame;
    frame.origin.y += height - 50;
    [UIView animateWithDuration:0.3
                     animations:^{
                         _editingView.frame = frame;
                     }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    textView.textColor = [UIColor blackColor];
    textView.text = @"";
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([textView.text length] == 0) {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Share your thougths...";
        [textView endEditing:YES];
    }
}

#pragma mark - Public Methods

- (void)postWellnessWall {
    [JBLoadingView startAnimating];
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIPostWellnessWall;
    
//    NSString *photo = [NSString stringWithFormat:@"%@", ];
    NSString *message = [NSString stringWithFormat:@"%@", [_textView.text description]];
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [loginInfo objectForKey:kInfoIDKey];
    parameter.dateTime = [_today description];
    parameter.message = message;
    parameter.photo = [_imageData base64Encoding];
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self PostWellnessWallDidFinishWithResponse:response];
    });
}

- (IBAction)didTapDone:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)didTapCamera:(id)sender {
    [_textView endEditing:YES];

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose from library", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"info: %@", info);
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (!selectedImage) {
        selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    if (selectedImage) {
        
        CGSize size =  selectedImage.size;
        CGRect frame = CGRectMake(0, 0, size.width * .25, size.height * .25);
        UIGraphicsBeginImageContext(frame.size);
        [selectedImage drawInRect:frame];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        _imageData = UIImageJPEGRepresentation(image, .1);
        _imgSelected.image = selectedImage;

        NSLog(@"size: %f - %f", image.size.width, image.size.height);
        NSLog(@"length: %f", [_imageData length] / 1024.0);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != 2) {
        [self showImagePickerWithSourceType:buttonIndex == 0 ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
}

- (void)showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType {
    _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            _imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        
    } else {
        _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:_imagePicker
                       animated:YES completion:^{
                       }];
}

#pragma mark - WSDLRequestResponseDelegate

- (void)PostWellnessWallDidFinishWithResponse:(WSDLRequestResponse *)response {
    NSLog(@"response: %@", [response info]);
    [JBLoadingView stopAnimating];
    if ([response error]) {
        [UIAlertView displayAlert:[[response error] localizedDescription]];
        return;
    }
    
    [UIAlertView displayAlert:[[response info] objectForKey:@"a:Message"]];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
