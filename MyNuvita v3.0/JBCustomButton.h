//
//  JBCustomButton.h
//  mynuvita
//
//  Created by John on 7/28/14.
//
//

#import "Globals.h"

typedef enum {
    JBCustomButtonDefaultStyle,
    JBCustomButtonArrowLeftStyle,
    JBCustomButtonArrowRightStyle,
    JBCustomButtonArrowTextLeftStyle,
    JBCustomButtonArrowTextRightStyle,
} JBCustomButtonStyle;

@interface JBCustomButton : UIButton

@property (assign, nonatomic) JBCustomButtonStyle buttonStyle;

@end
