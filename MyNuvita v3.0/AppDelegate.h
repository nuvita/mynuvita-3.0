//
//  AppDelegate.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 11/13/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"
#import "CrashHandler.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)loadLogin;
- (void)loadTabarController;
- (void)loadCoachTabarController;
- (void)loadNutritionTabarController;

@end

