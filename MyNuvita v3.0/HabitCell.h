//
//  HabitCell.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 3/19/15.
//  Copyright (c) 2015 John Bariquit. All rights reserved.
//

#import "Globals.h"

@protocol HabitCellDelegate <NSObject>

- (void)habitCellDidBeginEditing:(NSInteger)idx;
- (void)habitCellDidEndEditing:(NSInteger)idx;

@end

@interface HabitCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnHabit;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIView *viewRate;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (assign, nonatomic) BOOL top;
@property (assign, nonatomic) BOOL bottom;

@property (retain, nonatomic) id <HabitCellDelegate> delegate;

@end
