//
//  QuizViewController.h
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/15/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "Globals.h"

@interface QuizViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (retain, nonatomic) NSArray *questions;
@end
