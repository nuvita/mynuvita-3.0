//
//  CoachHomeViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/27/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "CoachHomeViewController.h"

@interface CoachHomeViewController ()

@end

@implementation CoachHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate loadTabarController];
}

@end
