//
//  ResourcesViewController.m
//  MyNuvita v3.0
//
//  Created by John Bariquit on 12/8/14.
//  Copyright (c) 2014 John Bariquit. All rights reserved.
//

#import "ResourcesViewController.h"

@interface ResourcesViewController ()

@property (retain, nonatomic) NSMutableArray *resources;

@end

@implementation ResourcesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _resources = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self getCoachResources];
}

#pragma mark - Public Methods

- (void)loadCoachResources:(NSDictionary *)info {
    [_resources removeAllObjects];
    
    id object = info[@"a:Resources"][@"a:CoachingResource"];
    if ([object isKindOfClass:[NSArray class]])
        [_resources addObjectsFromArray:object];
    if ([object isKindOfClass:[NSDictionary class]])
        [_resources addObject:object];
    
    [_tableView reloadData];
}

- (void)getCoachResources {
    NSUserDefaults *loginInfo = [NSUserDefaults standardUserDefaults];
    WSDLRequestBinding *binding = [WSDLRequest WSDLBinding];
    binding.logXMLInOut = YES;
    binding.api = kAPIGetCoachResources;
    
    WSDLRequestResponse *response;
    WSDLRequestParameter *parameter = [[WSDLRequestParameter alloc] init];
    parameter.eid = [[loginInfo objectForKey:kInfoIDKey] description];
    parameter.skip = @"0";
    parameter.take = @"10";
    
    response = [binding parseBindingRequestUsingParameter:parameter];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self GetCoachResourcesDidFinishWithResponse:response];
    });
    
}

#pragma mark - WSDLRequestResponse Delegate

- (void)GetCoachResourcesDidFinishWithResponse:(WSDLRequestResponse *)response {
    NSLog(@"response: %@", response.info);
    [self loadCoachResources:[response info]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_resources count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textColor = [UIColor blueColor];
        
        CALayer *layer = [CALayer drawLineWithSize:cell.frame.size];
        [cell.contentView.layer addSublayer:layer];
    }
    
   
    NSDictionary *data = _resources[indexPath.row];
    cell.textLabel.text = data[@"a:Title"];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)minHeightForText:(NSString *)_text {
    return [_text
            sizeWithFont:[UIFont boldSystemFontOfSize:16]
            constrainedToSize:CGSizeMake(300, 999999)
            lineBreakMode:NSLineBreakByWordWrapping
            ].height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = [self minHeightForText:@"from your coach"];
    return height + 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor clearColor];

    NSString *text = @"from your coach";
    CGFloat height = [self minHeightForText:text];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, height)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont boldSystemFontOfSize:20];
    lblTitle.numberOfLines = 0;
    lblTitle.text = @"from your coach";
    [lblTitle underline];
    
    [view addSubview:lblTitle];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *data = _resources[indexPath.row];
    [[NSUserDefaults standardUserDefaults] setObject:[data[@"a:URL"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                              forKey:kInfoURLKey];
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                  bundle:[NSBundle mainBundle]]
                                        instantiateViewControllerWithIdentifier:@"Webview"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

@end
